package com.miaobei.game.cubic.wxapi;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;

import org.cocos2dx.javascript.AppActivity;
import org.cocos2dx.javascript.WXSDK;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("tt","wxEntryActivity onCreate");
        // 这句话很关键
        try {
            AppActivity.wx_api.handleIntent(getIntent(), this);
        }catch (Exception e) {

        }

    }


    @Override
    public void onReq(BaseReq baseReq) {
        System.out.println("Enter the onReq");
    }

    @Override
    public void onResp(BaseResp baseResp) {
        System.out.println("Enter the onResp");
        if(baseResp.errCode == BaseResp.ErrCode.ERR_OK){
//            String code = ((SendAuth Resp) baseResp).code;
            String code = ((SendAuth.Resp) baseResp).code;
            System.out.println("==========code is ===========" + code);
            WXSDK.OnWxAuthCode(code);
            finish();
        }
    }
}
