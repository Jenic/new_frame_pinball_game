package org.cocos2dx.javascript;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelmsg.GetMessageFromWX;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import android.os.Bundle;
import android.util.Log;

import com.miaobei.game.cubic.wxapi.WXEntryActivity;
import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

public class WXSDK {
    // IWXAPI 是第三方app和微信通信的openApi接口

    static AppActivity GetContext() {
        return  AppActivity.GetContext();
    }
    static String JSClassName;
    public static void SetJsListener(String JSClassName) {
        WXSDK.JSClassName = JSClassName;
    }
    public static void Register(String appId, String secret){
        final  String fId = appId;
        Log.d("WXSDK", "WX register :" + appId + ","+ secret);
        AppActivity.WXInit(appId);
    }

    public static void GetWxAuthCode(){
        // send oauth request
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "none";
        AppActivity.wx_api.sendReq(req);
    }

    public static void OnWxAuthCode(String code){
        CallJsMethod("OnWxAuthCode",code);
    }
    // app 发送消息给微信
    public static void SendReq(){
        //初始化一个 WXTextObject 对象，填写分享的文本内容
        WXTextObject textObj = new WXTextObject();
        textObj.text = "text";

        //用 WXTextObject 对象初始化一个 WXMediaMessage 对象
        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = textObj;
        msg.description = "text";

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());  //transaction字段用与唯一标示一个请求
        req.message = msg;
        req.scene = 0;

        //调用api接口，发送数据到微信
    }

    public static void SendResp() {
        // 初始化一个 WXTextObject 对象
        WXTextObject textObj = new WXTextObject();
        textObj.text = "text";

        // 用 WXTextObject 对象初始化一个 WXMediaMessage 对象
        WXMediaMessage msg = new WXMediaMessage(textObj);
        msg.description = "text";

        // 构造一个Resp
        GetMessageFromWX.Resp resp = new GetMessageFromWX.Resp();
        // 将req的transaction设置到resp对象中，其中bundle为微信传递过来的Intent所带的内容，通过getExtras()方法获取
        Bundle bundle = GetContext().getIntent().getExtras();
        resp.transaction = new GetMessageFromWX.Req(bundle).transaction;
        resp.message = msg;

        //调用api接口，发送数据到微信
        AppActivity.wx_api.sendResp (resp) ;
    }

    public static void CallJsMethod(String funcName, String param) {
        final  String ffn = funcName;
        final String fpm = param;
        GetContext().runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(JSClassName + '.' + ffn  + "('" + fpm + "');");
            }
        });
    }
}
