package org.cocos2dx.javascript;

import android.util.Log;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConfig;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.bytedance.sdk.openadsdk.TTRewardVideoAd;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import static com.bytedance.sdk.openadsdk.TTAdConstant.TAG;

public class PangleAdManager {
    static String JSClassName;
    static TTAdNative mTTAdNative;

    static final  String OnInitedSuccess = "OnInitedSuceess";


    public static void SetListener(String listener){
        PangleAdManager.JSClassName = listener;
    }
    public static void Init(final String appId,final String appName){
        Log.d(TAG, "AdSdk Init: ");
        GetContext().runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Log.d(TAG, "AdSdk runOnGLThread: ");
                //强烈建议在应用对应的Application#onCreate()方法中调用，避免出现content为null的异常
                TTAdSdk.init(AppActivity.getContext(),
                        new TTAdConfig.Builder()
                                .appId(appId)
                                .useTextureView(true) //默认使用SurfaceView播放视频广告,当有SurfaceView冲突的场景，可以使用TextureView
                                .appName(appName)
                                .titleBarTheme(TTAdConstant.TITLE_BAR_THEME_DARK)//落地页主题
                                .allowShowNotify(true) //是否允许sdk展示通知栏提示,若设置为false则会导致通知栏不显示下载进度
                                .debug(true) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                                .directDownloadNetworkType() //允许直接下载的网络状态集合,没有设置的网络下点击下载apk会有二次确认弹窗，弹窗中会披露应用信息
                                .directDownloadNetworkType(TTAdConstant.NETWORK_STATE_WIFI) //允许直接下载的网络状态集合,没有设置的网络下点击下载apk会有二次确认弹窗，弹窗中会披露应用信息
//                                .supportMultiProcess(false) //是否支持多进程，true支持
                                .asyncInit(true) //是否异步初始化sdk,设置为true可以减少SDK初始化耗时。3450版本开始废弃~~
                                //.httpStack(new MyOkStack3())//自定义网络库，demo中给出了okhttp3版本的样例，其余请自行开发或者咨询工作人员。
                                .build());
                //如果明确某个进程不会使用到广告SDK，可以只针对特定进程初始化广告SDK的content
                //if (PROCESS_NAME_XXXX.equals(processName)) {
                //   TTAdSdk.init(context, config);
                //}
                mTTAdNative = TTAdSdk.getAdManager().createAdNative(GetContext());
            }
        });
    }




    static AppActivity GetContext() {
        return  AppActivity.GetContext();
    }

    public static void CallJsMethod(String funcName, String param) {
        final  String ffn = funcName;
        final String fpm = param;
        GetContext().runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(JSClassName + '.' + ffn  + "('" + fpm + "');");
            }
        });
    }
}
