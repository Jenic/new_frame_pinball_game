package org.cocos2dx.javascript;

import android.support.annotation.NonNull;
import android.transition.Scene;
import android.util.Log;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTRewardVideoAd;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.bytedance.sdk.openadsdk.TTAdConstant.TAG;
import static org.cocos2dx.javascript.AppActivity.mTTAdNative;
import static org.cocos2dx.javascript.PangleAdManager.GetContext;

public class PangleRewardVideo {
    static String JSClassName;

    // 激励视频
    static final String OnLoadRewardVideoSuccess = "OnLoadRewardVideoSuccess";
    static final String OnLoadRewardVideoFailed = "OnLoadRewardVideoFailed";
    static final String OnShowRewardVideoFailed  = "OnShowRewardVideoFailed";
    static final String OnRewardVideoShow = "OnRewardVideoShow";
    static final String OnRewardVideoClick = "OnRewardVideoClick";
    static final String OnRewardVideoSkip = "OnRewardVideoSkip";
    static final String OnRewardVideoReward = "OnRewardVideoReward";
    static final String OnRewardVideoClose = "OnRewardVideoClose";
    static Map<String, TTRewardVideoAd> adMap = new HashMap<String, TTRewardVideoAd>();
    static Map<String, Boolean> loadMap = new HashMap<String, Boolean>();
    public static void SetListener(String listener){
        PangleRewardVideo.JSClassName = listener;
        Log.d(TAG, "RewardVideo SetListener: "+ listener);
    }


    public static void LoadRewardVideo(String slotCode, final String SceneId){
        Log.d(TAG, "LoadRewardVideo: ");
        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(slotCode)
                .setRewardName("金币")
                .setRewardAmount(3)
                // 模板渲染 代码位ID
                .setExpressViewAcceptedSize(500,500)
                .setUserID("tag123")
                .setMediaExtra("media_extra")
                .setOrientation(TTAdConstant.VERTICAL)
                .build();

        final  String code = SceneId + ',' + slotCode;
        mTTAdNative.loadRewardVideoAd(adSlot, new TTAdNative.RewardVideoAdListener() {
            @Override
            public void onError(int i, String s) {
                Log.d(TAG, "onError: 1" + s);
                CallJsMethod(OnLoadRewardVideoFailed,code);
            }

            @Override
            public void onRewardVideoAdLoad(TTRewardVideoAd ttRewardVideoAd) {
                Log.d(TAG, "onRewardVideoAdLoad: ");
                adMap.put(SceneId, ttRewardVideoAd);
                loadMap.put(SceneId, true);

                ttRewardVideoAd.setRewardAdInteractionListener(new TTRewardVideoAd.RewardAdInteractionListener() {
                    @Override
                    public void onAdShow() {
                        Log.d(TAG, "onAdShow: ");
                        CallJsMethod(OnRewardVideoShow, code);
                        loadMap.put(SceneId,false);
                    }

                    @Override
                    public void onAdVideoBarClick() {
                        Log.d(TAG, "onAdVideoBarClick: ");
                        CallJsMethod(OnRewardVideoClick, code);
                    }

                    @Override
                    public void onAdClose() {
                        Log.d(TAG, "onAdClose: ");
                        CallJsMethod(OnRewardVideoClose, code);
                    }

                    @Override
                    public void onVideoComplete() {
                        Log.d(TAG, "onVideoComplete: ");
                    }

                    @Override
                    public void onVideoError() {

                        Log.d(TAG, "onVideoError: ");
                        CallJsMethod(OnShowRewardVideoFailed,code);
                    }

                    @Override
                    public void onRewardVerify(boolean b, int i, String s, int i1, String s1) {
                        Log.d(TAG, "onRewardVerify: ");
                        CallJsMethod(OnRewardVideoReward,null);
                    }

                    @Override
                    public void onSkippedVideo() {
                        Log.d(TAG, "onSkippedVideo: ");
                        CallJsMethod(OnRewardVideoSkip,code);
                    }
                });
            }

            @Override
            public void onRewardVideoCached() {
                Log.d(TAG, "onRewardVideoCached: ");
                CallJsMethod(OnLoadRewardVideoSuccess, code);
            }
        });
    }

    public static void ShowRwardVideo(final String SceneId){

        if(loadMap.get(SceneId)){
            GetContext().runOnUiThread(new Runnable(){
                @Override
                public void run() {
                    TTRewardVideoAd ad = adMap.get(SceneId);
                    ad.showRewardVideoAd(GetContext());
                }
            });
        } else {
            CallJsMethod(OnShowRewardVideoFailed,SceneId + ",");
        }
    }

    public static boolean IsRewardVideoLoaded(String sceneId){
        return  loadMap.get(sceneId)!=null && loadMap.get(sceneId);
    }

    public static void CallJsMethod(String funcName, String param) {
        final  String ffn = funcName;
        final String fpm = param;
        GetContext().runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(JSClassName + '.' + ffn  + "('" + fpm + "');");
            }
        });
    }
}
