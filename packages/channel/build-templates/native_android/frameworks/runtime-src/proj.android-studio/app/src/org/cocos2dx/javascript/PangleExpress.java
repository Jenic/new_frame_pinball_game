package org.cocos2dx.javascript;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.bytedance.sdk.openadsdk.TTSplashAd;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bytedance.sdk.openadsdk.TTAdConstant.TAG;
import static org.cocos2dx.javascript.AppActivity.GetContext;
import static org.cocos2dx.javascript.AppActivity.mTTAdNative;

public class PangleExpress {
    static Map<String, TTNativeExpressAd> adMap = new HashMap<String, TTNativeExpressAd>();
    static Map<String, View> vMap = new HashMap<String, View>();
    static String JSClassName;
    public static void SetListener(String listener){
        PangleExpress.JSClassName = listener;
        Log.d(TAG, "RewardVideo SetListener: "+ listener);
    }

    public static void LoadExpress(final String slotCode, final String SceneId) {
        AppActivity.GetContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AdSlot adSlot = new AdSlot.Builder()
                        .setCodeId(slotCode) //广告位id
                        .setSupportDeepLink(true)
                        .setAdCount(1) //请求广告数量为1到3条
                        .setExpressViewAcceptedSize(400,0) //期望模板广告view的size,单位dp
                        .build();

                mTTAdNative.loadNativeExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
                    //广告请求失败
                    @Override
                    public void onError(int code, String message) {
                        CallJsMethod("OnExpressLoadFailed", SceneId);
                    }

                    //广告请求成功
                    @Override
                    public void onNativeExpressAdLoad(List<TTNativeExpressAd> ads) {
                        final TTNativeExpressAd mTTAd = ads.get(0);
                        Log.d(TAG, "onNativeExpressAdLoad: ");
                        mTTAd.setExpressInteractionListener(new TTNativeExpressAd.ExpressAdInteractionListener() {

                            //广告点击回调
                            @Override
                            public void onAdClicked(View view, int type) {
                                Log.d(TAG, "onAdClicked: " + type);
                            }

                            //广告展示回调
                            @Override
                            public void onAdShow(View view, int type) {
                                Log.d(TAG, "onAdShow: " + type);
                            }

                            //广告渲染失败回调
                            @Override
                            public void onRenderFail(View view, String msg, int code) {

                                Log.d(TAG, "onRenderFail: " + code+msg);
                            }

                            //广告渲染成功回调
                            @Override
                            public void onRenderSuccess(final View view, float width, float height) {
                                Log.d(TAG, "onRenderSuccess: ");
                                vMap.put(SceneId,view);

                            }
                        });
                        mTTAd.render();
                        adMap.put(SceneId,mTTAd);

                    }
                });
            }
        });
    }

    public static void ShowExpress(final String SceneId){

        AppActivity.GetContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View mTTAd = vMap.get(SceneId);
                vMap.remove(SceneId);
                AppActivity.GetAdContainer().addView(mTTAd);
            }
        });
    }

    public static void CloseExpress(final  String SceneId){
        AppActivity.GetContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TTNativeExpressAd ad = adMap.get(SceneId);
                adMap.remove(SceneId);
                if(ad!=null){
                    ad.destroy();
                }
                AppActivity.GetAdContainer().removeAllViews();
                CallJsMethod("OnExpressClose", SceneId);
            }
        });

    }

    public static void CallJsMethod(String funcName, String param) {
        final  String ffn = funcName;
        final String fpm = param;
        PangleAdManager.GetContext().runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(JSClassName + '.' + ffn  + "('" + fpm + "');");
            }
        });
    }
}
