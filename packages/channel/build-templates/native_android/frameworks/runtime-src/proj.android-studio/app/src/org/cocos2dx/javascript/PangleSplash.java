package org.cocos2dx.javascript;
import android.util.Log;
import android.view.View;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.bytedance.sdk.openadsdk.TTSplashAd;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import static com.bytedance.sdk.openadsdk.TTAdConstant.TAG;
import static org.cocos2dx.javascript.AppActivity.GetContext;
import static org.cocos2dx.javascript.AppActivity.mTTAdNative;
public class PangleSplash {
    static TTSplashAd mTTSplashAd;
    static String JSClassName = "SplashWaitter";

    static boolean SDKInited = false;
    static boolean loadBegin = false;
    static String mslotId = "";
    public static Callback callback = new Callback();

    public static void LoadSplashAd(final  String slotId){
        AppActivity.app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "LoadSplashAd success: ");

                if (!SDKInited){
                    loadBegin= true;
                    mslotId = slotId;
                    return;
                }
                AdSlot adSlot = new AdSlot.Builder()
                        .setCodeId(slotId)
                        .setImageAcceptedSize(1080, 1920)
                        .build();

                mTTAdNative.loadSplashAd(adSlot, new TTAdNative.SplashAdListener() {
                    @Override
                    public void onError(int i, String s) {

                    }

                    @Override
                    public void onTimeout() {
                        LoadSplashAd(slotId);
                        Log.d(TAG, "LoadSplashAd onTimeout: load again ");

                    }

                    @Override
                    public void onSplashAdLoad(TTSplashAd ttSplashAd) {
                        if (ttSplashAd == null) {
                            return;
                        }
                        mTTSplashAd = ttSplashAd;
                        CallJsMethod("OnSplashLoaded",null);
                        Log.d(TAG, "onSplashAdLoad: ");
                    }
                }, 4);
            }
        });
    }

    public static void ShowSplashAd(){
        AppActivity.app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View view = mTTSplashAd.getSplashView();
                if (view != null ) {
                    AppActivity.GetSplashContainer().removeAllViews();
                    //把SplashView 添加到ViewGroup中,注意开屏广告view：width =屏幕宽；height >=75%屏幕高
                    AppActivity.GetSplashContainer().addView(view);
                    //设置不开启开屏广告倒计时功能以及不显示跳过按钮,如果这么设置，您需要自定义倒计时逻辑
                    //ad.setNotAllowSdkCountdown();
                }else {
                    //开发者处理跳转到APP主页面逻辑
                }
                mTTSplashAd.setSplashInteractionListener(new TTSplashAd.AdInteractionListener() {

                    //点击回调
                    @Override
                    public void onAdClicked(View view, int type) {

                    }

                    //展示回调
                    @Override
                    public void onAdShow(View view, int type) {

                    }

                    //跳过回调
                    @Override
                    public void onAdSkip() {
                        //开发者处理跳转到APP主页面逻辑
                        AppActivity.GetSplashContainer().removeAllViews();
                    }

                    //超时倒计时结束
                    @Override
                    public void onAdTimeOver() {
                        //开发者处理跳转到APP主页面逻辑
                        AppActivity.GetSplashContainer().removeAllViews();
                    }
                });
            }
        });
    }

    public static void CallJsMethod(String funcName, String param) {
        final  String ffn = funcName;
        final String fpm = param;
        PangleAdManager.GetContext().runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(JSClassName + '.' + ffn  + "('" + fpm + "');");
            }
        });
    }
}


class Callback implements TTAdSdk.InitCallback {

    @Override
    public void success() {
        Log.d(TAG, "SDKInit success: ");
        PangleSplash.SDKInited = true;
        if (PangleSplash.loadBegin) {
            PangleSplash.LoadSplashAd(PangleSplash.mslotId);
        }
    }

    @Override
    public void fail(int i, String s) {

    }
}