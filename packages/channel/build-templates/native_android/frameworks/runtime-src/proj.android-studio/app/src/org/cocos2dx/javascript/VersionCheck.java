package org.cocos2dx.javascript;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.view.View;

import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.acl.Permission;
import java.text.DecimalFormat;
import java.util.function.Consumer;

import static com.umeng.commonsdk.stateless.UMSLEnvelopeBuild.mContext;

public class VersionCheck {

    /**
     * 下载apk的线程
     */
    private static String savePath = "/sdcard/updateLgtAPK/"; // apk保存到SD卡的路径
    private static String saveFileName = savePath + "apkGS.apk"; // 完整路径名
    private static int preProgress;
    private static int progress;
    private static boolean cancelFlag = false; // 取消下载标志位

    public static void stopDownLoad(){
        cancelFlag = true;
    }
    /**
     * 下载apk的线程
     */
    public static void downloadAPK(final String remoteUrl) {
        startDownLoadApk(remoteUrl);
    }

    public static void startDownLoadApk(final String remoteUrl){
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                try {
                    URL url = new URL(remoteUrl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.connect();

                    int length = conn.getContentLength();
                    InputStream is = conn.getInputStream();

                    File file = new File(savePath);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    String apkFile = saveFileName;
                    File ApkFile = new File(apkFile);
                    FileOutputStream fos = new FileOutputStream(ApkFile);

                    int count = 0;
                    byte buf[] = new byte[1024];

                    do {
                        int numread = is.read(buf);
                        count += numread;
                        preProgress = progress;
                        progress = (int) (((float) count / length) * 100);
                        if(preProgress != progress){
                            updateProgress(progress);
                        }

                        if (numread <= 0) {
                            // 下载完成通知安装
                            installAPK();
                            CallJsMethod("onSuccess", null);
                            break;
                        }
                        fos.write(buf, 0, numread);
                    } while (!cancelFlag); // 点击取消就停止下载.
                    fos.close();
                    is.close();
                } catch (Exception e) {
                    CallJsMethod("onFailed", null);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * 下载完成后自动安装apk
     */
    public static void installAPK() {

        File apkFile = new File(saveFileName);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (!apkFile.exists()) {
            apkFile.mkdir();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);//添加这一句表示对目标应用临时授权该Uri所代表的文件

            Uri contentUri = FileProvider.getUriForFile(AppActivity.app, "com.miaobei.game.cubic.fileProvider", apkFile);

            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
            AppActivity.app.startActivity(intent);

        } else {
            intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            AppActivity.app.startActivity(intent);
        }
    }

    public static void updateProgress(float n){
        DecimalFormat df1 = new DecimalFormat("####.000");
        String p = df1.format(n);
        CallJsMethod("onProgress", p);
    }

    public static void CallJsMethod(String funcName, String param) {
        final  String JSClassName = "downloadListener";
        final  String ffn = funcName;
        final String fpm = param;
        AppActivity.app.runOnGLThread(new Runnable(){
            @Override
            public void run() {
                Cocos2dxJavascriptJavaBridge.evalString(JSClassName + '.' + ffn  + "('" + fpm + "');");
            }
        });
    }
}
