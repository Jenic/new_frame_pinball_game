var fs = require('fs');


var _logInfo = (log) => {
    console.log(log);
};
var _logErr = (log) => {
    console.error(log);
};

function LogInfo(log) {
    _logInfo(log);
}

function LogErr(log) {
    _logErr(log);
}

function SetEditor(editor) {
    this._logInfo = editor.log;
    this._logInfo = editor.failed;
}

function TransPath(path) {
    path = path.replace(/\\/g, "/");
    return path;
}


function LoadJson(path) {
    let outdata = undefined;
    if (fs.existsSync(path)) {
        let content = fs.readFileSync(path);
        outdata = JSON.parse(content);
    }

    if (!outdata) {
        LogErr(`load json filed: ${path}`);
    }
    //// PrintLog(`>. LoadJson, path = ${path}, data = ${JSON.stringify(outdata)}, result = ${outdata ? "succeed" : "failed"}`);
    return outdata;
}

function WriteText(path, content) {
    let fd = fs.openSync(path, "w");
    fs.writeFileSync(path, content);
    fs.closeSync(fd);
}

function MkDir(path) {
    // Editor.log(`创建文件夹: ${path}`);
    try {
        fs.mkdirSync(path, { recursive: true }, (err) => {
            if (err) {
                throw err;
            } else {
                // Editor.log(`创建文件夹-ok: ${path}`);
            }
        });
    } catch (e) {
        Editor.failed(e);
    }
}

function CopyFile(srcfile, dstfile) {
    if (!dstfile || 0 == dstfile.length) {
        return;
    }
    // Editor.log(`复制文件, ${srcfile} 到 ${dstfile}`);
    dstfile = util.TransPath(dstfile);
    if (dstfile.endsWith('/')) {
        dstfile = dstfile.substring(0, dstfile.length - 1);
    }
    // Editor.log(`复制文件到 ${dstfile}`);
    let lastIdx = dstfile.lastIndexOf("/");
    let fileName = dstfile.substring(lastIdx, dstfile.length);
    printLog(`复制文件:${fileName}\n到:${dstfile}`);
    // Editor.log(`lastIdx:${lastIdx}, 目录:${dstfile}`);
    let folder = dstfile.substring(0, lastIdx);
    // Editor.log(`转换后: ${folder}`);
    MkDir(folder);
    let data = fs.readFileSync(srcfile);
    fs.writeFileSync(dstfile, data);
}

module.exports = {
    LoadJson,
    SetEditor,
    TransPath,
    WriteText,
    MkDir,
    CopyFile,
}