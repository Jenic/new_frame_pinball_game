
var util = require('./nUtil.js')
var fs = require('fs');

var _isEditor = false
function printLog(log) {
    _isEditor ? Editor.log(log) : console.log(log)
}

function printErr(log) {
    _isEditor ? Editor.failed(log) : console.error(log);
}

function ContrastFileName(filename, start, end) {
    let minLen = start.length > end.length ? start.length : end.length;
    if (filename.length > minLen) {
        let result = (filename.startsWith(start) && filename.endsWith(end));
        return result;
    }
    return false;
}


function FixNum(num) {
    let snum = `${num}`;
    if (snum.length < 2) {
        return `0${snum}`;
    }
    return snum;
}

function TimeFlag() {
    var date = new Date();
    let YYYY = FixNum(date.getFullYear());
    let MM = FixNum(date.getMonth() + 1);
    let dd = FixNum(date.getDate());
    let hh = FixNum(date.getHours());
    let mm = FixNum(date.getMinutes());
    var ver_time = `${YYYY}-${MM}${dd}-${hh}${mm}`;
    return ver_time;
}

function ReadFiles(dir, paths = []) {
    try {
        const files = fs.readdirSync(dir)
        // printlog(dir)
        files.forEach(filename => {
            // printLog(dir);
            // const fullPath = fspath.join(dir, filename)
            const fullPath = `${util.TransPath(dir)}/${filename}`;
            const stat = fs.statSync(fullPath)
            if (stat.isDirectory()) {
                ReadFiles(fullPath, paths)
            } else {
                paths.push(fullPath)
            }
        })
    } catch (e) {
        printErr(e);
    }
}

function MkDir(path) {
    // Editor.log(`创建文件夹: ${path}`);
    try {
        fs.mkdirSync(path, { recursive: true }, (err) => {
            if (err) {
                throw err;
            } else {
                // Editor.log(`创建文件夹-ok: ${path}`);
            }
        });
    } catch (e) {
        Editor.failed(e);
    }
}

function CopyFile(srcfile, dstfile) {
    if (!dstfile || 0 == dstfile.length) {
        return;
    }
    // Editor.log(`复制文件, ${srcfile} 到 ${dstfile}`);
    dstfile = util.TransPath(dstfile);
    if (dstfile.endsWith('/')) {
        dstfile = dstfile.substring(0, dstfile.length - 1);
    }
    // Editor.log(`复制文件到 ${dstfile}`);
    let lastIdx = dstfile.lastIndexOf("/");
    let fileName = dstfile.substring(lastIdx, dstfile.length);
    printLog(`复制文件:${fileName}\n到:${dstfile}`);
    // Editor.log(`lastIdx:${lastIdx}, 目录:${dstfile}`);
    let folder = dstfile.substring(0, lastIdx);
    // Editor.log(`转换后: ${folder}`);
    MkDir(folder);
    let data = fs.readFileSync(srcfile);
    fs.writeFileSync(dstfile, data);
}


function LoadConfigs() {
    let jsonPath = `${util.TransPath(__dirname)}/channel_cfg.json`;
    let savePath = `${util.TransPath(__dirname)}/save_cfg.json`;
    let cfg = util.LoadJson(jsonPath);
    let saveData = util.LoadJson(savePath);
    let useCfg = cfg.channels[saveData.use];

    return { cfg: cfg, save: saveData, use: useCfg };
}

function CopyBuildTemplates(dest, folder, suffix) {
    suffix = suffix || '';
    let folderTemplates = `${util.TransPath(__dirname)}/build-templates/${folder + suffix}/`;
    printLog(`模板目录: ${folderTemplates}`);
    let folderDest = `${dest}/`;
    //! 复制模板文件
    let filesInTemplates = [];
    ReadFiles(folderTemplates, filesInTemplates);
    let copyCnt = 0;
    for (let i = 0; i < filesInTemplates.length; ++i) {
        let url = filesInTemplates[i];
        let tmp = url.replace(folderTemplates, '');
        let dstUrl = `${folderDest}${tmp}`;
        if (!dstUrl.endsWith("/.DS_Store")) {
            CopyFile(url, dstUrl);
            copyCnt += 1;
        }
    }
    printLog(`复制文件:${copyCnt}个`);
}

function TransPathEx(path) {
    path = util.TransPath(path);
    if (path.endsWith('/')) {
        return path.substring(0, path.length - 1);
    }
    return path;
}


function OnNativeBuildCompleted(dest) {
    let data = LoadConfigs();
    let cfg = data.cfg;
    let useCfg = data.use;
    printLog(`使用配置:${JSON.stringify(useCfg)}`);
    let suffix = useCfg.folderSuffix;
    CopyBuildTemplates(dest, 'native', suffix);

    //! 读取文件夹
    let files = [];
    ReadFiles(dest, files);
    printLog(`读取文件夹数量:${files.length}`);
    let timeFlag = TimeFlag();
    for (let i = 0; i < files.length; ++i) {
        let tmp = TransPathEx(files[i]);
        if (tmp.endsWith('/AppCfg.java')) {
            //! 替换配置信息
            let txt = fs.readFileSync(tmp).toString();
            txt = txt.replace("{{__build_time__}}", `${timeFlag}`);
            let keys = Object.keys(useCfg);
            for (let ik = 0; ik < keys.length; ++ik) {
                tmpkey = keys[ik];
                tmpval = useCfg[tmpkey];
                txt = txt.replace(`{{__${tmpkey}__}}`, tmpval);
            }
            // printLog(txt);
            util.WriteText(tmp, txt);
        }
    }
}

//! 处理index.html - web-mobile - /Users/liujun/GitSpace/pw_playworld/build/web-mobile
function OnBuildCompleted(isEditor, dest) {
    _isEditor = isEditor;
    printLog(`目标文件夹: ${dest}`);

    let disposed = false;
    let flag = '';
    let path = util.TransPath(dest);
    let elmts = path.split('/');
    if (elmts.length > 1) {
        flag = elmts[elmts.length - 1];
        // printLog(`flag = ${flag}`);
        switch (flag) {
            case 'web-mobile':
                disposed = true;
                OnWebmobileBuildCompleted(dest, flag);
                break;
            case 'jsb-default':
                OnNativeBuildCompleted(dest)
                disposed = true;
                break;
            case 'wechatgame':
                OnWechatGameBuildCompleted(dest);
                disposed = true;
            case 'jsb-link':
                OnNativeBuildCompleted(dest);
                disposed = true;
                break;
            default:
                break;
        }
    } else {
        printErr(`dest.elmts error`)
    }
    let timeFlag = TimeFlag();
    if (!disposed) {
        printLog(`未处理的目标编译: ${flag}, 时间: ${timeFlag}`);
    } else {
        printLog(`${flag} 处理完成: ${timeFlag}`);
    }
}

module.exports = {
    OnBuildCompleted,
}