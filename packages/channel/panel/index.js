// panel/index.js, this filename needs to match the one registered in package.json
var gPanel = null;
var gJdata = null;
var gId2data = {};


Editor.Panel.extend({
  // css style for panel
  style: `
    :host { margin: 5px; }
    h2 { color: #f90; }
  `,

  // html template for panel
  template: `
    <h2>APP渠道选择</h2>
    <hr />
    <select v-model="selCh" id="idSelCh" style="width:170px" @change="onSelIpAddr()"></select>
    <hr />
  `,

  // element and variable binding
  $: {
    idSelCh: '#idSelCh',
  },

  // method executed when template and styles are successfully loaded and initialized
  ready() {
    gPanel = this;

    vueWin = new window.Vue({
      el: this.shadowRoot,
      data: {
        selCh: '',
        atFolder: '',
        version: '',
      },
      created() {
        Editor.Ipc.sendToMain('channel:dlg_opened');
      },
      methods: {
        onSelIpAddr(event, ...args) {
          let tmpData = gId2data[this.selCh];
          Editor.Ipc.sendToMain('channel:printlog', `sel_host:${JSON.stringify(tmpData.key)}`);
          Editor.Ipc.sendToMain('channel:save_data', 'cfg_name', tmpData.key);
        },
        printlog(log) {
          Editor.Ipc.sendToMain('channel:printlog', `${log}`);
        }
      },
    });//! end of vue
  },
  addHost(name, val, selected) {
    this.$idSelCh.options.add(new Option(name, val, selected, selected));
  },
  // register your ipc messages here
  messages: {
    'channel:settings'(event, ...args) {
      gId2data = {};
      let jdata = args && args.length > 0 ? args[0] : null;
      let sData = args && args.length > 1 ? args[1] : null;
      gJdata = jdata;
      // this.$input.value = jdata.version;
      let channels = jdata ? jdata.channels : {};
      let useCfg = sData ? sData.use : "";

      let keys = Object.keys(channels);
      Editor.Ipc.sendToMain('channel:printlog', `${JSON.stringify(keys)}`);
      for (let i = 0; i < keys.length; ++i) {
        let tmpkey = keys[i];
        let tmpdata = channels[tmpkey];
        let selected = useCfg === tmpkey;
        this.addHost(tmpkey, i, selected);
        gId2data[i] = { key: tmpkey, data: tmpdata };
      }
    },//! end of build_es:settings
    'channel:hello'(event) {
      
    }
  }
});