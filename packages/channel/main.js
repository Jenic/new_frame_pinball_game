'use strict';

var util = require('./nUtil.js')
var bc = require('./build_completed')
var os = require('os');

var json_path = '';
var save_path = '';

var KEY_VERSION = 'version'
var KEY_CFGNAME = 'cfg_name'

function SaveData(key, val) {
  Editor.log(`保存数据, key=${key}, val=${val}`);
  if (key === KEY_VERSION || key === KEY_CFGNAME) {
    //! 保存配置
    if (key === KEY_CFGNAME) {
      let sdata = util.LoadJson(save_path);
      if (!sdata) {
        sdata = {};
      }
      sdata.use = val;
      let jsonTxt = JSON.stringify(sdata);
      util.WriteText(save_path, `${jsonTxt}`);
      Editor.log(`保存数据AppUseData:\n${jsonTxt}`);
    }
    else {
      let jdata = util.LoadJson(json_path);
      if (!jdata) {
        Editor.log(`数据配置不存在`);
        return;
      }
      let saveJson = true;
      //! 处理version修改
      if (key === KEY_VERSION) {
        jdata.version = val;
      } else if (key === KEY_CFGNAME) {
        if (jdata.use !== val) {
          jdata.use = val;
          saveJson = true;
        }
      }

      if (saveJson) {
        let jsonTxt = JSON.stringify(jdata);
        util.WriteText(json_path, `${jsonTxt}`);
        Editor.log(`保存数据:\n${jsonTxt}`);
      }
    }
  }//! end if conditions 
}//! SaveData

function OnBuildStart() {
  //! 获取当前路径并转换字符串
  Editor.failed(`>.... 构建开始...:${os.platform()}`);
}

function OnBuildFinished(dest) {
  let sdata = util.LoadJson(save_path);
  Editor.failed(`>.... 构建完成(copy:${sdata.copy})...:${os.platform()}||${dest}`);

  if (sdata.copy) {
    bc.OnBuildCompleted(true, dest);
  }
}

module.exports = {
  load() {
    // execute when package loaded
    util.SetEditor(Editor);
    json_path = util.TransPath(`${__dirname}/channel_cfg.json`);
    save_path = util.TransPath(`${__dirname}/save_cfg.json`);
  },

  unload() {
    // execute when package unloaded
  },

  // register your ipc messages here
  messages: {
    // 'editor:build-start'() {
    //   // Editor.failed(">.... 构建开始.");
    //   OnBuildStart();
    //   // console.log(">.... 构建开始.");
    // },
    'editor:build-finished'(event, ...args) {
      // Editor.failed(`">.... 构建完成." ${JSON.stringify(event)}, args: ${JSON.stringify(args[0].dest)}`);
      OnBuildFinished(args && args.length > 0 ? args[0].dest : "");
    },
    'open'() {
      // open entry panel registered in package.json
      Editor.Panel.open('channel');
    },
    'printlog'(event, ...args) {
      Editor.log(args[0]);
    },
    'save_data'(event, ...args) {
      Editor.log(`SAVE_DATA:${JSON.stringify(args)}`);
      if (args && args.length >= 2) {
        SaveData(args[0], args[1]);
      }
    },
    //! 对话框已打开
    'dlg_opened'(event) {
      Editor.log(`JsonPath: ${json_path}`);
      Editor.log(`SavePath: ${save_path}`);
      let jdata = util.LoadJson(json_path);
      let saveData = util.LoadJson(save_path);
      Editor.Ipc.sendToPanel('channel', 'channel:settings', jdata, saveData);
    },
    'say-hello'() {
      Editor.log('Hello World!');
      // send ipc message to panel
      Editor.Ipc.sendToPanel('channel', 'channel:hello');
    },
    'clicked'() {
      Editor.log('Button clicked!');
    }
  },
};