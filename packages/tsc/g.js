var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var f;
(function (f) {
    var Action = /** @class */ (function () {
        function Action() {
        }
        /**
         * 节点渐入
         * @param option 动作参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        Action.FadeIn = function (option) {
            var duration = option.duration || 1;
            var func = cc.fadeIn(duration);
            this.RunAction(option, func);
        };
        /**
         * 节点渐出
         * @param option 动作参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        Action.FadeOut = function (option) {
            var duration = option.duration || 1;
            var func = cc.fadeOut(duration);
            this.RunAction(option, func);
        };
        /**
         * 节点渐出
         * @param option 动作参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         * @param option.x 目标缩放(x和y)
         */
        Action.ScaleTo = function (option) {
            var duration = option.duration || 1;
            var scale = option.x || 1;
            var func = cc.scaleTo(duration, scale);
            this.RunAction(option, func);
        };
        Action.MoveTo = function (option) {
            var duration = option.duration || 1;
            var x = option.x || option.target.x;
            var y = option.y || option.target.y;
            var func = cc.moveTo(duration, x, y);
            this.RunAction(option, func);
        };
        /**
         * 节点 移动
         * @param option 动作配置参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        Action.MoveBy = function (option) {
            var duration = option.duration || 1;
            var x = option.x || 0;
            var y = option.y || 0;
            var func = cc.moveBy(duration, x, y);
            this.RunAction(option, func);
        };
        /**
         * 节点 渐显+形变
         * @param option 动作配置参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.x x 位移
         * @param option.y 位移
         * @param option.cb 动作回调,动作执行结束后调用
         * @param shrink 是否是缩小, 默认false:扩大进入
         */
        Action.ScaleFadeIn = function (option, shrink) {
            // node: cc.Node, duration: number, cb?: () => void
            var duration = option.duration || 1;
            var startScale = 0.2;
            if (shrink) {
                startScale = 2;
            }
            option.target.scale = startScale;
            option.target.opacity = 0;
            var func = cc.spawn(cc.scaleTo(duration, 1).easing(cc.easeBackOut()), cc.fadeIn(duration));
            this.RunAction(option, func);
        };
        /**
         * 节点 渐隐+形变
         * @param option 动作配置参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         * @param enlarge 是否是扩大, 默认false,缩小退出
         */
        Action.ScaleFadeOut = function (option, enlarge) {
            // node: cc.Node, duration: number, cb?: () => void
            var duration = option.duration || 1;
            var endScale = 0.2;
            if (enlarge) {
                endScale = 2;
            }
            // option.target.scale = 1;
            // option.target.opacity = 0;
            var func = cc.spawn(cc.scaleTo(duration, endScale).easing(cc.easeBackIn()), cc.fadeOut(duration));
            this.RunAction(option, func);
        };
        Action.RunAction = function (option, func) {
            var node = option.target;
            var delay = option.delay;
            var cb = option.cb || (function () { });
            delay = delay || 0;
            if (option.reset) {
                node.stopAllActions();
            }
            node.runAction(cc.sequence(cc.delayTime(delay), func, cc.callFunc(cb)));
        };
        return Action;
    }());
    f.Action = Action;
    var Wait = /** @class */ (function () {
        function Wait(cb) {
            this._targetHandler = function () { };
            this._completed = {};
            this._targetHandler = cb;
        }
        Object.defineProperty(Wait.prototype, "isAllReady", {
            get: function () {
                for (var i in this._completed) {
                    if (Object.prototype.hasOwnProperty.call(this._completed, i)) {
                        var el = this._completed[i];
                        if (!el)
                            return false;
                    }
                }
                return true;
            },
            enumerable: false,
            configurable: true
        });
        Wait.Alloc = function (cb) {
            return new Wait(cb ? cb : function () { });
        };
        Wait.prototype.WaitFor = function (str) {
            if (str instanceof Array) {
                for (var i = 0; i < str.length; i++) {
                    var el = str[i];
                    this.WaitFor(el);
                }
            }
            else {
                this._completed[str] = false;
            }
        };
        Wait.prototype.Ready = function (str) {
            this._completed[str] = true;
            if (this.isAllReady) {
                console.log('OK!!!!');
                this._targetHandler();
                this._targetHandler = function () { };
            }
        };
        return Wait;
    }());
    f.Wait = Wait;
    var Seq = /** @class */ (function () {
        function Seq() {
            this._sequence = [];
            this._immediately = null;
            var array = arguments[0];
            this._sequence = array || [];
        }
        Seq.Alloc = function () {
            return new Seq();
        };
        Seq.prototype.then = function (callback, context, endState) {
            var a = callback;
            var b = endState;
            if (context) {
                a = callback && callback.bind(context);
                b = endState && endState.bind(context);
            }
            this._sequence.push({ a: a, b: b });
            return this;
        };
        Seq.prototype.run = function () {
            this.resolve();
            return this;
        };
        Seq.prototype.resolve = function () {
            var next = this.getnext();
            if (!next) {
                return true;
            }
            if (next.a && typeof next.a === 'function') {
                next.a(this.resolve.bind(this));
            }
            this._immediately = next.b;
        };
        Seq.prototype.curFinish = function () {
            if (typeof this._immediately === 'function') {
                this._immediately();
                this._immediately = null;
            }
            return this.resolve();
        };
        Seq.prototype.getnext = function () {
            var a = this._sequence.shift();
            return a;
        };
        Seq.prototype.clear = function () {
            this._sequence = [];
        };
        return Seq;
    }());
    f.Seq = Seq;
})(f || (f = {}));
var f;
(function (f) {
    var Arr = /** @class */ (function () {
        function Arr() {
        }
        Arr.remove = function (array, value) {
            var index = array.indexOf(value);
            if (index >= 0) {
                this.removeAt(array, index);
                return true;
            }
            else {
                return false;
            }
        };
        Arr.fastRemove = function (array, value) {
            var index = array.indexOf(value);
            if (index >= 0) {
                array[index] = array[array.length - 1];
                --array.length;
            }
        };
        Arr.removeAt = function (array, index) {
            array.splice(index, 1);
        };
        Arr.fastRemoveAt = function (array, index) {
            var length = array.length;
            if (index < 0 || index >= length) {
                return;
            }
            array[index] = array[length - 1];
            array.length = length - 1;
        };
        Arr.contains = function (array, value) {
            return array.indexOf(value) >= 0;
        };
        Arr.verifyType = function (array, type) {
            if (array && array.length > 0) {
                for (var i = 0; i < array.length; i++) {
                    if (!(array[i] instanceof type)) {
                        return false;
                    }
                }
            }
            return true;
        };
        Arr.copy = function (array) {
            var i, len = array.length, arr_clone = new Array(len);
            for (i = 0; i < len; i += 1)
                arr_clone[i] = array[i];
            return arr_clone;
        };
        return Arr;
    }());
    f.Arr = Arr;
})(f || (f = {}));
var f;
(function (f) {
    var Event = /** @class */ (function () {
        function Event() {
            this._map = new Map();
        }
        Event.prototype.On = function (key, cb, caller, once) {
            var arr = this._map.get(key) || [];
            for (var i = 0; i < arr.length; i++) {
                var el = arr[i];
                if (el.target === caller && el.handler === cb) {
                    console.log('事件已注册，跳过');
                    return;
                }
            }
            var info = {
                target: caller,
                handler: cb,
                once: once
            };
            arr.push(info);
            this._map.set(key, arr);
        };
        Event.prototype.Once = function (key, cb, caller) {
            this.On(key, cb, caller, true);
        };
        Event.prototype.Off = function (key, cb, caller) {
            var arr = this._map.get(key);
            for (var i = 0; i < arr.length; i++) {
                var el = arr[i];
                if (el.target === caller && el.handler === cb) {
                    f.Arr.remove(arr, el);
                    return;
                }
            }
        };
        Event.prototype.Emit = function (key, data) {
            var handlers = this._map.get(key) || [];
            for (var i = 0; i < handlers.length; i++) {
                var el = handlers[i];
                if (typeof el.handler === 'function') {
                    el.handler.call(el.target, data);
                    if (el.once) {
                        this.Off(key, el.handler, el.target);
                    }
                }
            }
        };
        Event.prototype.Clear = function () {
            this._map = new Map();
        };
        return Event;
    }());
    f.Event = Event;
    var GEvent = /** @class */ (function () {
        function GEvent() {
        }
        GEvent.On = function (type, handler, target, once) {
            this._e.On(type, handler, target, once);
        };
        GEvent.Once = function (type, handler, target) {
            this._e.Once(type, handler, target);
        };
        GEvent.Off = function (type, handler, target) {
            this._e.Off(type, handler, target);
        };
        GEvent.Emit = function (type, data) {
            this._e.Emit(type, data);
        };
        GEvent.Clear = function () {
            this._e.Clear();
        };
        GEvent._e = new Event();
        return GEvent;
    }());
    f.GEvent = GEvent;
    var EventType;
    (function (EventType) {
        EventType["PageClose"] = "page_close";
        EventType["ClickBtnClose"] = "click_btn_close";
        EventType["ListItemUpdate"] = "list_item_update";
        EventType["ListItemContentVisible"] = "list_item_content_visible";
        EventType["ViewBaseAddListener"] = "view_base_add_listener";
        EventType["ViewBaseRemoveListener"] = "view_base_remove_listener";
        EventType["ViewBaseSetEventTarget"] = "view_base_set_event_target";
    })(EventType = f.EventType || (f.EventType = {}));
})(f || (f = {}));
/// <reference path="../../creator.d.ts" />
var f;
(function (f) {
    var Sets;
    (function (Sets) {
        var Queue = /** @class */ (function () {
            function Queue(arr) {
                this._arr = arr || [];
            }
            Queue.prototype.get = function () {
                return this._arr.shift();
            };
            Queue.prototype.set = function (obj) {
                this._arr.push(obj);
            };
            Queue.prototype.clear = function () {
                this._arr = [];
            };
            Object.defineProperty(Queue.prototype, "size", {
                get: function () {
                    return this._arr.length;
                },
                enumerable: false,
                configurable: true
            });
            return Queue;
        }());
        Sets.Queue = Queue;
        var Stack = /** @class */ (function () {
            function Stack(arr) {
                this._arr = arr;
            }
            Stack.prototype.get = function () {
                return this._arr.pop();
            };
            Stack.prototype.set = function (obj) {
                this._arr.push(obj);
            };
            Stack.prototype.clear = function () {
                this._arr = [];
            };
            Object.defineProperty(Stack.prototype, "size", {
                get: function () {
                    return this._arr.length;
                },
                enumerable: false,
                configurable: true
            });
            return Stack;
        }());
        Sets.Stack = Stack;
    })(Sets = f.Sets || (f.Sets = {}));
    // 简易 Tween
    var Tween = /** @class */ (function () {
        function Tween(obj) {
            this.states = new Sets.Queue();
            this.Timer = null;
            this.startObj = obj;
        }
        Tween.prototype.to = function (duration, obj, progress) {
            this.states.set(new TweenState(duration, obj, progress));
            return this;
        };
        Tween.prototype.call = function (callback) {
            this.states.set(new TweenCallback(callback));
        };
        Tween.prototype.start = function () {
            this._timeStamp = new Date().getTime();
            this.goOn();
            return this;
        };
        Tween.prototype.stop = function () {
            clearTimeout(this.Timer);
        };
        Tween.prototype.goOn = function () {
            var _this = this;
            if (this._currentState instanceof TweenState) {
                if (this._curTime > this._maxTime) {
                    var progress = this._currentState._progress;
                    if (typeof progress === 'function') {
                        this._curObj = this._currentState._target;
                        this.startObj = this._currentState._target;
                        progress(this._currentState._target, 1);
                    }
                    this.nextState();
                    return;
                }
                else {
                    var that_1 = this;
                    this.Timer = setTimeout(function () {
                        var now = new Date().getTime();
                        var dt = now - _this._timeStamp;
                        _this._timeStamp = now;
                        that_1._curTime += dt / 1000;
                        var state = that_1._currentState;
                        var progress = state._progress;
                        if (typeof progress === 'function') {
                            var Obj = {};
                            var ratio = that_1._curTime / that_1._maxTime;
                            that_1._curObj = Obj;
                            var keys = Object.keys(_this.startObj);
                            for (var i = 0; i < keys.length; i++) {
                                var el = keys[i];
                                var value1 = _this.startObj[el];
                                var value2 = _this.endObj[el];
                                if (typeof value1 === 'number' && typeof value2 === 'number') {
                                    var value = value1 + (value2 - value1) * ratio;
                                    Obj[el] = value;
                                }
                                if (typeof value1 === 'string' && typeof value2 === 'string') {
                                    var oLength = value1.length;
                                    var offset = value2.length - value1.length;
                                    Obj[el] = value2.slice(0, oLength + offset * ratio);
                                }
                            }
                            progress(Obj, ratio);
                        }
                        that_1.goOn();
                    }, 1000 / Tween.FPS);
                }
                return;
            }
            if (this._currentState instanceof TweenCallback) {
                this._currentState._callback();
                this.nextState();
                return;
            }
            this.nextState();
        };
        Tween.prototype.nextState = function () {
            if (this.states.size > 0) {
                var state = this.states.get();
                if (state instanceof TweenState) {
                    this._maxTime = state._duration;
                    this._curTime = 0;
                    this.endObj = state._target;
                }
                this._currentState = state;
                this.goOn();
            }
        };
        Tween.FPS = 60;
        return Tween;
    }());
    f.Tween = Tween;
    var TweenState = /** @class */ (function () {
        function TweenState(duration, target, progress) {
            this._duration = duration;
            this._target = target;
            this._progress = progress;
        }
        return TweenState;
    }());
    var TweenCallback = /** @class */ (function () {
        function TweenCallback(callback) {
            this._callback = callback;
        }
        return TweenCallback;
    }());
})(f || (f = {}));
var f;
(function (f) {
    var Http = /** @class */ (function () {
        function Http() {
        }
        Http.Get = function (url, callback) {
            var xhr = cc.loader.getXMLHttpRequest(); //new XMLHttpRequest();//
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var respone = xhr.responseText;
                    callback(respone);
                }
                else {
                }
            };
            xhr.open("GET", url, true);
            if (cc.sys.isNative) {
                xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
            }
            // note: In Internet Explorer, the timeout property may be set only after calling the open()
            // method and before calling the send() method.
            xhr.timeout = 5000; // 5 seconds for timeout
            xhr.send();
        };
        Http.POST = function (url, data, callback) {
            var xhr = cc.loader.getXMLHttpRequest(); //new XMLHttpRequest();//
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var respone = xhr.responseText;
                    callback(respone);
                }
                else {
                }
            };
            xhr.open("POST", url, true);
            if (cc.sys.isNative) {
                xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
            }
            xhr.setRequestHeader("Content-Type", "application/json");
            // note: In Internet Explorer, the timeout property may be set only after calling the open()
            // method and before calling the send() method.
            xhr.timeout = 5000; // 5 seconds for timeout
            xhr.send(JSON.stringify(data));
        };
        return Http;
    }());
    f.Http = Http;
})(f || (f = {}));
var f;
(function (f) {
    var NodeMgr = /** @class */ (function () {
        function NodeMgr() {
        }
        NodeMgr.AddSingletonChild = function (parent, name) {
            var node = parent.getChildByName(name);
            if (!cc.isValid(node)) {
                node = new cc.Node();
                node.name = name;
                node.parent = parent;
            }
            return node;
        };
        NodeMgr.AddSingletonComponent = function (node, type) {
            var comp = node.getComponent(type);
            if (!cc.isValid(comp)) {
                comp = node.addComponent(type);
            }
            return comp;
        };
        NodeMgr.CreateNodePool = function (str) {
            return new cc.NodePool(str);
        };
        NodeMgr.SetPageParam = function (node, param) {
            node[f.UI.DIALOG_INFO] = param;
        };
        NodeMgr.GetPageParam = function (node) {
            return node[f.UI.DIALOG_INFO];
        };
        NodeMgr.Visible = function (target, active, option) {
            if (!cc.isValid(target)) {
                console.warn('#SetImg ：target 无效 ');
                return;
            }
            var duration = option && option.duration || 0;
            var cb = option && option.cb || (function () { });
            if (duration > 0) {
                if (active) {
                    f.Action.ScaleFadeIn({ target: target, duration: duration, cb: cb });
                }
                else {
                    f.Action.ScaleFadeOut({ target: target, duration: duration, cb: cb });
                }
            }
            else {
                target.active = active;
                target.scale = active ? 1 : 0;
                target.opacity = active ? 255 : 0;
            }
        };
        return NodeMgr;
    }());
    f.NodeMgr = NodeMgr;
})(f || (f = {}));
var f;
(function (f) {
    var Res = /** @class */ (function () {
        function Res() {
        }
        Res.DEBUG = function () {
            return { viewCache: this.viewCache, dialogCache: this.dialogCache };
        };
        Res.GetTransEff = function (str) {
            // TODO 获取cocos 材质
            str;
            return;
        };
        /**
         * 资源预加载
         * @param prelaodInfo 预加载内容
         * @param prelaodInfo.comonConfirm 默认确认框
         * @param prelaodInfo.tip 默认tips提示
         */
        Res.Preload = function (prelaodInfo) {
            this.defaultDialog = prelaodInfo.comonConfirm;
            this.defaultTip = prelaodInfo.tip;
        };
        Res.LoadCommonDialog = function (node) {
            this.defaultDialog = node;
        };
        // -------------------------- 界面加载相关
        Res.LoadPage = function (option, cb) {
            var node = this.GetViewCache(option.name);
            if (cc.isValid(node)) {
                cb(node);
                return;
            }
            if (option.bundleName) {
                this.LoadBundle(option.bundleName, function (bundle) {
                    bundle.load(option.name, cc.Prefab, function (err, res) {
                        if (err) {
                            console.log("#Bundle LoadPage: Error ", err);
                            return;
                        }
                        var node = cc.instantiate(res);
                        node.name = option.name;
                        cb(node);
                    });
                });
            }
            else {
                var path = f.Util.GetUrl(option.path, option.name);
                cc.loader.loadRes(path, cc.Prefab, function (err, res) {
                    if (err) {
                        console.log("#LoadPage: Error ", err);
                        return;
                    }
                    var node = cc.instantiate(res);
                    node.name = option.name;
                    cb(node);
                });
            }
        };
        Res.LoadDialog = function (option, cb) {
            var name = option.name || this.DEFAULT_DIALOG_NAME;
            var node = this.GetDialogCache(name);
            if (cc.isValid(node)) {
                cb(node);
                return;
            }
            if (name === this.DEFAULT_DIALOG_NAME) {
                var dialog = cc.instantiate(this.defaultDialog);
                cb(dialog);
                return;
            }
            if (option.bundleName) {
                this.LoadBundle(option.bundleName, function (bundle) {
                    bundle.load(option.name, cc.Prefab, function (err, res) {
                        if (err) {
                            console.log("#Bundle LoadDialog: Error ", err);
                            return;
                        }
                        cb(cc.instantiate(res));
                    });
                });
            }
            else {
                var path = f.Util.GetUrl(option.path, option.name);
                cc.loader.loadRes(path, cc.Prefab, function (err, res) {
                    if (err) {
                        console.log("#LoadDialog: Error ", err);
                        return;
                    }
                    cb(cc.instantiate(res));
                });
            }
        };
        Res.LoadTip = function () {
            var node = this.GetTipCache();
            if (cc.isValid(node)) {
                return node;
            }
            var tip = cc.instantiate(this.defaultTip);
            return tip;
        };
        Res.LoadBundle = function (name, cb) {
            cc.assetManager.loadBundle(name, function (err, bundle) {
                if (err) {
                    console.log("#LoadBundle: Error", err);
                    return;
                }
                cb(bundle);
            });
        };
        Res.TakePage = function (node) {
            this.CacheView(node);
        };
        Res.TakeDialog = function (node) {
            this.CacheDialog(node);
        };
        Res.TakeTip = function (node) {
            this.CacheTip(node);
        };
        Res.GetDialogCache = function (dialogName) {
            for (var i = 0; i < this.dialogCache.length; i++) {
                var el = this.dialogCache[i];
                if (el.name === dialogName) {
                    f.Arr.remove(this.dialogCache, el);
                    return el;
                }
            }
            return null;
        };
        Res.CacheDialog = function (node) {
            this.dialogCache.push(node);
            node.removeFromParent(false);
            node.active = false;
            if (this.dialogCache.length > this._dialogCacheSize) {
                var delNode = this.dialogCache.shift();
                if (cc.isValid(delNode)) {
                    delNode.destroy();
                }
            }
        };
        Res.GetViewCache = function (pageName) {
            for (var i = 0; i < this.viewCache.length; i++) {
                var el = this.viewCache[i];
                if (el.name === pageName) {
                    f.Arr.remove(this.viewCache, el);
                    return el;
                }
            }
            return null;
        };
        Res.CacheView = function (node) {
            this.viewCache.push(node);
            node.removeFromParent(false);
            node.active = false;
            if (this.viewCache.length > this._viewCacheSize) {
                var delNode = this.viewCache.shift();
                if (cc.isValid(delNode)) {
                    delNode.destroy();
                }
            }
        };
        Res.GetTipCache = function () {
            return this.tipChache.shift();
        };
        Res.CacheTip = function (node) {
            this.tipChache.push(node);
            node.removeFromParent(false);
            node.active = false;
            if (this.tipChache.length > this._tipChacheSize) {
                var delNode = this.tipChache.shift();
                if (cc.isValid(delNode)) {
                    delNode.destroy();
                }
            }
        };
        // ---------------------------------------------------
        Res.GetStorage = function (key) {
            return cc.sys.localStorage.getItem(key);
        };
        Res.SetStorage = function (key, value) {
            cc.sys.localStorage.setItem(key, value);
        };
        Res.GetDefaultSprite = function () {
            var texture = new cc.RenderTexture();
            texture.initWithData(new Uint8Array([0, 0, 0]), cc.Texture2D.PixelFormat.RGB888, 1, 1);
            var sp = new cc.SpriteFrame(texture);
            return sp;
        };
        Res.viewCache = new Array();
        Res._viewCacheSize = 10;
        Res.dialogCache = new Array();
        Res._dialogCacheSize = 10;
        Res.tipChache = new Array();
        Res._tipChacheSize = 10;
        Res.DEFAULT_DIALOG_NAME = '_defaultDialogName_';
        return Res;
    }());
    f.Res = Res;
})(f || (f = {}));
var f;
(function (f_1) {
    var INF = 1e10;
    var ShaderUtil = /** @class */ (function () {
        function ShaderUtil() {
        }
        Object.defineProperty(ShaderUtil, "cameraNode", {
            get: function () {
                var node;
                if (this.nodePool.size() > 0) {
                    node = this.nodePool.get();
                }
                else {
                    node = new cc.Node;
                    var camera = node.addComponent(cc.Camera);
                    camera.backgroundColor = new cc.Color(255, 255, 255, 0); // 透明区域仍然保持透明，半透明区域和白色混合
                    camera.clearFlags = cc.Camera.ClearFlags.DEPTH | cc.Camera.ClearFlags.STENCIL | cc.Camera.ClearFlags.COLOR;
                    // 设置你想要的截图内容的 cullingMask
                    camera.cullingMask = 0xffffffff;
                }
                return node;
            },
            enumerable: false,
            configurable: true
        });
        ShaderUtil.snap = function (node, width, height) {
            // !当 node 带cc.Button 组件时，截图会出问题。。。
            if (!cc.isValid(node))
                return;
            node.active = true;
            var extend = 0; // 额外的截图范围
            width = width || node.width;
            height = height || node.height;
            // 使截屏处于被截屏对象中心（两者有同样的父节点 ）
            var cameraNode = this.cameraNode;
            var camera = cameraNode.getComponent(cc.Camera);
            cameraNode.x = (0.5 - node.anchorX) * width;
            cameraNode.y = (0.5 - node.anchorY) * height;
            cameraNode.parent = node;
            var success = false;
            var result = null;
            try {
                var scaleX = node.scaleX; //this.fitArea.scaleX;
                var scaleY = node.scaleY; //this.fitArea.scaleY;
                var gl = cc.game['_renderContext'];
                var targetWidth = Math.floor(width * scaleX + extend * 2); // texture's width/height must be integer
                var targetHeight = Math.floor(height * scaleY + extend * 2);
                // 内存纹理创建后缓存在目标节点上
                // 如果尺寸和上次不一样也重新创建
                var texture = new cc.RenderTexture();
                texture.initWithSize(targetWidth, targetHeight, gl.STENCIL_INDEX8);
                texture.packable = false;
                camera.alignWithScreen = false;
                // camera.orthoSize = root.height / 2;
                camera.orthoSize = targetHeight / 2;
                camera.targetTexture = texture;
                // 渲染一次摄像机，即更新一次内容到 RenderTexture 中
                camera.render(node);
                // let screenShot = target;
                success = true;
                result = texture;
            }
            finally {
                camera.targetTexture = null;
                this.nodePool.put(cameraNode);
                if (!success) {
                    // target.active = false;
                    console.log("截图失败");
                }
            }
            result.setFlipY(true);
            return result;
        };
        /**
         * 为包装节点 和其所有子节点 附加shader特效
         * @param node 包装节点
         * @param name 特效名称
         * @param option 设置
         */
        ShaderUtil.wrap = function (node, name, option, testNode) {
            if (!node) {
                cc.error('#ShaderUtil->warp(): node不存在：', node);
                return;
            }
            var material = f_1.Res.GetTransEff(name);
            if (!material) {
                cc.error('#ShaderUtil->warp(): 材质不存在：', name);
                return;
            }
            var handler = node.getChildByName(this.nodeName);
            if (!cc.isValid(handler)) {
                handler = new cc.Node();
                handler.parent = node;
                // 大小
                handler.height = node.height;
                handler.width = node.width;
                // 适配缩放
                handler.scaleY = -1 / node.scaleY;
                handler.scaleX = 1 / node.scaleX;
                // 适配锚点
                handler.anchorX = node.anchorX;
                handler.anchorY = 1 - node.anchorY;
                handler.name = this.nodeName;
            }
            var sprite = handler.getComponent(cc.Sprite);
            if (!cc.isValid(sprite)) {
                sprite = handler.addComponent(cc.Sprite);
            }
            var mat = sprite.setMaterial(0, material);
            var texture = ShaderUtil.snap(node);
            if (option) {
                if (option.SDF) {
                    var sdfRadius = Math.max(60, node.height / 3);
                    texture = ShaderUtil.RenderSDF(texture, sdfRadius, 0.5).texture;
                }
                if (option.color) {
                    mat.setProperty('color', [
                        option.color.r / 255,
                        option.color.g / 255,
                        option.color.b / 255,
                        option.color.a / 255
                    ]);
                }
                if (option.block) {
                    var block = handler.getComponent(cc.BlockInputEvents);
                    if (!cc.isValid(block)) {
                        block = handler.addComponent(cc.BlockInputEvents);
                    }
                    block.enabled = true;
                }
                else {
                    var block = handler.getComponent(cc.BlockInputEvents);
                    if (cc.isValid(block)) {
                        block.enabled = false;
                    }
                }
            }
            var sf = new cc.SpriteFrame(texture);
            sprite.spriteFrame = sf;
            if (cc.isValid(testNode)) {
                var comp = testNode.getComponent(cc.Sprite);
                comp.spriteFrame = sf;
            }
        };
        /**
         * 取消包装的特效
         * @param node 包装节点
         */
        ShaderUtil.unWrap = function (node) {
            var handler = node.getChildByName(this.nodeName);
            handler.destroy();
        };
        /**
         * 为精灵节点 添加特效，只影响其本身
         * @param node 精灵节点
         * @param name 特效名称
         */
        ShaderUtil.setShader = function (node, name) {
            if (!node) {
                cc.error('#ShaderUtil->setShader(): node不存在 :', node);
            }
            var sprite = node.getComponent(cc.Sprite);
            if (!sprite) {
                sprite = node.addComponent(cc.Sprite);
            }
            var material = f_1.Res.GetTransEff(name);
            sprite.setMaterial(0, material);
        };
        /**
         * 为节点node 取消其shader特效
         * @param node 精灵节点
         */
        ShaderUtil.resetShader = function (node) {
            this.setShader(node, 'default');
        };
        ShaderUtil.RenderSDF = function (texture, radius, cutoff) {
            var imgData = texture.readPixels();
            var width = texture.width;
            var height = texture.height;
            // initialize members
            // let cutoff = this.cutoff || 0.25;
            if (cutoff === undefined)
                cutoff = 0;
            radius = radius || this.radius || 18;
            var area = width * height;
            var longSide = Math.max(width, height);
            this.gridOuter = new Float64Array(area);
            this.gridInner = new Float64Array(area);
            this.f = new Float64Array(longSide);
            this.d = new Float64Array(longSide);
            this.z = new Float64Array(longSide + 1);
            this.v = new Int16Array(longSide);
            var alphaChannel = new Uint8ClampedArray(area);
            for (var i = 0; i < area; i++) {
                var a = imgData[i * 4 + 3] / 255; // alpha value
                this.gridOuter[i] = a === 1 ? 0 : a === 0 ? INF : Math.pow(Math.max(0, 0.5 - a), 2);
                this.gridInner[i] = a === 1 ? INF : a === 0 ? 0 : Math.pow(Math.max(0, a - 0.5), 2);
            }
            this.EDT(this.gridOuter, width, height, this.f, this.d, this.v, this.z);
            this.EDT(this.gridInner, width, height, this.f, this.d, this.v, this.z);
            for (i = 0; i < area; i++) {
                var d = this.gridOuter[i] - this.gridInner[i];
                alphaChannel[i] = Math.max(0, Math.min(255, Math.round(255 - 255 * (d / radius + cutoff))));
                // imgData[i * 4 + 0] = 255;
                // imgData[i * 4 + 1] = 255;
                // imgData[i * 4 + 2] = 255;
                imgData[i * 4 + 3] = alphaChannel[i];
            }
            var resultTexture = new cc.RenderTexture;
            resultTexture.initWithData(imgData, cc.Texture2D.PixelFormat.RGBA8888, width, height);
            return {
                texture: resultTexture,
                alpha: alphaChannel
            };
        };
        ;
        // 2D Euclidean distance transform by Felzenszwalb & Huttenlocher https://cs.brown.edu/~pff/dt/
        ShaderUtil.EDT = function (data, width, height, f, d, v, z) {
            for (var x = 0; x < width; x++) {
                for (var y = 0; y < height; y++) {
                    f[y] = data[y * width + x];
                }
                this.EDT1D(f, d, v, z, height);
                for (y = 0; y < height; y++) {
                    data[y * width + x] = d[y];
                }
            }
            for (y = 0; y < height; y++) {
                for (x = 0; x < width; x++) {
                    f[x] = data[y * width + x];
                }
                this.EDT1D(f, d, v, z, width);
                for (x = 0; x < width; x++) {
                    data[y * width + x] = Math.sqrt(d[x]);
                }
            }
        };
        // 1D squared distance transform
        ShaderUtil.EDT1D = function (f, d, v, z, n) {
            v[0] = 0;
            z[0] = -INF;
            z[1] = +INF;
            for (var q = 1, k = 0; q < n; q++) {
                var s = ((f[q] + q * q) - (f[v[k]] + v[k] * v[k])) / (2 * q - 2 * v[k]);
                while (s <= z[k]) {
                    k--;
                    s = ((f[q] + q * q) - (f[v[k]] + v[k] * v[k])) / (2 * q - 2 * v[k]);
                }
                k++;
                v[k] = q;
                z[k] = s;
                z[k + 1] = +INF;
            }
            for (q = 0, k = 0; q < n; q++) {
                while (z[k + 1] < q)
                    k++;
                d[q] = (q - v[k]) * (q - v[k]) + f[v[k]];
            }
        };
        ShaderUtil.nodePool = new cc.NodePool();
        ShaderUtil.nodeName = 'shader_util_node';
        return ShaderUtil;
    }());
    f_1.ShaderUtil = ShaderUtil;
})(f || (f = {}));
var f;
(function (f) {
    var Str = /** @class */ (function () {
        function Str() {
        }
        Str.format = function (tpl, data) {
            return tpl.replace(/\{\{(.+?)\}\}/g, function (m, m1) {
                m;
                return data[m1];
            });
        };
        // render('我是{{name}}，年龄{{age}}，性别{{sex}}',{
        //     name:'姓名',
        //     age:18,sex:'女',
        // }) 
        // // "我是姓名，年龄18，性别女"
        Str.Compress = function (strNormalString) {
            var strCompressedString = "";
            var ht = new Array();
            for (i = 0; i < 128; i++) {
                ht[i] = i;
            }
            var used = 128;
            var intLeftOver = 0;
            var intOutputCode = 0;
            var pcode = 0;
            var ccode = 0;
            var k = 0;
            for (var i = 0; i < strNormalString.length; i++) {
                ccode = strNormalString.charCodeAt(i);
                k = (pcode << 8) | ccode;
                if (ht[k] != null) {
                    pcode = ht[k];
                }
                else {
                    intLeftOver += 12;
                    intOutputCode <<= 12;
                    intOutputCode |= pcode;
                    pcode = ccode;
                    if (intLeftOver >= 16) {
                        strCompressedString += String.fromCharCode(intOutputCode >> (intLeftOver - 16));
                        intOutputCode &= (Math.pow(2, (intLeftOver - 16)) - 1);
                        intLeftOver -= 16;
                    }
                    if (used < 4096) {
                        used++;
                        ht[k] = used - 1;
                    }
                }
            }
            if (pcode != 0) {
                intLeftOver += 12;
                intOutputCode <<= 12;
                intOutputCode |= pcode;
            }
            if (intLeftOver >= 16) {
                strCompressedString += String.fromCharCode(intOutputCode >> (intLeftOver - 16));
                intOutputCode &= (Math.pow(2, (intLeftOver - 16)) - 1);
                intLeftOver -= 16;
            }
            if (intLeftOver > 0) {
                intOutputCode <<= (16 - intLeftOver);
                strCompressedString += String.fromCharCode(intOutputCode);
            }
            return strCompressedString;
        };
        Str.Decompress = function (strCompressedString) {
            var strNormalString = "";
            var ht = new Array();
            for (i = 0; i < 128; i++) {
                ht[i] = String.fromCharCode(i);
            }
            var used = 128;
            var intLeftOver = 0;
            var intOutputCode = 0;
            var ccode = 0;
            var pcode = 0;
            var key = 0;
            for (var i = 0; i < strCompressedString.length; i++) {
                intLeftOver += 16;
                intOutputCode <<= 16;
                intOutputCode |= strCompressedString.charCodeAt(i);
                while (1) {
                    if (intLeftOver >= 12) {
                        ccode = intOutputCode >> (intLeftOver - 12);
                        if (typeof (key = ht[ccode]) != "undefined") {
                            strNormalString += key;
                            if (used > 128) {
                                ht[ht.length] = ht[pcode] + String(key).substr(0, 1);
                            }
                            pcode = ccode;
                        }
                        else {
                            key = ht[pcode] + ht[pcode].substr(0, 1);
                            strNormalString += key;
                            ht[ht.length] = ht[pcode] + String(key).substr(0, 1);
                            pcode = ht.length - 1;
                        }
                        used++;
                        intLeftOver -= 12;
                        intOutputCode &= (Math.pow(2, intLeftOver) - 1);
                    }
                    else {
                        break;
                    }
                }
            }
            return strNormalString.trimEnd();
        };
        return Str;
    }());
    f.Str = Str;
})(f || (f = {}));
var f;
(function (f) {
    var UI = /** @class */ (function () {
        function UI() {
        }
        UI.GetLoadPageInfo = function (name) {
            var pageInfo;
            if (typeof name === 'string') {
                pageInfo = { name: name };
            }
            else {
                pageInfo = name;
            }
            return pageInfo;
        };
        UI.OpenMainPage = function (name) {
            var pageInfo = this.GetLoadPageInfo(name);
            f.Res.LoadPage(pageInfo, function (node) {
                MainPageController.Open(node, null);
            });
        };
        UI.OpenPage = function (name) {
            var pageInfo = this.GetLoadPageInfo(name);
            f.Res.LoadPage(pageInfo, function (node) {
                PageController.Open(node, null);
            });
        };
        UI.OpenDialog = function (name, param, eventTarget) {
            var pageInfo = this.GetLoadPageInfo(name);
            f.Res.LoadDialog(pageInfo, function (node) {
                f.NodeMgr.SetPageParam(node, param);
                DialogController.Open(node, eventTarget);
            });
        };
        UI.OpenDialogInline = function (name, param, eventTarget) {
            var pageInfo = this.GetLoadPageInfo(name);
            f.Res.LoadDialog(pageInfo, function (node) {
                f.NodeMgr.SetPageParam(node, param);
                DialogController.InlineOpen(node, eventTarget);
            });
        };
        UI.CloseDialog = function (name) {
            DialogController.CloseDialogByName(name);
        };
        /**
         *
         * @param option
         * @param option.txt 确认框内容
         * @param option.tittle 确认框标题
         * @param option.okcb 确认按钮回调
         * @param option.okTxt 确认按钮文本
         * @param option.cancelcb 取消按钮回调
         * @param option.cancelTxt 取消按钮回调
         */
        UI.Confirm = function (option) {
            var name = f.Res.DEFAULT_DIALOG_NAME;
            f.Res.LoadDialog({ name: name }, function (node) {
                f.NodeMgr.SetPageParam(node, option);
                DialogController.Open(node, null);
            });
        };
        UI.Tip = function (str) {
            TipsController.showTipsInLine(str);
        };
        /**
         * 为目标节点添加点击事件
         * @param target 目标节点
         * @param cb 点击回调
         * @param caller 作用域
         * @returns
         */
        UI.OnClick = function (target, cb, caller, option) {
            if (!cc.isValid(target)) {
                console.warn("#\u8282\u70B9\u65E0\u6548\uFF0C\u6DFB\u52A0\u70B9\u51FB\u4E8B\u4EF6\u5931\u8D25\uFF01");
                return;
            }
            if (caller)
                cb = cb.bind(caller);
            var x = target.scale;
            var zoom = option && option.zoom || 0.8;
            var duration = 0.05;
            // 防止多次点击
            var SINGLE = "__single_click";
            var block = target.getComponent(cc.BlockInputEvents);
            if (!block)
                target.addComponent(cc.BlockInputEvents);
            target.on(cc.Node.EventType.TOUCH_START, function () {
                if (target[SINGLE]) {
                    return;
                }
                target[SINGLE] = true;
                f.Action.ScaleTo({ target: target, duration: duration, x: x * zoom });
            });
            target.on(cc.Node.EventType.TOUCH_CANCEL, function () {
                target[SINGLE] = false;
                f.Action.ScaleTo({ target: target, duration: duration, x: x });
            });
            target.on(cc.Node.EventType.TOUCH_END, function () {
                target[SINGLE] = false;
                f.Action.ScaleTo({ target: target, duration: duration, x: x, cb: cb });
            });
        };
        UI.SetTxt = function (target, str) {
            if (!cc.isValid(target)) {
                console.warn('#SetTxt ：target 无效 ');
                return;
            }
            var label = target;
            if (label instanceof cc.Node) {
                var comp = label.getComponent(cc.Label);
                if (comp) {
                    label = comp;
                }
                else {
                    label = label.getComponent(cc.RichText);
                }
            }
            if (label) {
                label.string = String(str);
            }
            else {
                console.warn('#SetTxt ：target 未挂载 cc.Label | cc.RichText 组件 ');
            }
        };
        UI.SetImg = function (target, sf) {
            if (!cc.isValid(target)) {
                console.warn('#SetImg ：target 无效 ');
                return;
            }
            var sprite = target;
            if (sprite instanceof cc.Node) {
                sprite = sprite.getComponent(cc.Sprite);
            }
            if (sprite) {
                sprite.spriteFrame = sf;
            }
            else {
                console.warn('#SetImg ：target 未挂载 cc.Sprite 组件 ');
            }
        };
        UI.FixedBorders = function (node, borders, borderSize) {
            borders.x = node.x;
            borders.y = node.y;
            var top = borders.children[0];
            var bottom = borders.children[1];
            var left = borders.children[2];
            var right = borders.children[3];
            top.anchorY = 0;
            top.width = node.width + 2 * borderSize;
            top.height = borderSize;
            top.x = node.x;
            top.y = node.y + node.height / 2;
            bottom.anchorY = 1;
            bottom.width = node.width + 2 * borderSize;
            bottom.height = borderSize;
            bottom.x = node.x;
            bottom.y = node.y - node.height / 2;
            left.anchorX = 1;
            left.width = borderSize;
            left.height = node.height + borderSize * 2;
            left.x = node.x - node.width / 2;
            left.y = node.y;
            right.anchorX = 0;
            right.width = borderSize;
            right.height = node.height + borderSize * 2;
            right.x = node.x + node.width / 2;
            right.y = node.y;
        };
        /**
         * 关闭该节点所附着的页面（ViewBase）
         * @param node 节点 或者 节点所挂载的组件
         */
        UI.CloseLocatedView = function (node) {
            if (node instanceof cc.Component) {
                node = node.node;
            }
            var e = new cc.Event.EventCustom(f.EventType.ClickBtnClose, true);
            node.dispatchEvent(e);
        };
        return UI;
    }());
    f.UI = UI;
    (function (UI) {
        UI.DIALOG_INFO = '_dialog_info';
    })(UI = f.UI || (f.UI = {}));
    var UIHierarchy;
    (function (UIHierarchy) {
        UIHierarchy[UIHierarchy["main"] = 0] = "main";
        UIHierarchy[UIHierarchy["view"] = 1] = "view";
        UIHierarchy[UIHierarchy["dialog"] = 2] = "dialog";
        UIHierarchy[UIHierarchy["tips"] = 3] = "tips";
    })(UIHierarchy = f.UIHierarchy || (f.UIHierarchy = {}));
    var UIMgr = /** @class */ (function () {
        function UIMgr() {
        }
        UIMgr.Init = function () {
            if (this._inited) {
                return;
            }
            this._inited = true;
            MainPageController.Init(this.BindHierarchy('main'));
            PageController.Init(this.BindHierarchy('view'));
            DialogController.Init(this.BindHierarchy('dialog'));
            TipsController.Init(this.BindHierarchy('tips'));
        };
        UIMgr.BindHierarchy = function (str) {
            var scene = cc.director.getScene();
            var canvas = cc.find('Canvas', scene);
            var node = f.NodeMgr.AddSingletonChild(canvas, str);
            this._hierarchy[UIHierarchy[str]] = node;
            node.zIndex = UIHierarchy[str];
            return node;
        };
        UIMgr._hierarchy = [];
        UIMgr._inited = false;
        return UIMgr;
    }());
    f.UIMgr = UIMgr;
    var ViewController = /** @class */ (function () {
        function ViewController() {
        }
        ViewController.Init = function (node) {
            console.log("view controller init!");
            this.root = node;
            node.on(f.EventType.PageClose, this.OnPageClose, this);
        };
        ViewController.OnPageClose = function (event) {
            event.stopPropagationImmediate();
            this.Close(event.target);
        };
        ViewController.Open = function (node, eventTarget) {
            console.log('view controller open ', node);
            node.parent = this.root;
            node.active = true;
            //! onLoad方法在 首次激活 或者附加在 激活节点时 调用；
            //! 若在这之前 emit 则由于 未执行 onLoad里的on方法，emit石沉大海
            node.emit(f.EventType.ViewBaseAddListener);
            node.emit(f.EventType.ViewBaseSetEventTarget, eventTarget);
        };
        ViewController.Close = function (node) {
            node.emit(f.EventType.ViewBaseRemoveListener);
            f.Res.TakePage(node);
        };
        return ViewController;
    }());
    var MainPageController = /** @class */ (function (_super) {
        __extends(MainPageController, _super);
        function MainPageController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return MainPageController;
    }(ViewController));
    var PageController = /** @class */ (function (_super) {
        __extends(PageController, _super);
        function PageController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PageController;
    }(ViewController));
    var DialogController = /** @class */ (function (_super) {
        __extends(DialogController, _super);
        function DialogController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        DialogController.Init = function (node) {
            _super.Init.call(this, node);
            this._hidding = new Array();
            this._waittingDialogs = new Array();
            var mask = f.NodeMgr.AddSingletonChild(this.root, 'mask');
            var sprite = f.NodeMgr.AddSingletonComponent(mask, cc.Sprite);
            f.NodeMgr.AddSingletonComponent(mask, cc.BlockInputEvents);
            sprite.spriteFrame = f.Res.GetDefaultSprite();
            mask.color = cc.Color.GRAY;
            mask.opacity = 153;
            mask.width = cc.visibleRect.width;
            mask.height = cc.visibleRect.height;
            mask.active = false;
            mask.on(cc.Node.EventType.TOUCH_END, this.CloseCurrent, this);
            this._mask = mask;
        };
        DialogController.Open = function (node, eventTarget) {
            console.log('dialog controller open a dialog');
            _super.Open.call(this, node, eventTarget);
            if (this._opening) {
                this.Hide(this._opening);
            }
            this._opening = node;
            this._mask.active = true;
            f.Action.ScaleFadeIn({ target: node, duration: 0.3 });
        };
        DialogController.Hide = function (node) {
            node.active = false;
            this._hidding.push(node);
            node.emit(f.EventType.ViewBaseAddListener);
        };
        DialogController.Show = function (node) {
            node.emit(f.EventType.ViewBaseRemoveListener);
            node.active = true;
        };
        DialogController.InlineOpen = function (node, eventTarget) {
            if (!this._opening) {
                this.Open(node, eventTarget);
            }
            else {
                var dialog = {
                    node: node,
                    eventTarget: eventTarget
                };
                this._waittingDialogs.push(dialog);
            }
        };
        DialogController.CloseCurrent = function () {
            var node = this._opening;
            if (node) {
                this.Close(node);
            }
        };
        DialogController.CloseDialogByName = function (name) {
            for (var i = 0; i < this._hidding.length; i++) {
                var el = this._hidding[i];
                if (el.name === name) {
                    f.Arr.remove(this._hidding, el);
                    f.Res.TakeDialog(el);
                }
            }
        };
        DialogController.Close = function (node) {
            var _this = this;
            console.log('dialog controller close a dialog');
            this._opening = null;
            //! 先显示隐藏节点
            if (this._hidding.length > 0) {
                var hideNode = this._hidding.pop();
                this._opening = hideNode;
                this.Show(hideNode);
                f.Res.TakeDialog(node);
                return;
            }
            //! 再依次打开窗口
            var dialog = this._waittingDialogs.shift();
            this._mask.active = !!dialog;
            node.emit(f.EventType.ViewBaseRemoveListener);
            f.Action.ScaleFadeOut({
                target: node, duration: 0.3, cb: function () {
                    f.Res.TakeDialog(node);
                    if (dialog) {
                        _this.Open(dialog.node, dialog.eventTarget);
                    }
                }
            });
        };
        return DialogController;
    }(ViewController));
    var TipsController = /** @class */ (function (_super) {
        __extends(TipsController, _super);
        function TipsController() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        TipsController.showTip = function (str) {
            var _this = this;
            var node = f.Res.LoadTip();
            node.parent = this.root;
            node.active = true;
            var label = node.children[0];
            var comp = f.NodeMgr.AddSingletonComponent(label, cc.Label);
            comp.string = str;
            var duration = 0.8;
            var distance = node.height + 2;
            for (var i = 0; i < this._opening.length; i++) {
                var el = this._opening[i];
                f.Action.MoveBy({
                    target: el,
                    y: distance,
                    duration: duration
                });
            }
            this._opening.push(node);
            this._isInAction = true;
            node.runAction(cc.sequence(cc.moveBy(duration, 0, distance), cc.callFunc(function () {
                _this._isInAction = false;
                _this.hasNext();
            }), cc.delayTime(duration * 3), cc.callFunc(function () {
                _this.Close(node);
            })));
        };
        TipsController.showTipsInLine = function (str) {
            if (this._isInAction) {
                this._waittingTips.push(str);
            }
            else {
                this.showTip(str);
            }
        };
        TipsController.Close = function (node) {
            console.log('dialog controller close a dialog');
            f.Arr.remove(this._opening, node);
            f.Res.TakeDialog(node);
        };
        TipsController.hasNext = function () {
            var string = this._waittingTips.shift();
            if (string) {
                this.showTip(string);
            }
        };
        TipsController._opening = new Array();
        TipsController._waittingTips = new Array();
        TipsController._isInAction = false;
        return TipsController;
    }(ViewController));
})(f || (f = {}));
var f;
(function (f) {
    var Util = /** @class */ (function () {
        function Util() {
        }
        /**
         * 将number的值限制在范围 [lower, upper]内
         * @param number 需要限制的数
         * @param lower 最小值
         * @param upper 最大值
         * @returns 被限制在范围内的数
         */
        Util.Clamp = function (number, lower, upper) {
            if (number === number) {
                if (upper !== undefined) {
                    number = number <= upper ? number : upper;
                }
                if (lower !== undefined) {
                    number = number >= lower ? number : lower;
                }
            }
            return number;
        };
        Util.Random = function (n1, n2) {
            if (n1 > n2) {
                cc.warn('#Random: n2 must be greater than n1');
                return;
            }
            var size = n2 - n1;
            return Math.random() * size + n1;
        };
        Util.GetUrl = function (path, name) {
            path = path || '';
            var test = path + '/' + name;
            test.replace(/\\/g, '/');
            var a = test.replace(/[/]+/g, '/');
            return a;
        };
        Util._getClass = function (o) {
            return Object.prototype.toString.call(o).slice(8, -1).toLowerCase();
        };
        Util.CheckIntersection = function (node, node1) {
            var rect1 = node.getBoundingBoxToWorld();
            var rect2 = node1.getBoundingBoxToWorld();
            return cc.Intersection.rectRect(rect1, rect2);
        };
        // 求点Point 在线段AB 上 另一端的 对称点；
        Util.GetSymmetricPoint = function (A, B, P) {
            // ab基向量
            var lineNormal = A.sub(B).normalize();
            // 连接AP
            var AP = P.sub(A);
            // PP` 和 AB 交点 O， 求得 AO 距离
            var distance = lineNormal.dot(AP);
            // O点坐标
            var O = A.add(lineNormal.mulSelf(distance));
            var PO = O.sub(P);
            return O.add(PO);
        };
        Util.MD5 = function (string) {
            var x = Array();
            var k, AA, BB, CC, DD, a, b, c, d;
            var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
            var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
            var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
            var S41 = 6, S42 = 10, S43 = 15, S44 = 21;
            string = uTF8Encode(string);
            x = convertToWordArray(string);
            a = 0x67452301;
            b = 0xEFCDAB89;
            c = 0x98BADCFE;
            d = 0x10325476;
            for (k = 0; k < x.length; k += 16) {
                AA = a;
                BB = b;
                CC = c;
                DD = d;
                a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
                d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
                c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
                b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
                a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
                d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
                c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
                b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
                a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
                d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
                c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
                b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
                a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
                d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
                c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
                b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
                a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
                d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
                c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
                b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
                a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
                d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
                c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
                b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
                a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
                d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
                c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
                b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
                a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
                d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
                c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
                b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
                a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
                d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
                c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
                b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
                a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
                d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
                c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
                b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
                a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
                d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
                c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
                b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
                a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
                d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
                c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
                b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
                a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
                d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
                c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
                b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
                a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
                d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
                c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
                b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
                a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
                d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
                c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
                b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
                a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
                d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
                c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
                b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
                a = addUnsigned(a, AA);
                b = addUnsigned(b, BB);
                c = addUnsigned(c, CC);
                d = addUnsigned(d, DD);
            }
            var tempValue = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);
            return tempValue.toLowerCase();
            function rotateLeft(lValue, iShiftBits) {
                return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
            }
            function addUnsigned(lX, lY) {
                var lX4, lY4, lX8, lY8, lResult;
                lX8 = (lX & 0x80000000);
                lY8 = (lY & 0x80000000);
                lX4 = (lX & 0x40000000);
                lY4 = (lY & 0x40000000);
                lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
                if (lX4 & lY4)
                    return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
                if (lX4 | lY4) {
                    if (lResult & 0x40000000)
                        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                    else
                        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
                else {
                    return (lResult ^ lX8 ^ lY8);
                }
            }
            function F(x, y, z) {
                return (x & y) | ((~x) & z);
            }
            function G(x, y, z) {
                return (x & z) | (y & (~z));
            }
            function H(x, y, z) {
                return (x ^ y ^ z);
            }
            function I(x, y, z) {
                return (y ^ (x | (~z)));
            }
            function FF(a, b, c, d, x, s, ac) {
                a = addUnsigned(a, addUnsigned(addUnsigned(F(b, c, d), x), ac));
                return addUnsigned(rotateLeft(a, s), b);
            }
            ;
            function GG(a, b, c, d, x, s, ac) {
                a = addUnsigned(a, addUnsigned(addUnsigned(G(b, c, d), x), ac));
                return addUnsigned(rotateLeft(a, s), b);
            }
            ;
            function HH(a, b, c, d, x, s, ac) {
                a = addUnsigned(a, addUnsigned(addUnsigned(H(b, c, d), x), ac));
                return addUnsigned(rotateLeft(a, s), b);
            }
            ;
            function II(a, b, c, d, x, s, ac) {
                a = addUnsigned(a, addUnsigned(addUnsigned(I(b, c, d), x), ac));
                return addUnsigned(rotateLeft(a, s), b);
            }
            ;
            function convertToWordArray(string) {
                var lWordCount;
                var lMessageLength = string.length;
                var lNumberOfWordsTempOne = lMessageLength + 8;
                var lNumberOfWordsTempTwo = (lNumberOfWordsTempOne - (lNumberOfWordsTempOne % 64)) / 64;
                var lNumberOfWords = (lNumberOfWordsTempTwo + 1) * 16;
                var lWordArray = Array(lNumberOfWords - 1);
                var lBytePosition = 0;
                var lByteCount = 0;
                while (lByteCount < lMessageLength) {
                    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                    lBytePosition = (lByteCount % 4) * 8;
                    lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
                    lByteCount++;
                }
                lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                lBytePosition = (lByteCount % 4) * 8;
                lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
                lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
                lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
                return lWordArray;
            }
            ;
            function wordToHex(lValue) {
                var WordToHexValue = "", WordToHexValueTemp = "", lByte, lCount;
                for (lCount = 0; lCount <= 3; lCount++) {
                    lByte = (lValue >>> (lCount * 8)) & 255;
                    WordToHexValueTemp = "0" + lByte.toString(16);
                    WordToHexValue = WordToHexValue + WordToHexValueTemp.substr(WordToHexValueTemp.length - 2, 2);
                }
                return WordToHexValue;
            }
            ;
            function uTF8Encode(string) {
                string = string.replace(/\x0d\x0a/g, "\x0a");
                var output = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        output += String.fromCharCode(c);
                    }
                    else if ((c > 127) && (c < 2048)) {
                        output += String.fromCharCode((c >> 6) | 192);
                        output += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        output += String.fromCharCode((c >> 12) | 224);
                        output += String.fromCharCode(((c >> 6) & 63) | 128);
                        output += String.fromCharCode((c & 63) | 128);
                    }
                }
                return output;
            }
            ;
        };
        return Util;
    }());
    f.Util = Util;
})(f || (f = {}));
var f;
(function (f) {
    var ViewMgr = /** @class */ (function () {
        function ViewMgr() {
        }
        ViewMgr.Init = function (node) {
            this.root = node;
        };
        ViewMgr.Open = function (node) {
            if (cc.isValid(this._curView)) {
                this._curView.active = false;
                this._lastViews.push(this._curView);
            }
            node.active = true;
            ;
            node.parent = this.root;
            this._curView = node;
        };
        ViewMgr.Close = function (node) {
            f.Arr.remove(this._lastViews, node);
            if (this._curView === node) {
                node.destroy();
                this._curView = this._lastViews.pop();
                this._curView.active = true;
            }
        };
        ViewMgr.OnViewClose = function (e) {
            e.stopPropagationImmediate();
            var node = e.target;
            this.Close(node);
            // todo 让res controller 缓存节点
        };
        ViewMgr.root = null; //
        ViewMgr._curView = null;
        ViewMgr._lastViews = [];
        return ViewMgr;
    }());
    f.ViewMgr = ViewMgr;
})(f || (f = {}));
