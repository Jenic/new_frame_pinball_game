enum AudioName {
    add = 'add',
    adjust_line = 'adjust_line',
    bg = 'bg',
    bomb = 'bomb',
    click_invalid = 'click_invalid',
    click_open = 'click_open',
    click_redbag = 'click_redbag',
    emission = 'emission',
    fail = 'fail',
    hit = 'hit',
    hit_wall = 'hit_wall',
    landing = 'landing',
    muffler_effect = 'muffler_effect',
    success = 'success',
    tx = 'tx',
    warning = 'warning',
}
export default class AudioMgr {

    static _isMusicMute: boolean = false;
    public static get isMusicMute(): boolean { return AudioMgr._isMusicMute; }
    public static set isMusicMute(value: boolean) {
        AudioMgr._isMusicMute = value;
        if (value) {
            this.PlayMusic(this.audios.get(this._currentBGM));
        } else {
            this.StopMusic();
        }
    }
    public static musicVolume: number = 1;
    static _isEffectMute: boolean = false;
    public static get isEffectMute(): boolean { return AudioMgr._isEffectMute; }
    public static set isEffectMute(value: boolean) { AudioMgr._isEffectMute = value; }
    public static effectVolume: number = 1;

    static _currentBGM: string;

    static audios: Map<string, cc.AudioClip> = new Map<string, cc.AudioClip>();

    public static LoadLocalAudios(cb: () => void): void {
        cc.loader.loadResDir('audios', cc.AudioClip, (err, res) => {
            for (let i = 0; i < res.length; i++) {
                const el = res[i];
                this.audios.set(el.name, el);
            }
            cb();
        })
    }

    public static LoadRemoteAudios(): void {

    }

    public static PlayBGM(): void {
        this._currentBGM = AudioName.tx;
        this.PlayMusic(this.audios.get(AudioName.tx));
    }

    public static PlayGameBGM(): void {
        this._currentBGM = AudioName.bg;
        this.PlayMusic(this.audios.get(AudioName.bg));
    }

    public static PlayAddBallEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.add));
    }
    // 瞄准音效
    public static PlayMoveLineEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.adjust_line));

    }

    public static PlayBombEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.bomb));

    }

    public static PlayLaserEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.emission));

    }

    public static PlayClickEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.click_open));

    }

    public static PlayClickInvalidEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.click_invalid));

    }

    public static PlayClickRedBagEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.click_redbag));

    }

    public static PlayGameSuccessEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.success));

    }

    public static PlayGameFailureEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.fail));

    }

    public static PlayHitBoxEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.hit));

    }

    public static PlayHitWallEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.hit_wall));

    }

    public static PlayLandingEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.landing));

    }
    public static PlayWarningEffect(): void {
        this.PlayEffect(this.audios.get(AudioName.warning));
    }

    public static Play(clip: cc.AudioClip, volume: number = 1) {
        cc.audioEngine.play(clip, true, volume * this.musicVolume);

    }

    public static PlayMusic(clip: cc.AudioClip) {
        if (!this.isMusicMute) {
            this.StopMusic();
            cc.audioEngine.playMusic(clip, true);
        }
    }

    public static PlayEffect(clip: cc.AudioClip, volume: number = 1) {
        if (!this.isEffectMute) {
            cc.audioEngine.playEffect(clip, false);
        }
    }

    public static StopMusic() {
        cc.audioEngine.stopMusic();
    }
    // update (dt) {}
}
