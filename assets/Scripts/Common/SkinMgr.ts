import Net from "../Network/Net";

export default class SkinMgr {

    public static _skinData: Array<any>;
    static _skinRes: Map<string, cc.SpriteFrame> = new Map<string, cc.SpriteFrame>();

    public static UpdateSkinInfo(cb: (result: boolean) => void) {
        Net.BallList((err, resp) => {
            if (err) {
                cb(false);
                return;
            }
            console.log(resp);
            this._skinData = resp.data;
            cb(true);
        })
    }

    static LoadSkinRes(cb: () => void) {
        cc.loader.loadResDir('ball_skin', cc.SpriteFrame, (err, res) => {
            for (let i = 0; i < res.length; i++) {
                const el = res[i];
                this._skinRes.set(el.name, el);
            }
            cb();
        })
    }

    static getDefaultSkin() {
        for (let i = 0; i < this._skinData.length; i++) {
            const el = this._skinData[i];
            if (el.isCan) {
                return el.code;
            }
        }
        return this._skinData[0].code;
    }
    static getSkin(id: string) {
        return this._skinRes.get(id);
    }
    static getCurrentSkin() {
        let cur = f.Res.GetStorage('ball_skin');
        if (!cur) {
            cur = this.getDefaultSkin();
        }
        return this.getSkin(cur);
    }

    public static UnlockSkin(id: string, cb: (data: any) => void) {
        Net.UnlockBallSkin(id, (resp) => {
            console.log('unlock skin resp');
            cb(resp);
        });
    }

    public static UpdateLocalData(id: string) {
        for (let i = 0; i < this._skinData.length; i++) {
            const el = this._skinData[i];
            if (el.code === id) {
                el.isCan = true;
            }
        }
    }

    public static UserSkin(id: string) {
        f.Res.SetStorage('ball_skin', id);
    }
}