import Http from "../Network/Http";
import Net from "../Network/Net";

export default class AssetManager {

    public static boxSFs: Array<cc.SpriteFrame> = new Array<cc.SpriteFrame>();
    public static boxHitedSFs: Array<cc.SpriteFrame> = new Array<cc.SpriteFrame>();
    public static _isInited: boolean = false;

    public static SFs: Array<cc.SpriteFrame> = new Array<cc.SpriteFrame>();

    public static Init(cb: () => void) {
        if (this._isInited) {
            return;
        }
        this._isInited = true;

        Net.AppVersion({}, (err, resp) => {

            if (err) {
                return;
            }
            let url = resp.data.resourceUrl;
            let resourceNo = resp.data.resourceNo;
            if (resourceNo) {
                let wait = new f.Wait(cb);

                Http.Get(url, (res) => {
                    let json = JSON.parse(res);
                    let element = json.element;
                    for (let i = 0; i < element.length; i++) {
                        const el = element[i];
                        let oss = el.oss;
                        let hitPath = el.hitPath;
                        let initPath = el.initPath;
                        let defaultUrl = oss + initPath;
                        switch (el.code) {
                            case 2:

                                wait.WaitFor(hitPath);
                                wait.WaitFor(initPath);
                                cc.assetManager.loadRemote(defaultUrl, (err: Error, asset: cc.Texture2D) => {
                                    this.boxSFs[i] = new cc.SpriteFrame(asset);
                                    wait.Ready(initPath);
                                })

                                cc.assetManager.loadRemote(oss + hitPath, (err: Error, asset: cc.Texture2D) => {
                                    this.boxHitedSFs[i] = new cc.SpriteFrame(asset);
                                    wait.Ready(hitPath);
                                })
                                break;
                            default:
                                wait.WaitFor(initPath);
                                cc.assetManager.loadRemote(defaultUrl, (err: Error, asset: cc.Texture2D) => {
                                    this.SFs[el.code] = new cc.SpriteFrame(asset);
                                    wait.Ready(initPath);
                                })
                                break;
                        }
                    }
                })
                // 使用远程贴图


            } else {
                // 不走远程 走本地resources
                let wait = new f.Wait(cb);
                wait.WaitFor([
                    'normal',
                    'hited',
                ])
                cc.loader.loadResDir('boxs/normal', cc.SpriteFrame, (err, res) => {
                    this.boxSFs = res;
                    wait.Ready('normal');
                })
                cc.loader.loadResDir('boxs/hited', cc.SpriteFrame, (err, res) => {
                    this.boxHitedSFs = res;
                    wait.Ready('hited');
                })
            }
            console.log(resp);
        })

    }
}