import Net, { AccountInfoResponse, LoginResponse } from "../Network/Net";
import { SystemEvent } from "../Views/GameEvent";

export default class User {
    static isLogin: boolean = false;

    static _AccessToken: string = "";
    public static get AccessToken(): string {
        return this._AccessToken;
    };
    static set AccessToken(value: string) {
        this._AccessToken = value;
    }

    static userId: number;
    static userName: string;
    static expires: string;
    static avatar: string;

    static balance: number;
    static settled: number;

    static Login(data: LoginResponse) {
        this.isLogin = true;
    }

    public static Logout() {
        this.isLogin = false;

        this.userId = 0;
        this.AccessToken = "";
        this.userName = '';
        this.avatar = "";

        f.GEvent.Emit(SystemEvent.Logout);
    }

    public static UpDateUserInfo(cb: (resulet: boolean) => void) {
        Net.AccountInfo((err: any, info: AccountInfoResponse) => {
            if (err) {
                cb(false);
                return;
            }
            console.log(info);
            this.avatar = info.data.avatar;
            this.userId = info.data.userId;
            this.userName = info.data.name;
            this.balance = info.data.balance;
            this.settled = info.data.settled;
            cb(true);
        })
    }

    public static RefreshBalance(cb: (num: number) => void) {
        Net.AccountInfo((err: any, info: AccountInfoResponse) => {
            if (err) {
                return;
            }
            cb(info.data.balance);
        })
    }
}