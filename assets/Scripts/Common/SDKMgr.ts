const WXCode = '081BpvFa1jIaBB04UGHa19ihqs0BpvFW';
export default class SDKMgr {
    static _WxAuthCodeCb = (code: string): void => { };

    static AppId: string = 'wx97f1067816ff142b';
    static AppSecret: string = 'b4446ce14b0b66d8b859431c3f52b4a6';

    static _inst: SDKMgr;
    static WXJSSDK: any;
    static get inst(): SDKMgr {
        if (!this._inst) {
            this._inst = new SDKMgr();
        }
        return this._inst;
    }

    static Init() {
        SDKMgr.WXJSSDK = window['WXJSSDK'];
        SDKMgr.WXJSSDK.setListener(SDKMgr.inst);
        SDKMgr.WXJSSDK.InitSDK(SDKMgr.AppId, SDKMgr.AppSecret);
    }

    bridgeTest(str) {
        console.log(`SDKMgr bridgeTest`, str);
    }

    // ========== request ==========

    static GetWxAuthCode(cb: (code: string) => void) {
        this._WxAuthCodeCb = cb;
        if (cc.sys.isNative){
            SDKMgr.WXJSSDK.GetWxAuthCode();
        } else {
            cb(WXCode);
        }
    }


    public static GetWxAccessToken(code:string) {
        
    }

    // ========== respone ==========
    OnWxAuthCode(code: string) {
        SDKMgr._WxAuthCodeCb(code);
        SDKMgr._WxAuthCodeCb = () => { };
    }

    static Login(cb: () => void) {
        SDKMgr.WXJSSDK.Login();
    }

    OnLoginResp(str) {
        f.UI.Tip(str);
    }
}