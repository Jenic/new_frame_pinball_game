import { IRewardVideo } from "../Framework/PangleAdSdk/Interfaces/IRewardVideo";
import Net from "../Network/Net";
export enum AdSlot {
    open_open = 'index_open',
    addBall_reward = 'gq_jsq',
    level_insert = 'gq_kqcp',
    openRedBag_reward = 'tc_lhbxj',
    level_success_express = 'tc_cgcg',
    level_failure_express = 'tc_cgsb',
    getRedBag_express = 'tc_hdhbxj',
    withdraw_banner = 'tc_tx_banner',
}
// const AppId = '200660481181093888';
export default class AdMgr {
    static _adIdMap: Map<string, Array<any>> = new Map<string, Array<any>>();

    static RewardVideo;
    static Splash;
    static Express;

    static cb;
    static Init() {
        this.RewardVideo = window['RewardVideo'];
        this.Splash = window['Splash'];
        this.Express = window['Express'];
        this.RewardVideo.SetListener(AdMgr);
        this.Express.SetListener(AdMgr);
    }

    static preload() {
        this.LoadRewardVideo(AdSlot.openRedBag_reward);
        this.LoadRewardVideo(AdSlot.addBall_reward);
        this.LoadExpress(AdSlot.level_success_express);
        this.LoadExpress(AdSlot.getRedBag_express);
    }

    static _isSplashLoaded: boolean = false;
    static LoadSplashAd(sceneId: string) {
        console.log("ShowSplashAd", sceneId);
        let code = this.GetSlotId(sceneId);
        console.log("ShowSplashAd", code);
        if (!code) return false;
        if (cc.sys.isNative) {
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/PangleSplash', "LoadSplashAd", "(Ljava/lang/String;)V", code);
            this._isSplashLoaded = true;
            return true;
        }
    }

    static ShowSplashAd() {
        if (!this._isSplashLoaded) return;
        if (cc.sys.isNative) {
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/PangleSplash', "ShowSplashAd", "()V");
        }
    }

    static LoadExpress(sceneId: string) {
        let code = this.GetSlotId(sceneId);
        if (!code) return;
        this.Express.LoadExpress(code, sceneId);
    }

    static ShowExpressAd(sceneId: string) {
        let code = this.GetSlotId(sceneId);
        if (!code) return;
        this.Express.ShowExpressAd(sceneId);
    }

    static CloseExpressAd(sceneId: string) {
        let code = this.GetSlotId(sceneId);
        if (!code) return;
        this.Express.CloseExpressAd(sceneId);
    }

    static OnExpressLoadFailed(sceneId) {
        this.LoadExpress(sceneId);
    }

    static OnExpressClose(sceneId) {
        this.LoadExpress(sceneId);
    }

    static PreloadAdMap(sceneId: string, cb: () => void) {
        Net.AdInfo({ code: sceneId }, (err, resp) => {
            if (err) {
                return;
            }
            this._adIdMap.set(sceneId, resp.data);
            cb();
        })
    }

    static showRewardAd(cb: () => void, sceneId: string) {
        if (this.RewardVideo.IsRewardVideoLoaded(sceneId)) {
            this.RewardVideo.ShowRwardVideo(sceneId);
            this.cb = cb;
        } else {
            let code = this.GetSlotId(sceneId);
            if (!code) {
                cb()
            } else {
                f.UI.Tip('广告正在加载,请稍后再试');
                this.LoadRewardVideo(sceneId);
            };
        }

    }


    static LoadRewardVideo(sceneId: string, slotAd?: string) {
        if (slotAd) {
            this.RewardVideo.LoadRewardVideo(slotAd, sceneId);
        } else {
            let code = this.GetSlotId(sceneId)
            if (!code) return;
            this.RewardVideo.LoadRewardVideo(code, sceneId);
        }
    }

    public static GetSlotId(sceneId: string) {
        let adIds = this._adIdMap.get(sceneId);
        return adIds[0]?.codeNo;
    }

    static OnLoadRewardVideoFailed(sncd) {
        let arr = sncd.split(',');
        let [sceneId, code] = arr;
        let loadNext = false;
        let adIds = this._adIdMap.get(sceneId);
        for (let i = 0; i < adIds.length; i++) {
            const el = adIds[i];
            if (loadNext) {
                // 失败的加载,加载下一个
                this.LoadRewardVideo(sceneId, code);
                return;
            }
            if (el.codeNo == code) {
                loadNext = true;
            }
        }
    }

    static OnRewardVideoReward() {
        console.log('admgr OnRewardVideoReward');
        this.cb();
        this.cb = () => { };
    }

    static OnRewardVideoClose(sncd) {
        // 成功的加载  从头加载
        let [sceneId, code] = sncd.split(',');
        this.LoadRewardVideo(sceneId);

    }
}