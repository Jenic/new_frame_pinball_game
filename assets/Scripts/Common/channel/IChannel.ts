export default interface IChannel {
    Login(cb: () => void): void;
    normalLogin(acc: number, pswd: string, cb: (err: any, res: any) => void): void;
}