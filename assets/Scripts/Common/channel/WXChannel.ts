import Net from "../../Network/Net";
import SDKMgr from "../SDKMgr";
import IChannel from "./IChannel";

export default class WXChannel implements IChannel {
    normalLogin(acc: number, pswd: string, cb: (err: Error, res: any) => void): void {
    }
    Login(cb: () => void): void {

        SDKMgr.GetWxAuthCode((code: string) => {
            Net.WXLogin({ code }, (err, resp) => {
                if (resp.respCode === "00") {
                    f.Res.SetStorage('avatar', resp.data.avatar);
                    f.Res.SetStorage('expires', resp.data.expires);
                    f.Res.SetStorage('userId', String(resp.data.userId));
                    f.Res.SetStorage('accessToken', resp.data.accessToken);
                    cb();
                }

            })
        })
    }


}
