import Net, { LoginResponse, AccountInfoResponse } from "../../Network/Net";
import SDKMgr from "../SDKMgr";
import User from "../User";
import IChannel from "./IChannel";

export default class TestChannel implements IChannel {
    _loginCb;
    Login(cb: () => void): void {
        this._loginCb = cb;
        this.tryToLogin(() => {
            this.wxLogin();
        });
    }

    wxLogin() {
        let self = this;
        SDKMgr.GetWxAuthCode((code: string) => {
            Net.WXLogin({ code }, (err, resp) => {
                if (err) {
                    cc.warn(err);
                    return;
                }
                if (resp.respCode === "00") {
                    console.log(`set token ============|${resp.data.accessToken}|==`);
                    f.Res.SetStorage('accessToken', resp.data.accessToken);
                    this._loginCb();
                }
            })
        })
    }

    tryToLogin(cb) {
        let self = this;
        let accessToken = f.Res.GetStorage('accessToken');
        Net.Refresh({ accessToken }, (err, resp: LoginResponse) => {
            if (err) {
                cb();
                return;
            }
            if (resp.respCode === "00") {
                f.Res.SetStorage('accessToken', resp.data.accessToken);
                console.log(`new token ============|${resp.data.accessToken}|==`);
                this._loginCb();
            }
        })
    }


    // autoNormalLogin(cb: () => void): void {
    //     this._loginCb = cb;
    //     this.tryToLogin(() => {
    //         this.normalLogin();
    //     });
    // }

    normalLogin(acc, pswd, cb) {
        Net.Login({ mobile: acc, code: pswd }, (err, resp) => {
            if (err) {
                cb(err);
                return;
            }
            if (resp.respCode === "00") {
                console.log(`set token ============|${resp.data.accessToken}|==`);
                f.Res.SetStorage('accessToken', resp.data.accessToken);
                cb(null, null);
            }

        })
    }
}