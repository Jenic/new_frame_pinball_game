import Net from "../Network/Net";

//! 大关卡完成情况
const LevelKey = 'level_info';
//! 小关卡完成情况
const LevelInnerKey = 'level_inner';
const TotalGridRows = 13;
export default class LevelMgr {

    public static _levelList;
    public static _levelInfo;
    public static startBallCnt: number; // 当前关卡 初始球数量;
    public static showAdDelay: number; // 看广告加球按钮,显示时机;
    public static showAdDuration: number; // 广告按钮显示时长;
    public static originRows: number; // 初始显示的球行数
    public static AdBallCnt: number; // 一次广告加球数量

    public static _index: number; //
    public static nextLevel: number;
    public static UpdateRoundInfo(cb: (result: boolean) => void) {
        Net.RoundList((err, resp) => {
            if (err) {
                cb(false);
                return;
            }
            console.log(resp);
            this._levelList = resp.data;
            cb(true);
        })
    }

    public static getLastLevelIndex() {
        // let lastIndex = this._levelList.length - 1;
        // for (let i = 0; i < this._levelList.length; i++) {
        //     const el = this._levelList[i];
        //     if (!el.pass) {
        //         lastIndex = i;
        //         return lastIndex;
        //     }
        // }
        // return lastIndex;

        let index = 0;
        for (let i = 0; i < this._levelList.length; i++) {
            const el = this._levelList[i];
            if (el.pass) {
                index = i;
            } else {
                return index;
            }
        }
        return index;
    }

    public static get index() {
        return this._index || 0;
    }

    public static set index(index: number) {
        if (index >= this._levelList.length) {
            index = this._levelList.length - 1;
        }
        this._index = index;
    }

    public static GetCurLevelId() {
        let index = this._index;
        if (index == null || index == undefined) {
            index = this.getLastLevelIndex();
        }
        console.log('cur level', index);
        return this._levelList[index].id;
    }

    public static GetCurLevelIndex() {
        let index = this._index;
        if (index == null || index == undefined) {
            this._index = index = this.getLastLevelIndex();
        }
        return index;
    }

    public static GetLevelId(index) {
        return this._levelList[index].id;
    }


    // 解锁关卡 id
    public static UnlockLevel(id: string) {

        console.log('unlock level');
        for (let i = 0; i < this._levelList.length; i++) {
            const el = this._levelList[i];
            if (el.id == id) {
                el.pass = true;
            }
        }

        console.log(this._levelList)
    }

    public static GetLevelInfo(id: string, cb: () => void) {
        Net.RoundInfo(id, (err, resp) => {
            console.log('level info', resp);
            this.startBallCnt = resp.data.balls;
            this.AdBallCnt = resp.data.addBalls;
            this.showAdDelay = resp.data.adStart;
            this.showAdDuration = resp.data.adEnd - resp.data.adStart;
            this.originRows = TotalGridRows - resp.data.low;
            this._levelInfo = resp.data.items;
            cb();
        })
    }
}