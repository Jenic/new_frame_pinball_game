
export default class Util {

    static Destroy(node: cc.Node) {
        node["__destroy"] = true;
        node.destroy();
    }

    static isDestroy(node: cc.Node) {
        return !cc.isValid(node) || node["__destroy"];
    }
    // update (dt) {}
}
