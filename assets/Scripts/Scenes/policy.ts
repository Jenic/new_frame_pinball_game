import { GDefs } from "../Defineds/GDefs";
import ViewBase from "../Framework/View/ViewBase";

const { ccclass, property } = cc._decorator;

@ccclass
export default class policy extends ViewBase {

    @property(cc.Node)
    btn_disagree: cc.Node = null;

    @property(cc.Node)
    btn_agree: cc.Node = null;

    @property(cc.Node)
    btn_private_policy: cc.Node = null;

    @property(cc.Node)
    btn_service_protocol: cc.Node = null;


    AddUIEventListener() {
        f.UIMgr.Init();
        f.UI.OnClick(this.btn_agree, () => {
            f.Res.SetStorage('agree_policy', 'true');
            cc.director.loadScene(GDefs.GameScenes.preload);
        });

        f.UI.OnClick(this.btn_disagree, () => {
            cc.game.end();
        });

        f.UI.OnClick(this.btn_private_policy, () => {
            f.UI.OpenDialog(GDefs.GamePage.DialogPrivacyPolicy);
        })

        f.UI.OnClick(this.btn_service_protocol, () => {
            f.UI.OpenDialog(GDefs.GamePage.DialogServiceProtocol);
        })
    }
}
