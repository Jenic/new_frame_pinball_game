// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import AdMgr from "../Common/AdMgr";
import { GDefs } from "../Defineds/GDefs";

const { ccclass, property } = cc._decorator;

@ccclass
export default class game extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        f.UIMgr.Init();
        AdMgr.preload();
        AdMgr.ShowSplashAd();
        f.UI.OpenMainPage(GDefs.GamePage.GameTopPage);
    }

}
