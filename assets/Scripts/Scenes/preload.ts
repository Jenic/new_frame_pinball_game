import AdMgr, { AdSlot } from "../Common/AdMgr";
import AssetManager from "../Common/AssetsMgr";
import AudioMgr from "../Common/AudioMgr";
import SkinMgr from "../Common/SkinMgr";
import { GDefs } from "../Defineds/GDefs";
import Net from "../Network/Net";
/**
 * preload.ts
 * 预加载场景
 * 
 * CCC  2021-6-3 20:59:49   创建
 */
const { ccclass, property } = cc._decorator;
@ccclass
export default class preload extends cc.Component {

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Prefab)
    confirmDialog: cc.Prefab = null;
    @property(cc.Prefab)
    comonTip: cc.Prefab = null;

    @property(cc.ProgressBar)
    progress: cc.ProgressBar = null;

    @property(cc.Label)
    lblProgress: cc.Label = null;


    @property(cc.Node)
    btnClose: cc.Node = null;
    @property(cc.Node)
    btnDownload: cc.Node = null;
    @property(cc.Node)
    panelAgree: cc.Node = null;
    @property(cc.Node)
    panelProgress: cc.Node = null;
    @property(cc.ProgressBar)
    downloadProgress: cc.ProgressBar = null;
    @property(cc.Label)
    lblDownloadProgress: cc.Label = null;

    onLoad() {
        if (cc.sys.isNative) {
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', "SDKInit", "()V");
        }
    }

    start() {
        this.checkVersion();
        this.panelAgree.active = false;
        this.btnDownload.on(cc.Node.EventType.TOUCH_END, this.downLoadApk, this);
        this.btnClose.on(cc.Node.EventType.TOUCH_END, this.disagree, this);
    }

    url: string;
    enforce: boolean;
    checkVersion() {
        let version = '1.0.0';
        Net.AppVersion({}, (err, resp) => {
            if (err || version == resp.data.versionNo) {
                this.loading();
            } else {
                if (resp.data.online == true) {

                    this.url = resp.data.url;
                    this.enforce = resp.data.enforce;
                    this.panelAgree.active = true;
                    this.btnClose.active = !this.enforce;
                    this.isDownloading = false;
                } else {
                    cc.sys.openURL(resp.data.url);
                }
            }
        });
    }

    _isDownloading: boolean = false;
    get isDownloading() {
        return this._isDownloading;
    }
    set isDownloading(value) {
        this._isDownloading = value;
        this.btnDownload.active = !value;
        this.panelProgress.active = value;
    }
    downLoadApk() {
        let self = this;
        window['downloadListener'] = {
            onProgress: (str) => {
                let int = Number(str);
                self.downloadProgress.progress = int / 100;
                f.UI.SetTxt(self.lblDownloadProgress, `${int}%`);
            },
            onFailed: () => {
                console.log('下载失败')
            },
            onSuccess: () => {
                console.log('下载成功')
            }
        }
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            jsb.reflection.callStaticMethod('org/cocos2dx/javascript/VersionCheck', 'downloadAPK', '(Ljava/lang/String;)V', this.url);
        }
        this.isDownloading = true;
    }

    disagree() {
        if (!this.enforce) {
            this.panelAgree.active = false;
            this.loading();
        }
    }

    loading() {
        let wait = new f.Wait(this.next.bind(this));
        wait.WaitFor([
            'load',
            'action',
            'audio',
            'ballSkin',
            'adId',
            'assets'
        ]);

        let actionDelay = 1;
        let loadDelay = 2;
        if (GDefs.DEBUG) {
            actionDelay = 0.3;
            loadDelay = .3;
        }

        // LOGO 动作
        f.Action.ScaleFadeIn({
            target: this.logo,
            duration: actionDelay,
            cb: () => {
                wait.Ready('action');
            }
        });

        f.Res.Preload({
            comonConfirm: this.confirmDialog,
            tip: this.comonTip,
        });

        // 音频加载
        AudioMgr.LoadLocalAudios(() => {
            wait.Ready('audio');
        });

        // 广告管理器 初始化
        let needWaitAd = false;
        let keys = Object.keys(AdSlot);
        let w2 = new f.Wait(() => {
            AdMgr.Init();
            needWaitAd = AdMgr.LoadSplashAd(AdSlot.open_open);
            if (needWaitAd) {
                wait.WaitFor('splash');
            }
            wait.Ready('adId');
        });

        window['SplashWaitter'] = {
            OnSplashLoaded: () => {
                if (needWaitAd) {
                    wait.Ready('splash');
                }
            }
        }

        for (let i = 0; i < keys.length; i++) {
            const el = keys[i];
            let api = AdSlot[el];
            w2.WaitFor(api);
            AdMgr.PreloadAdMap(api, () => {
                w2.Ready(api);
            })
        }

        // 小球皮肤
        SkinMgr.LoadSkinRes(() => {
            wait.Ready('ballSkin')
        })

        // 假进度条
        cc.tween({ a: 0 })
            .to(loadDelay, { a: 1 }, {
                progress: (a, b, c, ratio) => {
                    this.progress.progress = ratio;
                    f.UI.SetTxt(this.lblProgress, `${(ratio * 100).toFixed(0)}%`)
                }
            })
            .delay(.4)
            .call(() => {
                wait.Ready('load');
            })
            .start();

        AssetManager.Init(() => {
            wait.Ready('assets');
        })

    }

    next(): void {
        cc.director.loadScene(GDefs.GameScenes.game);
    }
}

