import { GDefs } from "../Defineds/GDefs";

/**
 * logo.ts
 * logo场景脚本
 * CCC  2021-6-3 20:33:17   创建
 */
const { ccclass, property } = cc._decorator;

@ccclass
export default class logo extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        let time = 2;
        if (GDefs.DEBUG) {
            time = 0.3;
        }
        this.scheduleOnce(() => {
            let agree = f.Res.GetStorage('agree_policy');
            if (agree == 'true') {
                cc.director.loadScene(GDefs.GameScenes.preload);
            } else {
                cc.director.loadScene(GDefs.GameScenes.policy);
            }
        }, time);

        // TODO 存 ，  obj->binary->string->compress->localstorage
        // TODO 取 ，  localstorage->decompress->string->binary->obj
    }
    // update (dt) {}
}
