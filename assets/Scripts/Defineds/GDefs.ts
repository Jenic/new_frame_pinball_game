
export namespace GDefs {
    export enum GameScenes {
        logo = 'logo',
        hotupdate = 'hotupdate',
        preload = 'preload',
        game = 'game',
        policy = 'policy',
    }

    export enum GamePage {
        Game = "Game",
        GameTopPage = "GameTopPage",
        GameMainView = "GameMainView",
        DialogGameOver = "GameOver",
        DialogServiceProtocol = "ServiceProtocol",
        DialogPrivacyPolicy = "PrivacyPolicy",
        DialogGetRedBag = "GetRedBag",

        DialogPause = "Pause",
        DialogSelectLevel = "SelectLevel",
        DialogSelectSkin = "SelectSkin",
        DialogWithDraw = "WithDraw",
        DialogMyInfo = "MyInfo"
    }
    export const DEBUG: boolean = true;

    export namespace DialogParams {
        export interface GameOver {
            result: boolean;
            starCnt: number;
        }
    }
}