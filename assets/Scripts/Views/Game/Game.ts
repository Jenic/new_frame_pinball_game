// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import AdMgr, { AdSlot } from "../../Common/AdMgr";
import AssetManager from "../../Common/AssetsMgr";
import AudioMgr from "../../Common/AudioMgr";
import LevelMgr from "../../Common/LevelMgr";
import SkinMgr from "../../Common/SkinMgr";
import User from "../../Common/User";
import { GDefs } from "../../Defineds/GDefs";
import ViewBase from "../../Framework/View/ViewBase";
import Net from "../../Network/Net";
import { GameEventType, GridType } from "../GameEvent";
import EffectManager from "./EffectManager";
import GameBox from "./GameBox";
import GamePanel from "./GamePanel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Game extends ViewBase {

    @property(cc.Sprite)
    avatar: cc.Sprite = null;

    @property(cc.Label)
    lbl_cash: cc.Label = null;

    @property(GamePanel)
    content: GamePanel = null;

    @property(cc.Node)
    borders: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    @property(cc.Node)
    btnPause: cc.Node = null;

    _borderSize: number = 10;
    _gameStart: boolean = false;
    get gameStart(): boolean { return this._gameStart; }
    set gameStart(value: boolean) {
        this.btnPause.active = value;
        this._gameStart = value;
    }
    _gamePause: boolean = false;
    get gamePause(): boolean { return this._gamePause; }
    set gamePause(value: boolean) {
        this._gamePause = value;
    }

    _success: boolean = false;

    @property(cc.Graphics)
    testGraphics: cc.Graphics = null;

    @property(cc.Node)
    hit: cc.Node = null;
    @property(cc.Node)
    expl: cc.Node = null;

    @property(cc.Node)
    btnRedBag: cc.Node = null;

    @property(cc.Node)
    btnSkin: cc.Node = null;

    @property(cc.Node)
    btnAddBall: cc.Node = null;



    @property(cc.Sprite)
    bg: cc.Sprite = null;
    @property(cc.SpriteFrame)
    safeBg: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    dangerBg: cc.SpriteFrame = null;
    @property(cc.Label)
    lbl_level: cc.Label = null;

    @property(cc.Node)
    btn_cach_out: cc.Node = null;

    @property(cc.Label)
    lblName: cc.Label = null;
    /** 本次游戏,未开启的红包 */
    _RedBags: Array<number> = [];
    _redBagBtnInfo: any = null;

    showAdBtnTween: any;

    _boxMap: Map<number, { type: number, amount: number, num: number }>;
    onLoad() {
        super.onLoad();
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
        cc.director.getCollisionManager().enabledDrawBoundingBox = true;

        f.UI.OnClick(this.btnPause, this.OnClickBtnPause, this);

        for (let i = 0; i < this.content.gridPrefabs.length; i++) {
            const el = this.content.gridPrefabs[i];
            let el2 = AssetManager.SFs[i + 1];
            if (el2) {
                el.getComponent(GameBox).sp.spriteFrame = el2;
            }
        }
        EffectManager.Init({
            hit: this.hit,
            explode: this.expl
        })
        this.OnClickStart();

        cc.loader.load({ url: User.avatar, type: 'jpg' }, () => {

        }, (err, texture) => {
            let sf = new cc.SpriteFrame();
            sf.setTexture(texture);
            this.avatar.spriteFrame = sf;
        });
        f.UI.SetTxt(this.lblName, User.userName);
        this.lbl_cash.string = `${User.balance}`;
    }

    AddUIEventListener() {
        f.UI.OnClick(this.btnRedBag, this.OnClickRedBagBtn, this);
        f.UI.OnClick(this.btnSkin, () => {
            this.OpenDialog(GDefs.GamePage.DialogSelectSkin);
        }, this);
        f.UI.OnClick(this.btnAddBall, this.OnClickAddBall, this);
        f.UI.OnClick(this.btn_cach_out, () => {
            f.UI.OpenDialog(GDefs.GamePage.DialogWithDraw);
        })
        f.UI.OnClick(this.avatar.node, () => {
            this.OpenDialog(GDefs.GamePage.DialogMyInfo)
        })
    }

    AddMessageListeners() {
        this.On(GameEventType.OnClickNextLevel, this.OnClickNextLevel, this);
        this.On(GameEventType.OnClickRePlay, this.OnClickReplay, this);

        this.On(GameEventType.GameResume, this.GameResume, this);
        this.On(GameEventType.GameRestart, this.GameRestart, this);
        this.On(GameEventType.SelectLevel, this.SelectLevel, this);
        this.On(GameEventType.OpenRedBag, this.OnConfirmReceiveRedBag, this);
        this.On(GameEventType.OnClickSelectLevel, this.OnClickSelectLevel, this);
        this.NodeOn(GameEventType.BoxDestroy, this.OnBoxDestroy, this);
        this.NodeOn(GameEventType.GameOver, this.OnGameOver, this);
        this.NodeOn(GameEventType.BoxHit, this.OnBoxHit, this);
        this.On(GameEventType.OnClickSelectSkin, this.OnSelectSkin, this);
        this.On(GameEventType.OnLogout, this.OnLogout, this);
        AudioMgr.PlayGameBGM();
    }

    RemoveMessageListeners() {
        AudioMgr.StopMusic();
    }



    OnClickNextLevel() {
        LevelMgr.index += 1;
        this.OnClickStart();
    }
    OnClickReplay() {
        this.OnClickStart();
    }

    OnClickStart() {
        this.GameStart();
    }

    OnClickBtnPause() {
        this.OpenDialog(GDefs.GamePage.DialogPause);
        this.GamePause();
    }
    GameRestart() {
        this.gameStart = true;

        let id = LevelMgr.GetCurLevelId();
        let index = LevelMgr.GetCurLevelIndex();
        LevelMgr.GetLevelInfo(id, () => {
            let data = LevelMgr._levelInfo;
            f.NodeMgr.Visible(this.btnRedBag, false);
            this.content.Init(data, () => {
                this.StartANewRound();
            });

            let showAdDelay = LevelMgr.showAdDelay;
            let btn_add_ball = this.btnAddBall;
            let lbl = btn_add_ball.getChildByName('lbl').getComponent(cc.Label);
            lbl.string = `球+${LevelMgr.AdBallCnt}`;
            f.UI.SetTxt(this.lbl_level, `关卡-${index + 1}`)
            this.unScheduleShowAdBtn();
            this.scheduleOnce(this.showAdBtnSchedule, showAdDelay);
        })
        this._RedBags = [];
        this.bg.spriteFrame = this.safeBg
        this.SetSkin();
        this.btnRedBag.active = false;
        this.btnAddBall.active = false;
        this._boxMap = new Map<number, { type: number, amount: number, num: number }>()
        // 重复请求 收益
        this.unschedule(this.ReqBonus);
        this.schedule(this.ReqBonus, 3, cc.macro.REPEAT_FOREVER);
    }

    unScheduleShowAdBtn() {
        if (this.showAdBtnTween) {
            this.showAdBtnTween.stop();
        }
        this.unschedule(this.showAdBtnSchedule);
    }
    showAdBtnSchedule() {
        let btn_add_ball = this.btnAddBall;
        let showAdDuration = LevelMgr.showAdDuration;
        if (this.showAdBtnTween) {
            this.showAdBtnTween.stop();
        }
        btn_add_ball.active = true;
        let timeLabel = btn_add_ball.getChildByName('lbl_time').getComponent(cc.Label);
        this.showAdBtnTween = cc.tween({ a: 1 })
            .to(
                showAdDuration,
                { a: 3 },
                {
                    progress: (a, b, c, ratio) => {
                        let left = Math.floor((1 - ratio) * showAdDuration);
                        timeLabel.string = `${left}s`;
                    }
                }
            )
            .call(() => {
                btn_add_ball.active = false;
            })
            .start();
        // btn_add_ball.stopAllActions();
        // btn_add_ball.active = true;
        // btn_add_ball.runAction(
        //     cc.sequence(
        //         cc.delayTime(showAdDuration),
        //         cc.callFunc(() => {
        //             btn_add_ball.active = false;
        //         })
        //     )
        // )
    }
    SetSkin() {
        let sf = SkinMgr.getCurrentSkin();
        this.content.setSkin(sf);
    }
    GameStart() {
        if (this.gameStart) {
            this.GameResume();
            return;
        } else {
            this.GameRestart();
        }
    }
    GamePause() {
        // cc.director.pause()
        this.gamePause = false;
    }
    GameResume() {
        this.gamePause = true;
        // cc.director.resume()
    }
    StartANewRound() {
        console.log("Game Start a New Round")
        let seq = new f.Seq();
        seq.then(this.RoundBefore, this);
        seq.then(this.RoundStart, this);
        seq.then(this.RoundEnd, this);
        seq.then(this.RoundAfter, this);
        seq.then(this.CheckGameEnd, this);
        seq.run();
    }
    RoundBefore(end) {
        this.content.StartControl(end);
    }

    RoundStart(end) {
        this.content.StartAction(end);
    }
    RoundEnd(end) {
        console.log("Game Round End")
        // 回合结束之后 会有一些 清楚道具的操作,在此要一帧过后 继续执行
        this.scheduleOnce(() => {
            this.content.StartBoxFall(end);
        })
    }

    RoundAfter(end) {
        this.content.CreateStoreBoxes(end);
    }

    CheckGameEnd(end) {
        console.log("Round After");
        if (this.content.IsGameFailed()) {
            this.gameStart = false;
            this._success = false;
            this.GameEnd();
        } else if (this.content.IsGameSuccess()) {
            this.gameStart = false;
            this._success = true;

            let id = LevelMgr.GetCurLevelId();
            LevelMgr.UnlockLevel(id);

            this.GameEnd();
        } else
        // if (this.gameStart) 
        {
            this.StartANewRound();
            let isDanger = this.content.checkDanger();

            if (isDanger) {
                this.bg.spriteFrame = this.dangerBg;
                AudioMgr.PlayWarningEffect();
            } else {
                this.bg.spriteFrame = this.safeBg
            }
        }
        //  else {
        //     this.GameEnd();
        // }
    }

    GameEnd() {
        if (this._RedBags.length > 0) {
            this.OnClickRedBagBtn();
        }
        let param: GDefs.DialogParams.GameOver = {
            result: this._success,
            starCnt: 3,
        }
        this.OpenDialogInline(GDefs.GamePage.DialogGameOver, param);
    }

    OnGameOver(e: cc.Event.EventCustom) {
        console.log("OnGameOver");
        let rusult = e.detail;
        this.gameStart = false;
        this._success = rusult;

        // 最后一次请求收益, 结束轮询
        this.ReqBonus();
        this.unschedule(this.ReqBonus);
    }
    // update (dt) {}

    SelectLevel() {
        this.OpenDialog(GDefs.GamePage.DialogSelectLevel);
    }

    OnClickSelectLevel(n) {
        let e = n - 1;
        let level = n < 10 ? '0' + n : n;
        f.UI.Confirm({
            txt: `是否切换到关卡-${level}`,
            okTxt: '是',
            cancelTxt: '返回',
            okcb: () => {
                LevelMgr.index = e;
                this.GameRestart();
                f.UI.CloseDialog(GDefs.GamePage.DialogPause);
                f.UI.CloseDialog(GDefs.GamePage.DialogSelectLevel);
            },
        })
        console.log("OnClickSelectLevel", n);
    }

    OnBoxDestroy(e: cc.Event.EventCustom) {
        let isRedBag = e.detail;
        if (isRedBag) {
            let redBagId = e.target.uuid;
            this._RedBags.push(redBagId);
            // TODO show redbag button
            this._redBagBtnInfo = redBagId;
            console.log('$Game #OnBoxDestroy a RedBag destroy');

            f.NodeMgr.Visible(this.btnRedBag, true);
            let b = this._boxMap.get(GridType.redbag);
            let lbl = this.btnRedBag.getChildByName('lbl').getComponent(cc.Label);
            f.UI.SetTxt(lbl, `领取+${(b.num * b.amount).toFixed(2)}元`);

            f.Action.ScaleTo({
                target: this.btnRedBag,
                delay: 8,
                x: 0,
                cb: () => {
                    f.NodeMgr.Visible(this.btnRedBag, false);
                }
            })
        }

    }

    OnClickRedBagBtn() {
        f.NodeMgr.Visible(this.btnRedBag, false);
        let id = LevelMgr.GetCurLevelId();
        let b = this._boxMap.get(GridType.redbag);
        this.OpenDialogInline(GDefs.GamePage.DialogGetRedBag, {
            id,
            number: b.num,
            amount: b.amount,
        });
    }

    OnClickAddBall() {
        f.NodeMgr.Visible(this.btnAddBall, false);
        AdMgr.showRewardAd(() => {
            this.content.addBall(LevelMgr.AdBallCnt);
        }, AdSlot.addBall_reward)
    }

    OnConfirmReceiveRedBag() {
        f.Arr.remove(this._RedBags, this._redBagBtnInfo);
        User.RefreshBalance((number) => {
            f.UI.SetTxt(this.lbl_cash, number);
        })
    }

    OnSelectSkin(data) {
        // TODO: Change All Ball's Skin if exist;

        f.Res.SetStorage('ball_skin', data);
        let sf = SkinMgr.getSkin(data);
        this.content.setSkin(sf);
    }

    OnBoxHit(data) {
        let amount = data.detail.amount;
        let type = data.detail.type;
        let b = this._boxMap.get(type);
        if (!b) {
            b = { type, amount, num: 0 }
        }
        b.num += 1;
        this._boxMap.set(type, b);
    }

    ReqBonus() {
        console.log('ReqBonus');
        let b = this._boxMap.get(GridType.box);
        if (!b || !b.num) {
            return;
        }

        let id = LevelMgr.GetCurLevelId();;
        let elementType = GridType.box;
        let number = b.num;
        Net.HitBox({ id, items: [{ elementType, number }] }, (resp) => {
            console.log(`open HitBox resp`)
        })
        b = { type: GridType.box, amount: b.amount, num: 0 };
        this._boxMap.set(GridType.box, b);
    }

    OnLogout() {
        this.Close();
        f.UI.OpenPage(GDefs.GamePage.GameTopPage);
    }
}
