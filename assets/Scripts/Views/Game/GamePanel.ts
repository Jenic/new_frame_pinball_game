// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import AudioMgr from "../../Common/AudioMgr";
import LevelMgr from "../../Common/LevelMgr";
import SkinMgr from "../../Common/SkinMgr";
import Util from "../../Common/Util";
import DialogBase from "../../Framework/View/DialogBase";
import { GameEventType, GameItemType, GridType } from "../GameEvent";
import GameBox from "./GameBox";

const { ccclass, property } = cc._decorator;

const GridSize = 69;
const GridColCnt = 9;
const GridIndent = 1;
@ccclass
export default class GamePanel extends DialogBase {

    _ControlCallback: () => void = null;

    @property(cc.Node)
    content: cc.Node = null;
    @property(cc.Node)
    line1: cc.Node = null;
    @property(cc.Node)
    line2: cc.Node = null;
    @property(cc.Node)
    ball: cc.Node = null;
    @property(cc.Node)
    fakeBall: cc.Node = null;
    @property(cc.Node)
    gridPrefabs: cc.Node[] = [];

    @property(cc.Node)
    trigger: cc.Node = null;

    @property(cc.Node)
    bomb: cc.Node = null;
    @property(cc.Node)
    horizontalLaser: cc.Node = null;
    @property(cc.Node)
    verticalLaser: cc.Node = null;

    @property(cc.Label)
    lblCnt: cc.Label = null;

    _GameBoxes: Array<cc.Node> = [];
    /** 状态控制 */
    _gameAction: boolean = false;
    _gameControl: boolean = false;
    /** UI属性记录 */
    _bottomY: number = 0;
    _startPosition: cc.Vec2 = null;
    _endPosition: cc.Vec2 = null;
    _dispatchAngle: number = null;
    _ballCnt: number = 1;
    _actionWait: f.Wait = null;

    _leftItemArr: Array<any>;

    onLoad() {
        this._bottomY = -this.node.height / 2 + this.ball.height / 2;
        this.node.on(cc.Node.EventType.TOUCH_START, this.OnTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.OnTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.OnTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.OnTouchEnd, this);

        this.node.on(GameEventType.BallAdd, this.OnBallAdd, this);
        this.node.on(GameEventType.BallEnd, this.OnBallEnd, this);
        this.node.on(GameEventType.BallError, this.OnBallError, this);
        this.node.on(GameEventType.BoxDestroy, this.OnBoxDestroy, this);

        this.node.on(GameEventType.ItemUsedInRound, this.OnItemUsed, this);

        this.trigger.active = false;
    }

    OnTouchMove(e: cc.Event.EventTouch) {
        if (!this._gameControl) {
            return;
        }
        let worldPos = e.touch.getLocation();
        let origin = this.node.convertToWorldSpaceAR(this._startPosition);
        let forward = worldPos.sub(origin).normalizeSelf();

        // 弧度
        let angle = cc.Vec2.angle(forward, cc.v2(1.0));
        angle = f.Util.Clamp(angle, 0.05, Math.PI - 0.05);

        forward = cc.v2(Math.cos(angle), Math.sin(angle));

        var results = cc.director.getPhysicsManager().rayCast(origin, origin.add(forward.multiplyScalar(2000)), cc.RayCastType.Closest);

        for (let i = 0; i < results.length; i++) {
            const el = results[i];
            //! 检测点 是在墙体上的，而球有体积，所以检测点往回X距离 才是球的最远射线距离:以下求X
            // 碰撞点法向量
            let normal = el.normal;
            let pos = this.node.convertToNodeSpaceAR(el.point);
            // 引导线
            let line = pos.sub(this._startPosition);
            let lineForward = line.normalize();
            let X = 1 / normal.dot(lineForward) * this.fakeBall.width / 2;
            let distance = line.mag() + X;
            let newLine = lineForward.multiplyScalar(distance);
            let newPos = this._startPosition.add(newLine);

            // this.fakeBall.x = newPos.x;
            // this.fakeBall.y = newPos.y;
            // this.fakeBall.active = true;

            this.line1.x = this._startPosition.x;
            this.line1.y = this._startPosition.y;
            this.line1.angle = angle / Math.PI * 180;
            this.line1.width = distance;
            this.line1.active = true;

            let line2P = f.Util.GetSymmetricPoint(newPos, newPos.add(normal), this._startPosition);

            let line2Foward = line2P.sub(newPos).normalizeSelf();
            let lin2Angle = cc.Vec2.angle(line2Foward, cc.v2(1.0));
            let line2AnglePositive = line2Foward.dot(cc.v2(0, 1)) > 0 ? 1 : -1;
            this.line2.x = newPos.x;
            this.line2.y = newPos.y;
            this.line2.angle = lin2Angle / Math.PI * line2AnglePositive * 180;
            this.line2.active = true;
            let line2Length = 288;

            let newPosInWorld = this.node.convertToWorldSpaceAR(newPos);
            var line2Result = cc.director.getPhysicsManager().rayCast(newPosInWorld, newPosInWorld.add(line2Foward.mulSelf(300)), cc.RayCastType.All);

            for (let i = 0; i < line2Result.length; i++) {
                const el2 = line2Result[i];
                let line2End = this.node.convertToNodeSpaceAR(el2.point);
                let maxLine = line2End.sub(newPos);
                let length = maxLine.mag();
                if (length < line2Length) {
                    line2Length = length;
                }
            }
            this.line2.width = line2Length;

        }

        this._dispatchAngle = angle;
    }

    OnTouchEnd(e: cc.Event) {
        if (this._gameControl) {
            this._gameControl = false;
            this._ControlCallback();
            this.fakeBall.active = false;
            this.line1.active = false;
            this.line2.active = false;

        }
    }

    OnItemUsed(e: cc.Event.EventCustom) {
        e.stopPropagationImmediate();
        let node = e.target;
        let itemType = e.detail;

        switch (itemType) {
            case GameItemType.Bomb:
                this.SkillBomb(cc.v2(node.x, node.y));
                AudioMgr.PlayBombEffect();
                break;
            case GameItemType.CrossLaser:
                this.SkillVerticalLaser(cc.v2(node.x, node.y));
                this.SkillHorizontalLaser(cc.v2(node.x, node.y));
                AudioMgr.PlayLaserEffect();
                break;
            case GameItemType.HorizontalLaser:
                this.SkillHorizontalLaser(cc.v2(node.x, node.y));
                AudioMgr.PlayLaserEffect();
                break;
            case GameItemType.VerticalLaser:
                this.SkillVerticalLaser(cc.v2(node.x, node.y));
                AudioMgr.PlayLaserEffect();
            default:
                break;
        }
    }


    Init(data, cb: () => void) {
        console.log('game panel init')
        this._ballCnt = LevelMgr.startBallCnt;
        if (cc.sys.isBrowser) {
            this._ballCnt += 30;
        }

        if (this._eBalls && this._eBalls.length > 0) {
            for (let i = 0; i < this._eBalls.length; i++) {
                const el = this._eBalls[i];
                if(cc.isValid(el)){

                    let rig = el.getComponent(cc.RigidBody);
                    if(rig){
                        rig.linearVelocity = cc.v2();
                    }
                    el.stopAllActions();
                    el.destroy();
                }
            }
        }

        this._startPosition = cc.v2(0, this._bottomY);

        this.fakeBall.active = true;
        this.fakeBall.x = this._startPosition.x;
        this.fakeBall.y = this._startPosition.y;

        // let pos = cc.v2(-this.node.width / 2 + GridSize / 2, this.node.height / 2 - GridSize / 2);
        let wait = new f.Wait(cb);
        this.content.removeAllChildren();
        this._GameBoxes = [];
        this.content.x = 0;
        this.content.y = 0;
        let OriginGridRows = LevelMgr.originRows;
        let dataRows = Math.ceil(data.length / GridColCnt);
        let useData;
        if (dataRows > OriginGridRows) {
            let leftRows = dataRows - OriginGridRows;
            let lastIndex = leftRows * GridColCnt;
            let spliceNum = data.length - lastIndex;
            useData = data.splice(lastIndex, spliceNum);
            this._leftItemArr = data;
        } else {
            useData = data;
            this._leftItemArr = [];
        }
        for (let i = 0; i < useData.length; i++) {
            const el = useData[i];
            let x = i % GridColCnt;
            let y = Math.floor(i / GridColCnt);
            if (el.elementType && el.number > 0) {
                this.createBox(el, x, y, wait);
            }
        }
        this.showBallCntLabel();
    }

    createBox(el: any, x: number, y: number, wait: f.Wait) {
        if (!el.elementType) return;
        let pos = cc.v2(-this.node.width / 2 + GridSize / 2, this.node.height / 2 - GridSize / 2);
        let grid = cc.instantiate(this.gridPrefabs[el.elementType - 1]);
        grid.parent = this.content;
        this._GameBoxes.push(grid);
        grid.x = pos.x + x * (GridSize + GridIndent);
        grid.y = pos.y - y * (GridSize + GridIndent);
        // let rigid = grid.getChildByName('rigid');
        // rigid.active = false;;
        grid.scale = 0;
        wait.WaitFor(String(grid.uuid))
        f.Action.ScaleTo({
            target: grid,
            delay: 0.02 * x,
            duration: 0.1,
            x: 1,
            cb: () => {
                wait.Ready(String(grid.uuid));
                // rigid.active = true;
            }
        })
        let box: GameBox = grid.getComponent(GameBox);
        box.Init(el);
    }

    StartControl(cb: () => void) {
        this._ControlCallback = cb;
        this._gameControl = true;
        this.trigger.active = false;
        console.log('start control')

        this.showBallCntLabel();
    }

    showBallCntLabel() {
        this.lblCnt.node.active = true;
        this.lblCnt.string = `x${this._ballCnt}`;
        this.lblCnt.node.x = this._startPosition.x + 30;
        this.lblCnt.node.y = this._startPosition.y + 30;
    }

    _eBalls: any[];
    StartAction(cb: () => void) {
        console.log('start action');
        this._gameAction = true;
        this.trigger.active = true;
        this.lblCnt.node.active = false;

        // wait for 结束回调
        this._actionWait = new f.Wait(() => {
            console.log('end action');
            this._gameAction = false;
            this._startPosition = this._endPosition;
            cb();
        });
        this._endPosition = null;
        let speed = 2400;
        this.ball.active = false;
        let dispatchIndent = 0.1;

        this._eBalls = []
        for (let i = 0; i < this._ballCnt; i++) {
            let ball = cc.instantiate(this.ball);
            ball.active = true;
            this._eBalls.push(ball);
            ball.runAction(cc.sequence(
                cc.delayTime(i * dispatchIndent),
                cc.callFunc(() => {
                    ball.x = this._startPosition.x;
                    ball.y = this._startPosition.y;
                    ball.parent = this.node;
                    let forward = cc.v2(Math.cos(this._dispatchAngle), Math.sin(this._dispatchAngle));
                    forward.mulSelf(speed);
                    ball.getComponent(cc.RigidBody).linearVelocity = forward;
                })
            ))
            this._actionWait.WaitFor(ball.uuid);
        }
        // this.ball.runAction(cc.moveTo(1, this.ball.x + 100, this.ball.y + 100))
    }

    StartBoxFall(cb: () => void) {
        console.log("Start box fall");
        let noBox = true;
        let waitBallFall = new f.Wait(cb);
        for (let i = 0; i < this._GameBoxes.length; i++) {
            const el = this._GameBoxes[i];
            if (cc.isValid(el) && !Util.isDestroy(el)) {
                noBox = false;
                waitBallFall.WaitFor(el.uuid);
                f.Action.MoveBy({
                    target: el,
                    duration: 1,
                    y: - GridSize - GridIndent,
                    cb: () => {
                        waitBallFall.Ready(el.uuid);
                    }
                })
            }
        }

        if (noBox) {
            cb();
        }
    }

    CreateStoreBoxes(end) {
        if (this._leftItemArr.length > 0) {
            let wait = new f.Wait(end);
            let useData = this._leftItemArr.splice(this._leftItemArr.length - GridColCnt, GridColCnt);
            for (let i = 0; i < useData.length; i++) {
                const el = useData[i];
                this.createBox(el, i, 0, wait);
            }
        } else {
            end();
        }
    }
    OnBallAdd(e: cc.Event) {
        let node = e.target;
        let speed = 2400;

        let ball = cc.instantiate(this.ball);
        this._ballCnt++;
        ball.active = true;
        ball.x = node.x;
        ball.y = node.y;
        ball.parent = this.node;
        let angle = f.Util.Random(0.05, Math.PI - 0.05);
        let forward = cc.v2(Math.cos(angle), Math.sin(angle));
        forward.mulSelf(speed);
        ball.getComponent(cc.RigidBody).linearVelocity = forward;
        this._actionWait.WaitFor(ball.uuid);

    }

    OnBallEnd(e: cc.Event) {
        let node: cc.Node = e.target;
        node.getComponent(cc.RigidBody).linearVelocity = cc.v2();
        if (!this._endPosition) {
            this._endPosition = cc.v2(node.x, this._bottomY);
            this._actionWait.Ready(node.uuid);
            node.destroy();

            this.fakeBall.active = true;
            this.fakeBall.x = node.x;
            this.fakeBall.y = this._bottomY;
        } else {
            // 直线回收写法
            let speed = 2400;
            let distance = cc.v2(node.x, node.y).sub(this._endPosition).mag();
            f.Action.MoveTo({
                target: node,
                duration: distance / speed,
                x: this._endPosition.x,
                y: this._endPosition.y,
                cb: () => {
                    this._actionWait.Ready(node.uuid);
                    node.destroy();
                }
            })

            // let rigid = node.getComponent(cc.RigidBody)
            // let v2 = rigid.linearVelocity;
            // rigid.linearVelocity = cc.v2();
            // let scale = 0.2;

            // let randomx = node.x + v2.x * scale;
            // randomx = f.Util.Clamp(randomx, -this.node.width / 2, this.node.width / 2);

            // node.runAction(cc.sequence(
            //     cc.bezierTo(0.1,
            //         [cc.v2(node.x, node.y),
            //         cc.v2((node.x + randomx) / 2, this._bottomY + 200),
            //         cc.v2(randomx, this._bottomY)
            //         ]),
            //     cc.callFunc(() => {
            //         this._actionWait.Ready(node.uuid);
            //     })
            // ))
        }
    }

    OnBallError(e: cc.Event) {
        let node: cc.Node = e.target;
        node.getComponent(cc.RigidBody).linearVelocity = cc.v2();

        let speed = 2400;
        let distance = cc.v2(node.x, node.y).sub(this._endPosition).mag();
        f.Action.MoveTo({
            target: node,
            duration: distance / speed,
            x: this._endPosition.x,
            y: this._endPosition.y,
            cb: () => {
                this._actionWait.Ready(node.uuid);
                node.destroy();
            }
        })
    }

    OnBoxDestroy(e: cc.Event.EventCustom) {
        console.log('on box destroy', this.content)
        let over = true;
        for (let i = 0; i < this._GameBoxes.length; i++) {
            const el = this._GameBoxes[i];
            if (!Util.isDestroy(el)) {
                over = false;
                break;
            }
        }

        if (over) {
            let e = new cc.Event.EventCustom(GameEventType.GameOver, true);
            e.setUserData(true);
            this.node.dispatchEvent(e);
        }
    }

    IsGameSuccess() {
        console.log('on box destroy', this.content)
        let over = true;
        for (let i = 0; i < this._GameBoxes.length; i++) {
            const el = this._GameBoxes[i];
            if (!Util.isDestroy(el)) {
                over = false;
                break;
            }
        }
        return over;
    }

    IsGameFailed() {

        for (let i = 0; i < this._GameBoxes.length; i++) {
            const el = this._GameBoxes[i];
            if (el['__destroy']) {
                continue;
            }
            if (el.position.y < -440) {
                return true;
            }
        }
        return false;
    }

    VelocityLimit(node, speed) {
        if (!node) {
            cc.warn('#VelocityLimit: node 不存在', node);
            return;
        }

        let rigid = node.getComponent(cc.RigidBody);
        if (!rigid) {
            cc.warn('#VelocityLimit: node 节点上 不存在rigidbody 组件');
            return;
        }
        let v2: cc.Vec2 = rigid.linearVelocity;
        let sp = v2.mag();
        if (sp > speed) {
            v2.normalizeSelf().multiplyScalar(speed);
        };
        rigid.linearVelocity = v2;
    }


    /**
     * 在某点发生爆炸
     * @param pos 爆炸的世界坐标
     */
    public SkillBomb(pos: cc.Vec2) {
        let node = cc.instantiate(this.bomb);
        this.showSkillAtPosition(node, pos.x, pos.y);
    }

    /**
     * 在某点发生 横向激光
     * @param pos 激光的世界坐标
     */
    public SkillHorizontalLaser(pos: cc.Vec2) {
        let node = cc.instantiate(this.horizontalLaser);
        this.showSkillAtPosition(node, pos.x, pos.y);
    }
    /**
     * 在某点发生 纵向激光
     * @param pos 激光的世界坐标
     */
    public SkillVerticalLaser(pos: cc.Vec2) {
        let node = cc.instantiate(this.verticalLaser);
        this.showSkillAtPosition(node, pos.x, pos.y);
    }

    showSkillAtPosition(node: cc.Node, x: number, y: number) {
        node.parent = this.node;
        node.x = x;
        node.y = y
        node.active = true;
        f.Action.FadeOut({
            target: node,
            delay: 0.15,
            duration: 0.15,
            cb: () => {
                node.destroy();
            }
        });
    }

    setSkin(sf: cc.SpriteFrame) {
        this.ball.getComponent(cc.Sprite).spriteFrame = sf;
        this.fakeBall.getComponent(cc.Sprite).spriteFrame = sf;
    }

    addBall(n: number) {
        this._ballCnt += n;
        f.UI.Tip(`小球数量 +${n}`);
    }

    checkDanger() {
        for (let i = 0; i < this._GameBoxes.length; i++) {
            const el = this._GameBoxes[i];
            if (el['__destroy']) {
                continue;
            }
            if (el.position.y < -370) {
                return true;
            }
        }

        return false;
    }
}
