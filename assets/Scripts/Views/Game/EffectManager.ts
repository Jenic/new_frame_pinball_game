
export default class EffectManager {

    static Inited: boolean = false;
    static _hitPrefab: cc.Node | cc.Prefab;
    static _explodePrefab: cc.Node | cc.Prefab;
    static _hitNodePool: cc.NodePool;
    static _explodeNodePool: cc.NodePool;

    public static Init(info: { hit: cc.Node | cc.Prefab, explode: cc.Node | cc.Prefab }) {
        if (this.Inited) return;
        this.Inited = true;;
        this._hitPrefab = info.hit;
        this._explodePrefab = info.explode;

        this._explodeNodePool = new cc.NodePool();
        this._hitNodePool = new cc.NodePool();
    }

    public static GetHit() {
        let node: cc.Node;
        if (this._hitNodePool.size() > 0) {
            node = this._hitNodePool.get();
        } else {
            node = cc.instantiate(this._hitPrefab) as cc.Node;
        }
        return node;
    }
    public static PutHit(node: cc.Node) {
        this._hitNodePool.put(node);
    }

    public static GetExplode() {
        let node: cc.Node;
        if (this._explodeNodePool.size() > 0) {
            node = this._explodeNodePool.get();
        } else {
            node = cc.instantiate(this._explodePrefab) as cc.Node;
        }
        return node;
    }

    public static PutExplode(node: cc.Node) {
        this._explodeNodePool.put(node);
    }
}
