import AudioMgr from "../../Common/AudioMgr";
import Util from "../../Common/Util";
import { GameEventType, GameItemType, GridType } from "../GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameBall extends cc.Component {


    // LIFE-CYCLE CALLBACKS:
    // 撞击墙的次数,连续撞击墙20次 为错误,回收节点;
    _Cnt: number = 0;

    onBeginContact(collider, self, other) {
        let group = other.node.group;
        if (group === 'wall') {
            ++this._Cnt;
            if (other.tag == '1') {
                // this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2();
                console.log('a ball end');
                this.node.dispatchEvent(new cc.Event(GameEventType.BallEnd, true));
                AudioMgr.PlayHitWallEffect();
            } else {
                AudioMgr.PlayLandingEffect();
            }
        }

        if (this._Cnt > 20) {


            // this.node.dispatchEvent(new cc.Event(GameEventType.BallError, true));
            let rigid = this.node.getComponent(cc.RigidBody);
            let v2 = rigid.linearVelocity;
            rigid.linearVelocity = v2.add(cc.v2(0, -300));
        }
    }

    // update (dt) {}
}
