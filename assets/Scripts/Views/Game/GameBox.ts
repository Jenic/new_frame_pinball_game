
import AssetManager from "../../Common/AssetsMgr";
import AudioMgr from "../../Common/AudioMgr";
import Util from "../../Common/Util";
import { GameEventType, GameItemType, GridType } from "../GameEvent";
import EffectManager from "./EffectManager";


const { ccclass, property } = cc._decorator;

@ccclass
export default class GameBox extends cc.Component {

    @property(cc.Sprite)
    sp: cc.Sprite = null;

    @property(cc.Label)
    lbl: cc.Label = null;

    // @property(cc.SpriteFrame)
    // sfs: cc.SpriteFrame[] = [];

    type: number;
    // get frame(): cc.SpriteFrame[] {
    //     let arr = [null, null, null, null, null, null];
    //     if (this.sfs && this.sfs.length >= 5) {
    //         arr = this.sfs;
    //     }
    //     return arr;;
    // }
    _num: number = 0;
    get num(): number { return this._num; }
    set num(value: number) {

        this._num = value;
        f.UI.SetTxt(this.lbl, value);

        if (this.type === GridType.box) {
            this.setBoxSpriteFrame();
        }

        let show = value > 1;
        f.NodeMgr.Visible(this.lbl?.node, show);

        if (value === 0) {
            this.Dest();
        }
    }

    // ---- 修改打击状态 -----
    _isHited = false;
    set isHited(value: boolean) {
        this._isHited = value;
        this.setBoxSpriteFrame();
    }
    setBoxSpriteFrame() {
        if (this.type === GridType.box) {

            let spArr = AssetManager.boxSFs;
            if (this._isHited) {
                spArr = AssetManager.boxHitedSFs;
            }
            // let index = this._num % 5;
            let index = Math.floor(this._num / 10);
            if (index > 4) index = 4;
            let sf = spArr[index];

            if (!Util.isDestroy(this.node)) {
                this.sp.spriteFrame = sf;
            }
        }
    }
    _hitTween: any;
    clearTween() {
        if (this._hitTween) {
            this._hitTween.stop();
        }
    }
    changeHitState() {
        this.clearTween();
        this.isHited = true;

        this._hitTween = cc.tween({ a: 1 })
            .to(1, { a: 2 })
            .call(() => {
                this.isHited = false;
            })
            .start()
    }

    amount: number;

    Init(data: any) {
        // 0: {id: "351020104349257728", elementType: 2, number: 12, amount: 1e-7}
        this.type = data.elementType;
        this.num = data.number;
        this.amount = data.amount;

        this.clearTween();
        this.isHited = false;
    }

    Hit() {
        this.num -= 1;
        if (this.type === GridType.box || this.type === GridType.redbag) {
            let hit = EffectManager.GetHit();
            hit.active = true;
            hit.parent = this.node.parent;
            hit.x = this.node.x;
            hit.y = this.node.y;
            hit.scale = 1;
            f.Action.ScaleTo({
                target: hit,
                x: 1.32,
                duration: 0.6,
                cb: () => {
                    EffectManager.PutHit(hit);
                }
            });

            let event = new cc.Event.EventCustom(GameEventType.BoxHit, true);
            event.setUserData({
                amount: this.amount,
                type: this.type,
            });
            this.node.dispatchEvent(event);

            if (this.type === GridType.box) {
                this.changeHitState();
            }
        }
    }

    onBeginContact(collider, self, other) {
        let group = other.node.group;
        if (group === 'skill') {
            // 技能只对 box 和 红包生效
            if (this.type === GridType.box || this.type === GridType.redbag) {
                // 技能碰撞 只生效一次
                let arr = other.node['__effected'] || [];
                let uuid = this.node.uuid;
                if (!f.Arr.contains(arr, uuid)) {
                    arr.push(uuid);
                    other.node['__effected'] = arr;
                    // console.log(`node ${this.node.uuid} on beginContact`);
                    this.Hit();
                    AudioMgr.PlayHitBoxEffect();
                }
            }
        }
        if (group === 'ball') {
            let useItemTrigger = new cc.Event.EventCustom(GameEventType.ItemUsedInRound, true);
            switch (this.type) {
                case GridType.add:
                    this.node.dispatchEvent(new cc.Event(GameEventType.BallAdd, true));
                    AudioMgr.PlayAddBallEffect();
                    break;
                case GridType.bomb:
                    useItemTrigger.setUserData(GameItemType.Bomb);
                    this.node.dispatchEvent(useItemTrigger);
                    break;
                case GridType.crossLaser:
                    useItemTrigger.setUserData(GameItemType.CrossLaser);
                    this.node.dispatchEvent(useItemTrigger);
                    break;
                case GridType.horizontalLaser:
                    useItemTrigger.setUserData(GameItemType.HorizontalLaser);
                    this.node.dispatchEvent(useItemTrigger);
                    break;
                case GridType.verticalLaser:
                    useItemTrigger.setUserData(GameItemType.VerticalLaser);
                    this.node.dispatchEvent(useItemTrigger);
                    break;
            }

            this.Hit();
        }

    }


    Dest() {
        Util.Destroy(this.node);
        let event = new cc.Event.EventCustom(GameEventType.BoxDestroy, true);
        event.setUserData(this.type === GridType.redbag);
        this.node.dispatchEvent(event);

        if (this.type === GridType.box || this.type === GridType.redbag) {

            let node = EffectManager.GetExplode();
            node.x = this.node.x;
            node.y = this.node.y;
            node.parent = this.node.parent;
            let particle: cc.ParticleSystem = f.NodeMgr.AddSingletonComponent(node, cc.ParticleSystem) as cc.ParticleSystem;
            particle.spriteFrame = this.sp.spriteFrame;
            particle.resetSystem();
            f.Action.ScaleTo({
                target: node,
                duration: 2,
                x: 1,
                cb: () => {
                    EffectManager.PutExplode(node);
                }
            })
        }

    }

    Clear() {
        this.node.destroy();
    }
    // update (dt) {}
}
