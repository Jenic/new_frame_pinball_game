export enum SystemEvent {
    TryLogin = 'try_login',
    Login = 'login',
    Logout = 'logout',
    Refresh = 'refresh',
}

export enum GameEventType {
    BallEnd = 'ball_end',
    BallAdd = 'ball_add',
    BallError = 'ball_error',
    BoxDestroy = 'box_destroy',
    BoxHit = 'box_hit',
    GameOver = 'game_over',

    OnClickRePlay = 'on_click_replay',
    OnClickNextLevel = 'on_click_next_level',

    GetRedBag = 'get_red_bag',
    OpenRedBag = 'open_red_bag',
    ItemUsedInRound = 'item_in_round',

    GameRestart = 'game_restart',
    GameResume = 'game_resume',
    SelectLevel = 'select_level',
    OnClickSelectLevel = 'on_click_select_level',
    OnClickSelectSkin = 'on_click_select_skin',

    OnUpdateSkinList = 'on_update_skin_list',
    OnLogout = 'on_logout'
}

export enum GameItemType {
    Bomb = 1,
    CrossLaser,
    HorizontalLaser,
    VerticalLaser,
}

export enum GridType {
    add = 1,
    box,
    redbag,
    bomb,
    crossLaser,
    horizontalLaser,
    verticalLaser,
}