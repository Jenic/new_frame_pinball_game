import SkinMgr from "../../Common/SkinMgr";
import DialogBase from "../../Framework/View/DialogBase";
import { List } from "../../Framework/View/List";
import { GameEventType } from "../GameEvent";
import SkinListItem from "./SkinListItem";


const { ccclass, property } = cc._decorator;

@ccclass
export default class DialogSelectSkin extends DialogBase {

    @property(cc.Node)
    btnConfirm: cc.Node = null;

    @property(List)
    List: List = null;


    AddUIEventListener() {
        f.UI.OnClick(this.btnConfirm, () => {
            // TODO Set Default Skin to SkinMgr
            let checkNodes = this.List.checkedItems;
            let comp = checkNodes[0].getComponent(SkinListItem);
            this.Declare(GameEventType.OnClickSelectSkin, comp._ballCode);
            this.Close();
        });
    }

    AddMessageListeners() {
        this.NodeOn(GameEventType.OnUpdateSkinList, this.UpdateUI, this);
    }

    UpdateUI() {
        console.log("Skin UpdateUI");
        let skinList = SkinMgr._skinData;
        this.List.SetData(skinList);
    }


    // update (dt) {}
}
