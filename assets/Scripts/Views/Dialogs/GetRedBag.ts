// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import AdMgr, { AdSlot } from "../../Common/AdMgr";
import DialogBase from "../../Framework/View/DialogBase";
import Net from "../../Network/Net";
import { GameEventType, GridType } from "../GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GetRedBag extends DialogBase {

    @property(cc.Label)
    lbl_money: cc.Label = null;
    @property(cc.Node)
    btnOpen: cc.Node = null;

    @property(cc.Node)
    btnClose: cc.Node = null;
    @property(cc.Label)
    lbl_close: cc.Label = null;

    _coolDown: number;
    _tweener: any;

    UpdateUI() {
        let param = this.GetPageParam();
        this.lbl_money.string = `${(param.amount * param.number).toFixed(2)}元`;

        this.tweenClose();
    }

    tweenClose() {
        if (this._tweener) {
            this._tweener.stop();
        }

        this._tweener = cc.tween({ a: 1 })
            .to(3, { a: 3 }, {
                progress: (a, b, c, ratio) => {
                    let left = Math.ceil(3 - 3 * ratio);
                    this._coolDown = left;
                    let str = '放弃奖励';
                    if (left > 0) {
                        str += `(${left}s)`
                    }
                    f.UI.SetTxt(this.lbl_close, str);
                }
            })
            .call(() => {
                this._coolDown = 0;
            })
            .start();
    }

    AddUIEventListener() {
        f.UI.OnClick(this.btnOpen, this.OnClickOpen, this);
        f.UI.OnClick(this.btnClose, () => {
            if (this._coolDown <= 0) {
                this.Close();
            }
        }, this);
    }

    OnClickOpen() {
        AdMgr.showRewardAd(() => {
            let param = this.GetPageParam();
            let id = param.id;
            let elementType = GridType.redbag;
            let number = param.number;
            Net.OpenRedBag({ id, items: [{ elementType, number }] }, (resp) => { 
                this.Declare(GameEventType.OpenRedBag);

            });
        }, AdSlot.openRedBag_reward);
        this.Close();
    }


    
    AddMessageListeners() {
        if (cc.sys.isNative) {
            AdMgr.ShowExpressAd(AdSlot.getRedBag_express);
        }
    }

    RemoveMessageListeners() {
        if (cc.sys.isNative) {
            AdMgr.CloseExpressAd(AdSlot.getRedBag_express);
        }
    }
    // update (dt) {}
}
