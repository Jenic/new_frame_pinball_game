import User from "../../Common/User";
import { GDefs } from "../../Defineds/GDefs";
import ViewBase from "../../Framework/View/ViewBase";
import { GameEventType } from "../GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DialogMyInfo extends ViewBase {

    @property(cc.Node)
    btnClose: cc.Node = null;


    @property(cc.Node)
    btnLogout: cc.Node = null;

    @property(cc.Label)
    lblName: cc.Node = null;

    @property(cc.Sprite)
    avatar: cc.Sprite = null;

    @property(cc.Node)
    btnPrivacyPolicy: cc.Node = null;

    @property(cc.Node)
    btnServiceProtocol: cc.Node = null;

    UpdateUI() {

        cc.loader.load({ url: User.avatar, type: 'jpg' }, () => {

        }, (err, texture) => {
            let sf = new cc.SpriteFrame();
            sf.setTexture(texture);
            this.avatar.spriteFrame = sf;
        });

        f.UI.SetTxt(this.lblName, User.userName);
    }

    AddUIEventListener() {
        f.UI.OnClick(this.btnClose, this.Close.bind(this));
        f.UI.OnClick(this.btnLogout, () => {
            this.Close();
            this.Declare(GameEventType.OnLogout)
        });
        f.UI.OnClick(this.btnPrivacyPolicy, this.OnClickPrivacyPolicy, this, { zoom: 1 });
        f.UI.OnClick(this.btnServiceProtocol, this.OnClickServiceProtocol, this, { zoom: 1 });
    }

    OnClickPrivacyPolicy() {
        f.UI.OpenDialog(GDefs.GamePage.DialogPrivacyPolicy);
    }

    OnClickServiceProtocol() {
        f.UI.OpenDialog(GDefs.GamePage.DialogServiceProtocol);
    }


    // update (dt) {}
}
