// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ListItemBase from "../../Framework/View/ListItemBase";
import Net from "../../Network/Net";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WithDrawListItem extends ListItemBase {

    @property(cc.Label)
    lbl_tittle: cc.Label = null;
    @property(cc.Label)
    lbl_desc: cc.Label = null;
    @property(cc.Node)
    btn: cc.Node = null;

    start() {
        f.UI.OnClick(this.btn, () => {
            console.log(`with draw`, this._data);
            Net.Withdraw(this._data.id, (res) => {
                console.log('withdraw resp', res);
                f.UI.Tip(res.respMsg);
            });
        })
    }
    UpdateUI(data) {
        super.UpdateUI(data);
        f.UI.SetTxt(this.lbl_tittle, data.option);
        f.UI.SetTxt(this.lbl_desc, data.title);
        // id: number,
        // option: number,
        // title: string,


    }

    // update (dt) {}
}
