// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SkinMgr from "../../Common/SkinMgr";
import ListItemBase from "../../Framework/View/ListItemBase";
import Net from "../../Network/Net";
import { GameEventType } from "../GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SkinListItem extends ListItemBase {

    @property(cc.Sprite)
    sprite: cc.Sprite = null;

    @property(cc.Label)
    lbl: cc.Label = null;

    @property(cc.Node)
    locked: cc.Node = null;

    @property(cc.Node)
    unlock: cc.Node = null;

    @property(cc.Node)
    btnAd: cc.Node = null;

    _ballCode;

    // onLoad () {}

    start() {
        f.UI.OnClick(this.btnAd, this.OnClickAd, this);
    }

    UpdateUI(data) {

        f.UI.SetTxt(this.lbl, data.name);
        let sf = SkinMgr.getSkin(data.code);
        this.sprite.spriteFrame = sf;
        f.NodeMgr.Visible(this.locked, !data.isCan);
        f.NodeMgr.Visible(this.unlock, data.isCan);
        this._ballCode = data.code;
    }
    // update (dt) {}

    OnClickAd() {
        SkinMgr.UnlockSkin(this._ballCode, (resp) => {
            console.log(resp);
            SkinMgr.UpdateLocalData(this._ballCode);
            let event = new cc.Event(GameEventType.OnUpdateSkinList, true);
            this.node.dispatchEvent(event);
        })
    }
}
