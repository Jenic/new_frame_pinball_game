// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import DialogBase from "../../Framework/View/DialogBase";
import { List } from "../../Framework/View/List";
import Net from "../../Network/Net";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WithDraw extends DialogBase {

    @property(List)
    List: List = null;

    @property(cc.Label)
    lbl_money: cc.Label = null;
    // LIFE-CYCLE CALLBACKS:

    UpdateUI() {
        Net.WithdrawSetting((err, res) => {
            console.log(res);
            this.List.SetData(res.data);
        })
    }
    // update (dt) {}
}
