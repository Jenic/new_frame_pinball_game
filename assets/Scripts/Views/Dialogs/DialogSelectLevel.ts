// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LevelMgr from "../../Common/LevelMgr";
import DialogBase from "../../Framework/View/DialogBase";
import { List } from "../../Framework/View/List";
import { GameEventType } from "../GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class DialogSelectLevel extends DialogBase {

    @property(List)
    List: List = null;
    @property(cc.Node)
    btnReturn: cc.Node = null;

    AddUIEventListener() {
        f.UI.OnClick(this.btnReturn, () => {
            this.Declare(GameEventType.GameResume);
            this.Close();
        })
    }

    AddMessageListeners() {
        this.NodeOn(GameEventType.OnClickSelectLevel, this.OnClickSelectLevel, this);
    }
    UpdateUI() {
        let last = LevelMgr.getLastLevelIndex();
        let unlock = last + 1;
        // let unlock = LevelMgr.getLastLevelIndex();
        let max = LevelMgr._levelList.length;
        let LevelData = [];

        for (let i = 0; i < max; i++) {
            const el = i + 1;
            if (el <= unlock) {
                LevelData.push(el);
            } else {
                LevelData.push(0);
            }
        }

        this.List.SetData(LevelData);
    }

    OnClickSelectLevel(e: cc.Event.EventCustom) {
        let node = e.target;
        e.stopPropagationImmediate();
        let levelNum = e.detail;
        this.Declare(GameEventType.OnClickSelectLevel, levelNum);
    }
    // update (dt) {}
}
