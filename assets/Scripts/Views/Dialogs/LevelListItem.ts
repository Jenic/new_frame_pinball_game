// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LevelMgr from "../../Common/LevelMgr";
import ListItemBase from "../../Framework/View/ListItemBase";
import { GameEventType } from "../GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LevelListItem extends ListItemBase {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.Node)
    locked: cc.Node = null;
    @property(cc.Node)
    unlock: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    start() {
        f.UI.OnClick(this.node, () => {
            let event = new cc.Event.EventCustom(GameEventType.OnClickSelectLevel, true);
            event.setUserData(this._data);
            this.node.dispatchEvent(event);
        }, this, { zoom: 1 });
    }

    UpdateUI(data: any) {
        super.UpdateUI(data);

        this.locked.active = !data;
        this.unlock.active = !!data;

        let levelTxt = data < 10 ? '0' + data : data;
        this.label.string = `关卡-${levelTxt}`;
        console.log("UpdateUI ,", data);

        let selected = data === LevelMgr.GetCurLevelIndex() + 1;
        let toggle = this.node.getComponent(cc.Toggle);
        this.schedule(() => {
            toggle.isChecked = selected;
        })
    }

    // update (dt) {}
}
