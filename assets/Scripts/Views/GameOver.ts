import AdMgr, { AdSlot } from "../Common/AdMgr";
import AudioMgr from "../Common/AudioMgr";
import { GDefs } from "../Defineds/GDefs";
import DialogBase from "../Framework/View/DialogBase";
import { GameEventType } from "./GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameOver extends DialogBase {

    @property(cc.Node)
    bgFailed: cc.Node = null;

    @property(cc.Node)
    bgSuccess: cc.Node = null;

    @property(cc.Node)
    btnNext: cc.Node = null;

    @property(cc.Node)
    btnReplay: cc.Node = null;


    @property(cc.Label)
    lbl_again: cc.Label = null;
    @property(cc.Label)
    lbl_next: cc.Label = null;
    // LIFE-CYCLE CALLBACKS:

    _tweener: any;
    UpdateUI() {
        let param: GDefs.DialogParams.GameOver = this.GetPageParam();
        let result = param.result;

        let tittle = result ? `恭喜过关` : `游戏失败`;
        if (result) {
            AudioMgr.PlayGameSuccessEffect();
        } else {
            AudioMgr.PlayGameFailureEffect();
        }
        f.NodeMgr.Visible(this.bgSuccess, result);
        f.NodeMgr.Visible(this.bgFailed, !result);
        f.NodeMgr.Visible(this.btnNext, result);
        f.NodeMgr.Visible(this.btnReplay, !result);
        console.log('param in GameOver Page', param);


        this.tweenClose();
    }

    _coolDown: number;
    tweenClose() {
        if (this._tweener) {
            this._tweener.stop();
        }

        this._tweener = cc.tween({ a: 1 })
            .to(3, { a: 3 }, {
                progress: (a, b, c, ratio) => {
                    let left = Math.ceil(3 - 3 * ratio);
                    this._coolDown = left;
                    let str1 = '下一关';
                    let str2 = '再玩一次'
                    if (left > 0) {
                        str1 += `(${left}s)`
                        str2 += `(${left}s)`
                    }
                    f.UI.SetTxt(this.lbl_again, str2);
                    f.UI.SetTxt(this.lbl_next, str1);
                }
            })
            .call(() => {
                this._coolDown = 0;
            })
            .start();
    }

    AddUIEventListener() {
        f.UI.OnClick(this.btnNext, () => {
            if (this._coolDown <= 0) {
                f.UI.CloseLocatedView(this);
                this.Declare(GameEventType.OnClickNextLevel);
            }
        }, this);

        f.UI.OnClick(this.btnReplay, () => {
            if (this._coolDown <= 0) {
                f.UI.CloseLocatedView(this);
                this.Declare(GameEventType.OnClickRePlay);
            }
        }, this);

    }



    AddMessageListeners() {
        if (cc.sys.isNative) {
            AdMgr.ShowExpressAd(AdSlot.level_success_express);
        }
    }

    RemoveMessageListeners() {
        if (cc.sys.isNative) {
            AdMgr.CloseExpressAd(AdSlot.level_success_express);
        }
    }


    // update (dt) {}
}
