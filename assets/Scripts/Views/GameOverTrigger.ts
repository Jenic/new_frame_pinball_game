// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameEventType } from "./GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    onBeginContact(collider, self, other) {
        let group = other.node.group;
        if (group === 'box') {
            let e = new cc.Event.EventCustom(GameEventType.GameOver, true);
            e.setUserData(false);
            this.node.dispatchEvent(e);
        }
    }
}
