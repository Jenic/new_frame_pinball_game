// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import DialogBase from "../Framework/View/DialogBase";
import { GameEventType } from "./GameEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Pause extends DialogBase {

    @property(cc.Node)
    btnRestart: cc.Node = null;
    @property(cc.Node)
    btnSelectLevel: cc.Node = null;
    @property(cc.Node)
    btnResume: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:
    AddUIEventListener() {
        f.UI.OnClick(this.btnRestart, () => {
            this.Declare(GameEventType.GameRestart);
            this.Close();
        });

        f.UI.OnClick(this.btnSelectLevel, () => {
            this.Declare(GameEventType.SelectLevel);
        });

        f.UI.OnClick(this.btnResume, () => {
            this.Declare(GameEventType.GameResume);
            this.Close();
        });
    }

    // update (dt) {}
}
