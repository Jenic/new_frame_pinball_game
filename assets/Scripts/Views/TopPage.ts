import AdMgr, { AdSlot } from "../Common/AdMgr";
import AudioMgr from "../Common/AudioMgr";
import IChannel from "../Common/channel/IChannel";
import TestChannel from "../Common/channel/TestChannel";
import LevelMgr from "../Common/LevelMgr";
import SDKMgr from "../Common/SDKMgr";
import SkinMgr from "../Common/SkinMgr";
import User from "../Common/User";
import { GDefs } from "../Defineds/GDefs";
import ViewBase from "../Framework/View/ViewBase";
import Net from "../Network/Net";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TopPage extends ViewBase {
    @property(cc.Node)
    wx_login: cc.Node = null;
    @property(cc.Node)
    normal_login: cc.Node = null;

    @property(cc.Node)
    btnStart: cc.Node = null;

    @property(cc.Node)
    btnTest: cc.Node = null;

    @property(cc.Node)
    btnTest2: cc.Node = null;

    @property(cc.Node)
    btnServiceProtocol: cc.Node = null;
    @property(cc.Node)
    btnPrivacyPolicy: cc.Node = null;
    @property(cc.Node)
    btnServiceProtocol2: cc.Node = null;
    @property(cc.Node)
    btnPrivacyPolicy2: cc.Node = null;

    @property(cc.Toggle)
    agreeToggle: cc.Toggle = null;
    @property(cc.Toggle)
    agreeToggle2: cc.Toggle = null;

    @property(cc.EditBox)
    accEdit: cc.EditBox = null;
    @property(cc.EditBox)
    pswdEdit: cc.EditBox = null;
    @property(cc.Node)
    btn_traLogin: cc.Node = null;

    isLogin: boolean = false;
    onLoad() {
        super.onLoad();
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
        cc.director.getCollisionManager().enabledDrawBoundingBox = true;


        SDKMgr.Init();
        // f.UI.Confirm({
        //     txt: 'adasd',
        //     okTxt: 'bu ok',
        //     cancelTxt: 'cancel ?',
        //     okcb: () => {
        //         console.log('on click confirm ok');
        //     },
        //     cancelcb: () => {
        //         console.log('on click confirm ok');
        //     }
        // })
    }

    AddMessageListeners() {
        AudioMgr.PlayBGM();
    }

    RemoveMessageListeners() {
        AudioMgr.StopMusic();
    }

    AddUIEventListener() {
        f.UI.OnClick(this.btnPrivacyPolicy, this.OnClickPrivacyPolicy, this, { zoom: 1 });
        f.UI.OnClick(this.btnServiceProtocol, this.OnClickServiceProtocol, this, { zoom: 1 });
        f.UI.OnClick(this.btnPrivacyPolicy2, this.OnClickPrivacyPolicy, this, { zoom: 1 });
        f.UI.OnClick(this.btnServiceProtocol2, this.OnClickServiceProtocol, this, { zoom: 1 });
    }

    OnClickPrivacyPolicy() {
        f.UI.OpenDialog(GDefs.GamePage.DialogPrivacyPolicy);
    }

    OnClickServiceProtocol() {
        f.UI.OpenDialog(GDefs.GamePage.DialogServiceProtocol);
    }

    start() {
        let isSupportWX = false;
        if (cc.sys.isNative) {
            isSupportWX = jsb.reflection.callStaticMethod('org/cocos2dx/javascript/AppActivity', "isWeChatAppInstalled", "()Z");
        }
        this.wx_login.active = isSupportWX;
        this.normal_login.active = !isSupportWX;

        let channel: IChannel = new TestChannel();
        f.UI.OnClick(this.btnStart, () => {
            if (this.isLogin) {
                return;
            }
            // 微信登录
            if (this.agreeToggle.isChecked) {
                this.isLogin = true;
                let blockWait = new f.Wait(() => {
                    // TODO 取消遮挡
                })
                // TODO 开始遮挡
                channel.Login(() => {
                    let wait = new f.Wait(() => {
                        f.UI.OpenMainPage(GDefs.GamePage.Game);
                        this.isLogin = false;
                        this.Close();
                    })

                    blockWait.WaitFor(['skin', 'level', 'userInfo']);
                    wait.WaitFor(['skin', 'level', 'userInfo']);


                    User.UpDateUserInfo((res) => {
                        blockWait.Ready('userInfo');
                        res && wait.Ready('userInfo');
                    });
                    LevelMgr.UpdateRoundInfo((res) => {
                        blockWait.Ready('level');
                        res && wait.Ready('level');
                    });
                    SkinMgr.UpdateSkinInfo((res) => {
                        blockWait.Ready('skin');
                        res && wait.Ready('skin');
                    })
                });


            } else {
                f.UI.Tip('请同意服务协议和隐私政策!');
            }
        })

        f.UI.OnClick(this.btn_traLogin, () => {
            if (this.isLogin) {
                return;
            }
            // 微信登录
            if (this.agreeToggle2.isChecked) {
                this.isLogin = true;
                let blockWait = new f.Wait(() => {
                    // TODO 取消遮挡
                })
                let acc = +this.accEdit.string;
                let pswd = this.accEdit.string;
                // TODO 开始遮挡
                channel.normalLogin(acc, pswd, (err, res) => {
                    if (err) {
                        f.UI.Tip(err.respMsg);
                        return;
                    }
                    let wait = new f.Wait(() => {
                        f.UI.OpenMainPage(GDefs.GamePage.Game);
                        this.isLogin = false;
                        this.Close();
                    })

                    blockWait.WaitFor(['skin', 'level', 'userInfo']);
                    wait.WaitFor(['skin', 'level', 'userInfo']);

                    User.UpDateUserInfo((res) => {
                        blockWait.Ready('userInfo');
                        res && wait.Ready('userInfo');
                    });
                    LevelMgr.UpdateRoundInfo((res) => {
                        blockWait.Ready('level');
                        res && wait.Ready('level');
                    });
                    SkinMgr.UpdateSkinInfo((res) => {
                        blockWait.Ready('skin');
                        res && wait.Ready('skin');
                    })
                });


            } else {
                f.UI.Tip('请同意服务协议和隐私政策!');
            }
        })
    }

    // update (dt) {}
}
