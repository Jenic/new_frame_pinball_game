import Http from "./Http";

export enum API {
    say = '/v1/say',
    version = '/cube/api/app/version',
    login = '/cube/api/account/login',
    wxlogin = '/cube/api/account/wxlogin',
    tuiclient = '/cube/api/account/tuiclient',
    logout = '/cube/api/account/logout',
    refresh = '/cube/api/account/refresh',
    logoutaccount = '/cube/api/account/logoutaccount',
    accountInfo = '/cube/api/account/info',
    adInfo = '/cube/api/ad/info',
    appVersion = '/cube/api/app/version',
    appInfo = '/cube/api/app/info',
    smscode = '/cube/api/common/smscode',

    round = '/cube/api/game/round',
    roundFirst = '/cube/api/game/roundfirst',

    ballList = '/cube/api/game/ball',
    unlockBall = '/cube/api/game/unlockball',
    openRedBag = '/cube/api/game/hitredbag',
    hitBox = '/cube/api/game/hitelement',

    withdraw = '/cube/api/game/withdraw',
    withdrawSetting = '/cube/api/game/withdrawsetting',
}

const appId = '200660481181093888';

// 198775721987018752:预上线包
// 371309565989490688:oppo
// 371309616547631104:vivo
// 371309659782516736:华为
// 371309710042861568:小米
// 371309751461613568:taptap
// 371309792154750976:好游快爆
// 371309829232398336:4399
// 371309874191142912:虫虫游戏

const channelId = '371309874191142912';
const version = '1.0.0';
const signKey = '507926345d74a5c638badd29ab4126a7';
const timestamp = new Date().getTime().toString().substr(0, 10);

export default class NetMgr {
    public static Login(data: LoginParameter, cb: (err: any, resp: LoginResponse) => void) {
        this.Post(API.login, data, cb);
    }

    public static WXLogin(data: wxLoginParameter, cb: (err: any, resp: LoginResponse) => void) {
        this.Post(API.wxlogin, data, cb);
    }

    public static TuiClient(data: TuiClientParameter, cb: (err: any, resp: Response) => void) {
        this.Post(API.tuiclient, data, cb);
    }

    public static Logout(data: AccessToken, cb: (err: any, resp: Response) => void) {
        this.Post(API.logout, data, cb);
    }

    public static Refresh(data: AccessToken, cb: (err: any, resp: LoginResponse) => void) {
        this.Post(API.refresh, data, cb);
    }

    public static LogoutAccount(data: any, cb: (err: any, resp: Response) => void) {
        this.Post(API.logoutaccount, data, cb);
    }

    public static AccountInfo(cb: (err: any, resp: AccountInfoResponse) => void) {
        this.Post(API.accountInfo, {}, cb, true);
    }

    public static AdInfo(data: AdInfoParameter, cb: (err: any, resp: AdInfo) => void) {
        this.Post(API.adInfo, data, cb);
    }

    public static AppVersion(data: any, cb: (err: any, resp: AppVersion) => void) {
        this.Post(API.appVersion, data, cb);
    }

    public static AppInfo(data: AppInfoParameter, cb: (err: any, resp: AppInfo) => void) {
        this.Post(API.appInfo, data, cb);
    }

    public static SmsCode(data: SmsCodeParameter, cb: (err: any, resp: Response) => void) {
        this.Post(API.smscode, data, cb);
    }

    public static RoundList(cb: (err: any, resp: RoundListInfo) => void) {
        this.Post(API.round, {}, cb, true);
    }

    public static RoundInfo(id: string, cb: (err: any, resp: RoundInfo) => void) {
        this.Post(API.roundFirst, { id }, cb, true);
    }

    public static BallList(cb: (err: any, resp: BallListInfo) => void) {
        this.Post(API.ballList, {}, cb, true);
    }

    public static UnlockBallSkin(code, cb: (err: any, resp: Response) => void) {
        this.Post(API.unlockBall, { code }, cb, true);
    }

    public static HitBox(data: RedBagParameter, cb: (err: any, resp: Response) => void) {
        this.Post(API.hitBox, data, cb, true);
    }

    public static OpenRedBag(data: RedBagParameter, cb: (err: any, resp: Response) => void) {
        this.Post(API.openRedBag, data, cb, true);
    }

    public static WithdrawSetting(cb: (err: any, resp: WithDrawSetting) => void) {
        this.Post(API.withdrawSetting, {}, cb, true);
    }


    public static Withdraw(id: string, cb: (err: any, resp: any) => void) {
        this.Post(API.withdraw, { id }, cb, true);
    }

    static Post(api: API, data: any, cb: (err: any, resp: Response) => void, needSign: boolean = false) {
        let request: Request = this.Sign(data);
        if (needSign) {
            request = {
                ...request,
                Authorization: 'bearer ' + this.GetAccessToken(),
            }
        }
        Http.Post(api, data, cb, request);
    }

    static Sign(data): Request {
        var strSign = `appId=${appId}&channelId=${channelId}&version=${version}&timestamp=${timestamp}&key=${signKey}`
        strSign = "json=" + JSON.stringify(data) + "&" + strSign;
        let sign = f.Util.MD5(strSign);
        return {
            appId,
            channelId,
            version,
            timestamp,
            sign
        }
    }

    static GetAccessToken(): string {
        return f.Res.GetStorage('accessToken');
    }
}

interface Request {
    appId: string,
    channelId: string,
    timestamp?: string,
    version?: string,
    sign?: string,
    Authorization?: string,
}

interface Response {
    respCode: string,
    respMsg: string,
}


interface LoginParameter {
    mobile: string,
    code: string
}

interface wxLoginParameter {
    code: string
}

interface TuiClientParameter {
    clientId: string
}

interface AccessToken {
    accessToken: string
}

interface AdInfoParameter {
    code: string
}

interface AppInfoParameter {
    code: string
}

interface SmsCodeParameter {
    mobile: string,
    type: string
}

interface RedBagParameter {
    id: string,
    items: {
        elementType: number,
        number: number,
    }[]
}
export interface LoginResponse extends Response {
    data: {
        accessToken: string,
        expires: string,
        userId: number,
        name: string,
        avatar: string
    }
}

export interface AccountInfoResponse extends Response {
    data: {
        userId: number,
        name: string,
        avatar: string,
        messages: number,
        /**
         * 余额
         */
        balance: number,
        /**
         * 以提现
         */
        settled: number,
    }
}

interface AdInfo extends Response {
    data: Array<{
        id: number,
        codeNo: string,
        plat: number,
        type: string,
        material: string,
        css: string,
        url: string
    }>
}

interface AppVersion extends Response {
    data: {
        versionNo: string,
        content: string,
        url: string,
        enforce: boolean,
        number: number,
        online:boolean,

        resourceNo: number,
        resourceUrl: string


    }
}

interface AppInfo extends Response {
    data: {
        name: string,
        content: string
    }
}

interface RoundListInfo extends Response {
    data: {
        id: number,
        name: string,
        pass: boolean,
    }[]
}

interface RoundInfo extends Response {
    data: {
        adEnd: number,
        adStart: number,
        addBalls: number,
        balls: number,
        low: number,
        items: {
            id: number,
            elementType: number,
            number: number,
            amount: number,
        }[]
    }
}

interface BallListInfo extends Response {
    data: {
        code: number,
        name: string,
        isCan: boolean,
    }[]
}

interface WithDrawSetting extends Response {
    data: {
        id: number,
        option: number,
        title: string,
    }[]
}