


export default class Http {

    // static appID = '345212906708799488';
    // static channelId = '345221073190653952';
    // cube.maoobei.com
    static url: string = 'https://cube.maoobei.com';
    // static url: string = 'http://39.108.95.110:50033';

    // public static RequestPostTest(json) {
    //     let xhr = cc.loader.getXMLHttpRequest();
    //     xhr.ontimeout = function () {
    //         console.log("请求超时，请稍后重试！");
    //     }
    //     xhr.onreadystatechange = function () {
    //         console.log(xhr);
    //         if (xhr.readyState === 4) {
    //             if (xhr.status >= 200 && xhr.status < 400) {
    //                 let respone = xhr.responseText;
    //                 let resp = JSON.parse(respone);
    //                 console.log('Http Response:', resp);
    //             }
    //         }
    //     }
    //     // var url = cc.js.formatStr("%s?accessToken=%s", gf.Const.CREATE_ROLE_URL, access_token);
    //     // xhr.open('POST', `${this.host}/`, true);
    //     xhr.open('Post', `${this.url}/cube.api.common/smscode`, true);

    //     xhr.setRequestHeader("Content-Type", "application/json");

    //     1626745763312
    //     1626083883
    //     let timeStamp = Math.floor(new Date().getTime() / 1000);
    //     xhr.setRequestHeader("appId", this.appID);
    //     xhr.setRequestHeader("channelId", this.channelId);
    //     xhr.setRequestHeader("timestamp", String(timeStamp));
    //     xhr.setRequestHeader("version", "1.0.0");
    //     xhr.setRequestHeader("sign", f.Util.MD5(JSON.stringify(json)));
    //     // xhr.setRequestHeader("Token", access_token);
    //     xhr.timeout = 8000;

    //     xhr.send(JSON.stringify(json));
    // }

    static Get(url, callback) {
        var xhr = cc.loader.getXMLHttpRequest();//new XMLHttpRequest();//
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                var respone = xhr.responseText;
                callback(respone);
            } else {

            }
        };
        xhr.open("GET", url, true);
        if (cc.sys.isNative) {
            xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
        }
        // note: In Internet Explorer, the timeout property may be set only after calling the open()
        // method and before calling the send() method.
        xhr.timeout = 5000;// 5 seconds for timeout
        xhr.send();
    }

    static Post(api, data, callback, headers) {
        var xhr = cc.loader.getXMLHttpRequest();//new XMLHttpRequest();//
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                var respone = xhr.responseText;
                let json = JSON.parse(respone)
                if (json.respCode === "00") {
                    callback(null, json);
                } else if (json.respCode === "22") {
                    console.log('登录超时');
                    callback(json, null);
                } else {
                    console.error(`请求失败 错误码: ${json.respCode},${json.respMsg}`);
                    callback(json, null);
                }
            }
        };
        xhr.open("POST", this.url + api, true);
        if (cc.sys.isNative) {
            xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
        }
        xhr.setRequestHeader("Content-Type", "application/json");
        for (const i in headers) {
            if (Object.prototype.hasOwnProperty.call(headers, i)) {
                const el = headers[i];
                xhr.setRequestHeader(i, String(el));
            }
        }
        // note: In Internet Explorer, the timeout property may be set only after calling the open()
        // method and before calling the send() method.
        xhr.timeout = 5000;// 5 seconds for timeout
        xhr.send(JSON.stringify(data));
    }

}

