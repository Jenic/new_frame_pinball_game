import platform from "../platform";

const JAVA_CLASS_NAME = "org/cocos2dx/javascript/WXSDK";
export default class WXAndroidJS implements platform {


    setJsListener(str) {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "SetJsListener", "(Ljava/lang/String;)V", str);
    }

    Init(AppId, AppSecret) {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "Register", "(Ljava/lang/String;Ljava/lang/String;)V", AppId, AppSecret);
    }


    GetWxAuthCode() {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "GetWxAuthCode", "()V");
    }

    reqAuth() {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "ReqAuth", "()V");
    }
}