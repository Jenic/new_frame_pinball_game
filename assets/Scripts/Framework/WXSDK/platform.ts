export default interface IPlatform {
    Init(appId: string, appSecret: string): void;
    setJsListener(listener: string): void;
    GetWxAuthCode():void;
}