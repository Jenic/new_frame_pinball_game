import WXIOSJS from "./IOS/WXIOS";
import WXAndroidJS from "./Android/WXAndroid";
import platform from "./platform";

var initPlatformBridge = function (): platform {
    if (cc.sys.os === cc.sys.OS_IOS) {
        return new WXIOSJS();
    } else if (cc.sys.os === cc.sys.OS_ANDROID) {
        return new WXAndroidJS();
    }
    return new WXIOSJS();
};

var platformBridge: platform = initPlatformBridge();

var WXJSSDK = WXJSSDK || {
    // ========== JS Call Native =========

    // 初始化SDK
    InitSDK: function (appId, appKey) {
        platformBridge.Init(appId, appKey);
    },
    // 原生回调函数的逻辑执行主体
    listener: null,
    setListener: function (obj) {
        this.listener = obj;
        if (platformBridge) {
            platformBridge.setJsListener('WXJSSDK');
        } else {
            cc.log("You must run on Android or iOS.");
        }
    },


    // ========== JS Call Native =========
    // 获取微信code
    GetWxAuthCode() {
        platformBridge.GetWxAuthCode();
    },

    // TODO: 以上所有方法,都需要在platformBridge里实现

    // ========== Native Call JS ==========
    // 获取微信code回调
    OnWxAuthCode(str) {
        if (this.listener && this.listener.bridgeTest) {
            this.listener.OnWxAuthCode(str);
        }
    },
    OnLoginSuccess: function () {

    },

    OnLoginResp(str) {
        if (this.listener && this.listener.bridgeTest) {
            this.listener.OnLoginResp(str);
        }
    },

    bridgeTest(str) {
        console.log(`bridgeTest:`, str);
        if (this.listener && this.listener.bridgeTest) {
            this.listener.bridgeTest(str);
        }
    },

    bridgeTempStrTest() {
        console.log(`bridgeTest: bridgeTempStrTest`);

    }
};
window['WXJSSDK'] = WXJSSDK;