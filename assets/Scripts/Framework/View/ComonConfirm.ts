// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ViewBase from "./ViewBase";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ComonConfirm extends ViewBase {

    @property(cc.Label)
    desc: cc.Label = null;

    @property(cc.Node)
    btnOk: cc.Node = null;
    @property(cc.Label)
    lblOk: cc.Label = null;

    @property(cc.Node)
    btnCancel: cc.Node = null;
    @property(cc.Label)
    lblCancel: cc.Label = null;

    _okCallback: () => void;
    _cancelCallback: () => void;

    onEnable() {
        super.onEnable();
        let param: f.UI.ConfirmDialogInfo = this.GetPageParam();
        let txt = param.txt || '无';
        let okTxt = param.okTxt || '是';
        let cancelTxt = param.cancelTxt || '返回';
        this._okCallback = param.okcb || (() => { });
        this._cancelCallback = param.cancelcb || (() => { });

        f.UI.SetTxt(this.desc, txt);
        f.UI.SetTxt(this.lblOk, okTxt);
        f.UI.SetTxt(this.lblCancel, cancelTxt);
        f.NodeMgr.Visible(this.btnOk, !!param.okTxt || !!param.okcb);
        f.NodeMgr.Visible(this.btnCancel, !!param.cancelTxt || !!param.cancelcb);

        f.UI.OnClick(this.btnOk, () => {
            this._okCallback();
            this.Close();
        })
        f.UI.OnClick(this.btnCancel, () => {
            this._cancelCallback();
            this.Close();
        })
    }

    // update (dt) {}
}
