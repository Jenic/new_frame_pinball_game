
const { ccclass, property, menu } = cc._decorator;


interface CallbackInfo {
    key: string,
    callback: () => void,
    target: any
}
/**
* onLoad 方法重写必须继承
*/
@ccclass
@menu('View/Base')
export default class ViewBase extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    _eventTarget: f.Event = null;
    _holdEventTarget: f.Event = null;

    _targetCallbakInfos: Array<CallbackInfo>;
    _nodeCallbakInfos: Array<CallbackInfo>;
    @property
    autoFixedSize: boolean = true;

    onLoad() {
        this.node.x = 0;
        this.node.y = 0;
        this._eventTarget = new f.Event();
        this._targetCallbakInfos = new Array<CallbackInfo>();
        this._nodeCallbakInfos = new Array<CallbackInfo>();

        // 该节点被利用时触发
        this.node.on(f.EventType.ViewBaseAddListener, this._AddMessageListeners, this);
        // 该节点被回收时触发
        this.node.on(f.EventType.ViewBaseRemoveListener, this._RemoveMessageListeners, this);
        this.node.on(f.EventType.ViewBaseSetEventTarget, this.SetEventTargetListeners, this);
        // 回收时,UI不可见,UI上的绑定时间,不需要取消监听
        this.AddUIEventListener();
    }

    /**
     * 模块UI消息注册,自动调用
     */
    AddUIEventListener() {

    }

    private _AddMessageListeners() {
        this.NodeOn(f.EventType.ClickBtnClose, this.OnClickBtnClose, this);
        this.AddMessageListeners();
    }
    /**
     * 移除监听事件
     */
    private _RemoveMessageListeners() {
        for (let i = 0; i < this._targetCallbakInfos.length; i++) {
            const el = this._targetCallbakInfos[i];
            this.Off(el.key, el.callback, el.target);
        }
        this._targetCallbakInfos = new Array<CallbackInfo>();

        for (let i = 0; i < this._nodeCallbakInfos.length; i++) {
            const el = this._nodeCallbakInfos[i];
            this.node.off(el.key, el.callback, el.target);
        }
        this._nodeCallbakInfos = new Array<CallbackInfo>();
        this.RemoveMessageListeners();
    }

    // for override
    /**
     * 模块内消息注册,自动调用
     */
    AddMessageListeners() { }
    /**
     * 模块内消息取消注册,自动调用
     * 执行页面回收时的逻辑 无需手动取消注册事件
     */
    RemoveMessageListeners() { }
    /**
     * 更新UI,首次加载自动调用
     */
    UpdateUI() { }

    SetEventTargetListeners(event: f.Event) {
        this._holdEventTarget = event;
    }

    /**
     * 节点激活时调用;
     * @param notify 若使用此方法,需调用 super.onEnable();
     */
    onEnable() {
        if (this.autoFixedSize) {
            let size = cc.visibleRect;
            this.node.width = size.width;
            this.node.height = size.height;
        }
        this.UpdateUI();
    }

    //! 监听到点击关闭事件
    OnClickBtnClose(event: cc.Event) {
        event.stopPropagationImmediate();
        this.Close();
    }
    /**
     * 注册事件
     * Once方法注册过的事件，不能重复注册
     * @param key 事件类型
     * @param cb 事件回调
     * @param caller 作用域
     */
    On(key: string, cb: f.Event.EventHandler, caller?: any, once?: boolean) {
        this._targetCallbakInfos.push({
            key,
            callback: cb,
            target: caller,
        })
        this._eventTarget.On(key, cb, caller, once);
    }

    /**
     * 注册引擎事件
     * @param key 事件类型
     * @param cb 事件回调
     * @param caller 作用域
     */
    NodeOn(key: string, cb: f.Event.EventHandler, caller?: any) {
        this._nodeCallbakInfos.push({
            key,
            callback: cb,
            target: caller,
        })
        this.node.on(key, cb, caller);
    }

    /**
     * 注册一次性事件
     * On方法注册过的事件，不能重复注册
     * @param key 事件类型
     * @param cb 事件回调
     * @param caller 作用域
     */
    Once(key: string, cb: f.Event.EventHandler, caller?: any) {
        this._eventTarget.On(key, cb, caller);
    }

    /**
     * 发送事件
     * @param key 事件类型
     * @param data 数据
     */
    Emit(key: string, data?) {
        this._eventTarget.Emit(key, data);
    }
    /**
     * 取消已注册的事件
     * @param key 事件类型
     * @param cb 事件回调
     * @param caller 作用域
     */
    Off(key: string, cb: f.Event.EventHandler, caller?: any) {
        this._eventTarget.Off(key, cb, caller);
    }
    /**
     * 对外发送事件
     * @param key 事件类型
     * @param data 参数
     */
    Declare(key: string, data?: any) {
        this._holdEventTarget.Emit(key, data);
    }

    /**
     * 打开对话框，会把本页面的事件对象，传入该对话框
     * @param name 页面名称
     * @param bundleName bundle包名
     */
    OpenDialog(name: string | f.Res.LoadPageInfo, param?: any) {
        let pageInfo: f.Res.LoadPageInfo;
        if (typeof name === 'string') {
            pageInfo = { name }
        } else {
            pageInfo = name
        }
        f.UI.OpenDialog(pageInfo, param, this._eventTarget);
    }

    /**
     * 按顺序打开对话框，
     * 会等待未关闭的对话框关闭
     * 会把本页面的事件对象，传入该对话框
     * @param name 页面名称
     * @param bundleName bundle包名
     */
    OpenDialogInline(name: string | f.Res.LoadPageInfo, param?: any) {
        let pageInfo: f.Res.LoadPageInfo;
        if (typeof name === 'string') {
            pageInfo = { name }
        } else {
            pageInfo = name
        }
        f.UI.OpenDialogInline(pageInfo, param, this._eventTarget);
    }

    GetPageParam() {
        return this.node[f.UI.DIALOG_INFO];
    }

    /**
     * 关闭页面
     * @param notify 若使用次方法,需调用 super.Close();
     */
    Close() {
        //! 冒泡事件，让管理器关闭自己
        let e = new cc.Event.EventCustom(f.EventType.PageClose, true);
        this.node.dispatchEvent(e);
    }
}
