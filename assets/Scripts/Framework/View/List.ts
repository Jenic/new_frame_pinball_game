const { ccclass, property } = cc._decorator;

@ccclass
export class List extends cc.Component {
    content: cc.Node = null;
    @property(cc.Node)
    view: cc.Node = null;

    get checkedItems(): Array<cc.Node> {
        let container = this.content.getComponent(cc.ToggleContainer);
        let arr = container.toggleItems;
        let list = [];
        for (let i = 0; i < arr.length; i++) {
            const el = arr[i];
            if (el.isChecked) {
                list.push(el.node);
            }
        }
        return list;
    }

    onLoad() {
        this.content = this.node;
        let scroll = this.getComponent(cc.ScrollView);
        if (cc.isValid(scroll)) {
            this.content = scroll.content;
        }
        this.node.on(cc.ScrollView.EventType.SCROLLING.toString(), this.OnScroll, this);
    }

    SetData(data: any) {
        List.SetDataToList(this.content, data);
    }



    public static SetDataToList(node: cc.Node, data: Array<any>) {
        let sample = node.children[0];
        if (!cc.isValid(sample)) {
            console.warn('#SetDataToList 父节点至少包含一个子节点');
            return;
        }

        for (let i = 0; i < data.length; i++) {
            const el = data[i];
            let child = node.children[i];
            if (!cc.isValid(child)) {
                child = cc.instantiate(sample);
                child.parent = node;
            }
            child.active = true;
            child.emit(f.EventType.ListItemUpdate, el);
        }
        for (let i = data.length; i < node.children.length; i++) {
            const el = node.children[i];
            el.active = false;
        }
    }


    OnScroll() {
        let view = this.view;
        if (view) {
            for (let i = 0; i < this.content.children.length; i++) {
                const el = this.content.children[i];
                let result = f.Util.CheckIntersection(view, el);
                el.emit(f.EventType.ListItemContentVisible, result)
            }
        }
    }
}