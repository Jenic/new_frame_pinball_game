// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ListItemBase extends cc.Component {


    @property({ type: cc.Node, displayName: "item的内容节点,item节点为frame,content为所有内容的容器,在scrollview组件中会用来判定是否显示!" })
    content: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:
    _data: any;

    onLoad() {
        this.node.on(f.EventType.ListItemUpdate, this.UpdateUI, this);
        this.node.on(f.EventType.ListItemContentVisible, this.OnContentVisible, this);
    }

    UpdateUI(data: any) {
        console.log('list base update')
        this._data = data;
    }

    OnContentVisible(result: boolean) {
        this.content.active = result;
    }

    // update (dt) {}
}
