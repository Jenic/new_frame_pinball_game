import IExpress from "./Interfaces/IExpress";
import Platform from "./Platform";


var platformBridge: IExpress = Platform.getExpress();

var Express = Express || {
    // ========== JS Call Native =========

    listener: null,
    SetListener: function (obj) {
        this.listener = obj;
        if (platformBridge) {
            platformBridge.SetListener('Express');
        } else {
            cc.log("You must run on Android or iOS.");
        }
    },

    ShowExpressAd(sceneId: string) {
        platformBridge.ShowExpress(sceneId);
    },

    LoadExpress(code: string, sceneId: string) {
        platformBridge.LoadExpress(code, sceneId);
    },

    CloseExpressAd(sceneId: string) {
        platformBridge.CloseExpress(sceneId);
    },

    OnExpressLoadFailed(sceneId: string) {
        console.log('Express#OnExpressLoadFailed', sceneId);
        if (this.listener && this.listener.OnExpressLoadFailed) {
            this.listener.OnExpressLoadFailed(sceneId);
        }
    },
    OnExpressClose(sceneId: string) {
        console.log('Express#OnExpressClose', sceneId);
        if (this.listener && this.listener.OnExpressClose) {
            this.listener.OnExpressClose(sceneId);
        }
    }
};
window['Express'] = Express;