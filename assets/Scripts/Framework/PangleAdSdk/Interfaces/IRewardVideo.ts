export interface IRewardVideo {
    SetListener(listener: string): void;
    LoadRewardVideo(slotId: string, sceneId: string): void;
    ShowRwardVideo(sceneId: string): void;
    IsRewardVideoLoaded(sceneId: string): boolean;
}