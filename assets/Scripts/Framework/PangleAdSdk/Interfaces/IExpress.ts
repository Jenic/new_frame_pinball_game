export default interface IExpress {
    SetListener(listener: string): void;
    LoadExpress(code: string, sceneId: string): void;
    ShowExpress(sceneId: string): void;
    CloseExpress(sceneId: string): void;
}