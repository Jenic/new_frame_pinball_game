import AdSdkAndroid from "./Android/AdSdkAndroid";
import ExpressAndroid from "./Android/ExpressAndroid";
import RewardVideoAndroid from "./Android/RewardVideoAndroid";
import { IAdSdk } from "./Interfaces/IAdSdk";
import IExpress from "./Interfaces/IExpress";
import { IRewardVideo } from "./Interfaces/IRewardVideo";

export default class Platform {
    public static getAdSdk(): IAdSdk {
        if (cc.sys.os === cc.sys.OS_ANDROID) {
            return new AdSdkAndroid();
        }
        return new AdSdkAndroid();
    }

    public static getRewardVideo(): IRewardVideo {
        if (cc.sys.os === cc.sys.OS_ANDROID) {
            return new RewardVideoAndroid();
        }

        return new RewardVideoAndroid();
    }

    public static getExpress(): IExpress {
        if (cc.sys.os === cc.sys.OS_ANDROID) {
            return new ExpressAndroid();
        }

        return new ExpressAndroid();
    }
}