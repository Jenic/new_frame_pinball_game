import { IAdSdk } from "./Interfaces/IAdSdk";
import Platform from "./Platform";


var platformBridge: IAdSdk = Platform.getAdSdk();

var AdSdk = AdSdk || {
    // ========== JS Call Native =========

    // 初始化SDK
    InitSDK: function (appId, appName) {
        platformBridge.Init(appId, appName);
    },

};
window['AdSdk'] = AdSdk;