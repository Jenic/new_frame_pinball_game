import { IRewardVideo } from "./Interfaces/IRewardVideo";
import Platform from "./Platform";


var platformBridge: IRewardVideo = Platform.getRewardVideo();

var RewardVideo = RewardVideo || {
    // ========== JS Call Native =========
    listener: null,
    SetListener: function (obj) {
        this.listener = obj;
        if (platformBridge) {
            platformBridge.SetListener('RewardVideo');
        } else {
            cc.log("You must run on Android or iOS.");
        }
    },
    LoadRewardVideo(slotId: string, sceneId: string): void {
        platformBridge.LoadRewardVideo(slotId, sceneId);
    },
    ShowRwardVideo(sceneId): void {
        platformBridge.ShowRwardVideo(sceneId);
    },
    IsRewardVideoLoaded(sceneId: string): boolean {
        return platformBridge.IsRewardVideoLoaded(sceneId);
    },


    // 激励视频
    OnLoadRewardVideoSuccess: function (sncd: string) {
        console.log('RewardVideo#OnLoadRewardVideoSuccess', sncd);
        if (this.listener && this.listener.OnLoadRewardVideoSuccess) {
            this.listener.OnLoadRewardVideoSuccess(sncd);
        }
    },
    OnLoadRewardVideoFailed: function (sncd: string) {
        console.log('RewardVideo#OnLoadRewardVideoFailed', sncd);
        if (this.listener && this.listener.OnLoadRewardVideoFailed) {
            this.listener.OnLoadRewardVideoFailed(sncd);
        }
    },
    OnShowRewardVideoFailed: function (sncd: string) {
        console.log('RewardVideo#OnShowRewardVideoFailed', sncd);
        if (this.listener && this.listener.OnShowRewardVideoFailed) {
            this.listener.OnShowRewardVideoFailed(sncd);
        }
    },
    OnRewardVideoShow: function (sncd: string) {
        console.log('RewardVideo#OnRewardVideoShow', sncd);
        if (this.listener && this.listener.OnRewardVideoShow) {
            this.listener.OnRewardVideoShow(sncd);
        }
    },
    OnRewardVideoClick: function (sncd: string) {
        console.log('RewardVideo#OnRewardVideoClick', sncd);
        if (this.listener && this.listener.OnRewardVideoClick) {
            this.listener.OnRewardVideoClick(sncd);
        }
    },
    OnRewardVideoSkip: function (sncd: string) {
        console.log('RewardVideo#OnRewardVideoSkip', sncd);
        if (this.listener && this.listener.OnRewardVideoSkip) {
            this.listener.OnRewardVideoSkip(sncd);
        }
    },
    OnRewardVideoReward: function (sncd: string) {
        console.log('RewardVideo#OnRewardVideoReward', sncd);
        if (this.listener && this.listener.OnRewardVideoReward) {
            this.listener.OnRewardVideoReward(sncd);
        }
    },
    OnRewardVideoClose: function (sncd: string) {
        console.log('RewardVideo#OnRewardVideoClose', sncd);
        if (this.listener && this.listener.OnRewardVideoClose) {
            this.listener.OnRewardVideoClose(sncd);
        }
    },
};
window['RewardVideo'] = RewardVideo;