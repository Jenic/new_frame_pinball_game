import { IRewardVideo } from "../Interfaces/IRewardVideo";

const JAVA_CLASS_NAME = "org/cocos2dx/javascript/PangleRewardVideo";

export default class RewardVideoAndroid implements IRewardVideo {
    SetListener(listener: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "SetListener", "(Ljava/lang/String;)V", listener);
    }
    LoadRewardVideo(slotId: string, sceneId: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "LoadRewardVideo", "(Ljava/lang/String;Ljava/lang/String;)V", slotId , sceneId);
    }
    ShowRwardVideo(scenId: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "ShowRwardVideo", "(Ljava/lang/String;)V", scenId);
    }
    IsRewardVideoLoaded(sceneId: string): boolean {
        return jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "IsRewardVideoLoaded", "(Ljava/lang/String;)Z", sceneId);
    }

}