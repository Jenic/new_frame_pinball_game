import IExpress from "../Interfaces/IExpress";

const JAVA_CLASS_NAME = "org/cocos2dx/javascript/PangleExpress";

export default class ExpressAndroid implements IExpress {
    SetListener(listener: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "SetListener", "(Ljava/lang/String;)V", listener);
    }
    LoadExpress(code: string, sceneId: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "LoadExpress", "(Ljava/lang/String;Ljava/lang/String;)V", code, sceneId);
    }
    ShowExpress(sceneId: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "ShowExpress", "(Ljava/lang/String;)V", sceneId);
    }
    CloseExpress(sceneId: string): void {
        jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "CloseExpress", "(Ljava/lang/String;)V", sceneId);

    }


}