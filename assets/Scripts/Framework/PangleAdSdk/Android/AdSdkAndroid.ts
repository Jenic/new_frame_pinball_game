import { IAdSdk } from "../Interfaces/IAdSdk";
const JAVA_CLASS_NAME = "org/cocos2dx/javascript/PangleAdManager";
export default class AdSdkAndroid implements IAdSdk {
    Init(appId: string, appName: string): void {
        if (cc.sys.isNative) {
            console.log('Android AdSdk Init', appId);
            jsb.reflection.callStaticMethod(JAVA_CLASS_NAME, "Init", "(Ljava/lang/String;Ljava/lang/String;)V", appId, appName);
        }
    }

}