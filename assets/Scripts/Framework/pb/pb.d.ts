import * as $protobuf from "protobufjs";
export namespace jenic {

    interface ILevelInfo {
        id?: (number|null);
        unlock?: (boolean|null);
        total?: (number|null);
        progress?: (number|null);
    }

    class LevelInfo implements ILevelInfo {
        constructor(properties?: jenic.ILevelInfo);
        public id: number;
        public unlock: boolean;
        public total: number;
        public progress: number;
        public static encode(message: jenic.ILevelInfo, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): jenic.LevelInfo;
    }

    interface ILevelList {
        max?: (number|null);
        list?: (jenic.ILevelInfo[]|null);
    }

    class LevelList implements ILevelList {
        constructor(properties?: jenic.ILevelList);
        public max: number;
        public list: jenic.ILevelInfo[];
        public static encode(message: jenic.ILevelList, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): jenic.LevelList;
    }

    interface IRoleCreateReq {
        serCode?: (string|null);
        roleName?: (string|null);
        gender?: (number|null);
        professionId?: (number|null);
    }

    class RoleCreateReq implements IRoleCreateReq {
        constructor(properties?: jenic.IRoleCreateReq);
        public serCode: string;
        public roleName: string;
        public gender: number;
        public professionId: number;
        public static encode(message: jenic.IRoleCreateReq, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): jenic.RoleCreateReq;
    }

    interface IReadRoleArchiveResp {
        accountId?: (string|null);
        roleId?: (string|null);
        roleName?: (string|null);
        gender?: (number|null);
        professionId?: (number|null);
        vipLevel?: (number|null);
        vipPoint?: (number|Long|null);
    }

    class ReadRoleArchiveResp implements IReadRoleArchiveResp {
        constructor(properties?: jenic.IReadRoleArchiveResp);
        public accountId: string;
        public roleId: string;
        public roleName: string;
        public gender: number;
        public professionId: number;
        public vipLevel: number;
        public vipPoint: (number|Long);
        public static encode(message: jenic.IReadRoleArchiveResp, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): jenic.ReadRoleArchiveResp;
    }

    interface IUser {
        age?: (number|null);
        name?: (string|null);
        gender?: (number|null);
    }

    class User implements IUser {
        constructor(properties?: jenic.IUser);
        public age: number;
        public name: string;
        public gender: number;
        public static encode(message: jenic.IUser, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): jenic.User;
    }
}
