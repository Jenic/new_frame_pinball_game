/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.account = (function() {

    var account = {};

    account.LoginReq = (function() {

        function LoginReq(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        LoginReq.prototype.accountId = "";
        LoginReq.prototype.password = "";
        LoginReq.prototype.simcode = 0;

        LoginReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(10).string(message.accountId);
            if (message.password != null && Object.hasOwnProperty.call(message, "password"))
                writer.uint32(18).string(message.password);
            if (message.simcode != null && Object.hasOwnProperty.call(message, "simcode"))
                writer.uint32(24).int32(message.simcode);
            return writer;
        };

        LoginReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.account.LoginReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.accountId = reader.string();
                    break;
                case 2:
                    message.password = reader.string();
                    break;
                case 3:
                    message.simcode = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("accountId"))
                throw $util.ProtocolError("missing required 'accountId'", { instance: message });
            return message;
        };

        return LoginReq;
    })();

    account.LoginResp = (function() {

        function LoginResp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        LoginResp.prototype.accountId = "";
        LoginResp.prototype.nickname = "";
        LoginResp.prototype.level = 0;

        LoginResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(10).string(message.accountId);
            writer.uint32(18).string(message.nickname);
            writer.uint32(24).int32(message.level);
            return writer;
        };

        LoginResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.account.LoginResp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.accountId = reader.string();
                    break;
                case 2:
                    message.nickname = reader.string();
                    break;
                case 3:
                    message.level = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("accountId"))
                throw $util.ProtocolError("missing required 'accountId'", { instance: message });
            if (!message.hasOwnProperty("nickname"))
                throw $util.ProtocolError("missing required 'nickname'", { instance: message });
            if (!message.hasOwnProperty("level"))
                throw $util.ProtocolError("missing required 'level'", { instance: message });
            return message;
        };

        return LoginResp;
    })();

    account.LoginReq2 = (function() {

        function LoginReq2(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        LoginReq2.prototype.accountId = "";
        LoginReq2.prototype.password = "";
        LoginReq2.prototype.simcode = 0;

        LoginReq2.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(10).string(message.accountId);
            if (message.password != null && Object.hasOwnProperty.call(message, "password"))
                writer.uint32(18).string(message.password);
            if (message.simcode != null && Object.hasOwnProperty.call(message, "simcode"))
                writer.uint32(24).int32(message.simcode);
            return writer;
        };

        LoginReq2.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.account.LoginReq2();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.accountId = reader.string();
                    break;
                case 2:
                    message.password = reader.string();
                    break;
                case 3:
                    message.simcode = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("accountId"))
                throw $util.ProtocolError("missing required 'accountId'", { instance: message });
            return message;
        };

        return LoginReq2;
    })();

    account.LoginResp2 = (function() {

        function LoginResp2(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        LoginResp2.prototype.accountId = "";
        LoginResp2.prototype.nickname = "";
        LoginResp2.prototype.level = 0;

        LoginResp2.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(10).string(message.accountId);
            writer.uint32(18).string(message.nickname);
            writer.uint32(24).int32(message.level);
            return writer;
        };

        LoginResp2.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.account.LoginResp2();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.accountId = reader.string();
                    break;
                case 2:
                    message.nickname = reader.string();
                    break;
                case 3:
                    message.level = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("accountId"))
                throw $util.ProtocolError("missing required 'accountId'", { instance: message });
            if (!message.hasOwnProperty("nickname"))
                throw $util.ProtocolError("missing required 'nickname'", { instance: message });
            if (!message.hasOwnProperty("level"))
                throw $util.ProtocolError("missing required 'level'", { instance: message });
            return message;
        };

        return LoginResp2;
    })();

    return account;
})();

module.exports = $root;
