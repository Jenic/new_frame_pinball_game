import * as $protobuf from "protobufjs";
export namespace account {

    interface ILoginReq {
        accountId: string;
        password?: (string|null);
        simcode?: (number|null);
    }

    class LoginReq implements ILoginReq {
        constructor(properties?: account.ILoginReq);
        public accountId: string;
        public password: string;
        public simcode: number;
        public static encode(message: account.ILoginReq, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): account.LoginReq;
    }

    interface ILoginResp {
        accountId: string;
        nickname: string;
        level: number;
    }

    class LoginResp implements ILoginResp {
        constructor(properties?: account.ILoginResp);
        public accountId: string;
        public nickname: string;
        public level: number;
        public static encode(message: account.ILoginResp, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): account.LoginResp;
    }

    interface ILoginReq2 {
        accountId: string;
        password?: (string|null);
        simcode?: (number|null);
    }

    class LoginReq2 implements ILoginReq2 {
        constructor(properties?: account.ILoginReq2);
        public accountId: string;
        public password: string;
        public simcode: number;
        public static encode(message: account.ILoginReq2, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): account.LoginReq2;
    }

    interface ILoginResp2 {
        accountId: string;
        nickname: string;
        level: number;
    }

    class LoginResp2 implements ILoginResp2 {
        constructor(properties?: account.ILoginResp2);
        public accountId: string;
        public nickname: string;
        public level: number;
        public static encode(message: account.ILoginResp2, writer?: $protobuf.Writer): $protobuf.Writer;
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): account.LoginResp2;
    }
}
