/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.jenic = (function() {

    /**
     * Namespace jenic.
     * @exports jenic
     * @namespace
     */
    var jenic = {};

    jenic.LevelInfo = (function() {

        /**
         * Properties of a LevelInfo.
         * @memberof jenic
         * @interface ILevelInfo
         * @property {number|null} [id] LevelInfo id
         * @property {boolean|null} [unlock] LevelInfo unlock
         * @property {number|null} [total] LevelInfo total
         * @property {number|null} [progress] LevelInfo progress
         */

        /**
         * Constructs a new LevelInfo.
         * @memberof jenic
         * @classdesc Represents a LevelInfo.
         * @implements ILevelInfo
         * @constructor
         * @param {jenic.ILevelInfo=} [properties] Properties to set
         */
        function LevelInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LevelInfo id.
         * @member {number} id
         * @memberof jenic.LevelInfo
         * @instance
         */
        LevelInfo.prototype.id = 0;

        /**
         * LevelInfo unlock.
         * @member {boolean} unlock
         * @memberof jenic.LevelInfo
         * @instance
         */
        LevelInfo.prototype.unlock = false;

        /**
         * LevelInfo total.
         * @member {number} total
         * @memberof jenic.LevelInfo
         * @instance
         */
        LevelInfo.prototype.total = 0;

        /**
         * LevelInfo progress.
         * @member {number} progress
         * @memberof jenic.LevelInfo
         * @instance
         */
        LevelInfo.prototype.progress = 0;

        /**
         * Encodes the specified LevelInfo message. Does not implicitly {@link jenic.LevelInfo.verify|verify} messages.
         * @function encode
         * @memberof jenic.LevelInfo
         * @static
         * @param {jenic.ILevelInfo} message LevelInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LevelInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.id);
            if (message.unlock != null && Object.hasOwnProperty.call(message, "unlock"))
                writer.uint32(/* id 2, wireType 0 =*/16).bool(message.unlock);
            if (message.total != null && Object.hasOwnProperty.call(message, "total"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.total);
            if (message.progress != null && Object.hasOwnProperty.call(message, "progress"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.progress);
            return writer;
        };

        /**
         * Decodes a LevelInfo message from the specified reader or buffer.
         * @function decode
         * @memberof jenic.LevelInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {jenic.LevelInfo} LevelInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LevelInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.jenic.LevelInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.id = reader.int32();
                    break;
                case 2:
                    message.unlock = reader.bool();
                    break;
                case 3:
                    message.total = reader.int32();
                    break;
                case 4:
                    message.progress = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return LevelInfo;
    })();

    jenic.LevelList = (function() {

        /**
         * Properties of a LevelList.
         * @memberof jenic
         * @interface ILevelList
         * @property {number|null} [max] LevelList max
         * @property {Array.<jenic.ILevelInfo>|null} [list] LevelList list
         */

        /**
         * Constructs a new LevelList.
         * @memberof jenic
         * @classdesc Represents a LevelList.
         * @implements ILevelList
         * @constructor
         * @param {jenic.ILevelList=} [properties] Properties to set
         */
        function LevelList(properties) {
            this.list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LevelList max.
         * @member {number} max
         * @memberof jenic.LevelList
         * @instance
         */
        LevelList.prototype.max = 0;

        /**
         * LevelList list.
         * @member {Array.<jenic.ILevelInfo>} list
         * @memberof jenic.LevelList
         * @instance
         */
        LevelList.prototype.list = $util.emptyArray;

        /**
         * Encodes the specified LevelList message. Does not implicitly {@link jenic.LevelList.verify|verify} messages.
         * @function encode
         * @memberof jenic.LevelList
         * @static
         * @param {jenic.ILevelList} message LevelList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LevelList.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.max != null && Object.hasOwnProperty.call(message, "max"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.max);
            if (message.list != null && message.list.length)
                for (var i = 0; i < message.list.length; ++i)
                    $root.jenic.LevelInfo.encode(message.list[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            return writer;
        };

        /**
         * Decodes a LevelList message from the specified reader or buffer.
         * @function decode
         * @memberof jenic.LevelList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {jenic.LevelList} LevelList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LevelList.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.jenic.LevelList();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.max = reader.int32();
                    break;
                case 2:
                    if (!(message.list && message.list.length))
                        message.list = [];
                    message.list.push($root.jenic.LevelInfo.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return LevelList;
    })();

    jenic.RoleCreateReq = (function() {

        /**
         * Properties of a RoleCreateReq.
         * @memberof jenic
         * @interface IRoleCreateReq
         * @property {string|null} [serCode] RoleCreateReq serCode
         * @property {string|null} [roleName] RoleCreateReq roleName
         * @property {number|null} [gender] RoleCreateReq gender
         * @property {number|null} [professionId] RoleCreateReq professionId
         */

        /**
         * Constructs a new RoleCreateReq.
         * @memberof jenic
         * @classdesc Represents a RoleCreateReq.
         * @implements IRoleCreateReq
         * @constructor
         * @param {jenic.IRoleCreateReq=} [properties] Properties to set
         */
        function RoleCreateReq(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RoleCreateReq serCode.
         * @member {string} serCode
         * @memberof jenic.RoleCreateReq
         * @instance
         */
        RoleCreateReq.prototype.serCode = "";

        /**
         * RoleCreateReq roleName.
         * @member {string} roleName
         * @memberof jenic.RoleCreateReq
         * @instance
         */
        RoleCreateReq.prototype.roleName = "";

        /**
         * RoleCreateReq gender.
         * @member {number} gender
         * @memberof jenic.RoleCreateReq
         * @instance
         */
        RoleCreateReq.prototype.gender = 0;

        /**
         * RoleCreateReq professionId.
         * @member {number} professionId
         * @memberof jenic.RoleCreateReq
         * @instance
         */
        RoleCreateReq.prototype.professionId = 0;

        /**
         * Encodes the specified RoleCreateReq message. Does not implicitly {@link jenic.RoleCreateReq.verify|verify} messages.
         * @function encode
         * @memberof jenic.RoleCreateReq
         * @static
         * @param {jenic.IRoleCreateReq} message RoleCreateReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoleCreateReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.serCode != null && Object.hasOwnProperty.call(message, "serCode"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.serCode);
            if (message.roleName != null && Object.hasOwnProperty.call(message, "roleName"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.roleName);
            if (message.gender != null && Object.hasOwnProperty.call(message, "gender"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.gender);
            if (message.professionId != null && Object.hasOwnProperty.call(message, "professionId"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.professionId);
            return writer;
        };

        /**
         * Decodes a RoleCreateReq message from the specified reader or buffer.
         * @function decode
         * @memberof jenic.RoleCreateReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {jenic.RoleCreateReq} RoleCreateReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoleCreateReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.jenic.RoleCreateReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.serCode = reader.string();
                    break;
                case 2:
                    message.roleName = reader.string();
                    break;
                case 3:
                    message.gender = reader.int32();
                    break;
                case 4:
                    message.professionId = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return RoleCreateReq;
    })();

    jenic.ReadRoleArchiveResp = (function() {

        /**
         * Properties of a ReadRoleArchiveResp.
         * @memberof jenic
         * @interface IReadRoleArchiveResp
         * @property {string|null} [accountId] ReadRoleArchiveResp accountId
         * @property {string|null} [roleId] ReadRoleArchiveResp roleId
         * @property {string|null} [roleName] ReadRoleArchiveResp roleName
         * @property {number|null} [gender] ReadRoleArchiveResp gender
         * @property {number|null} [professionId] ReadRoleArchiveResp professionId
         * @property {number|null} [vipLevel] ReadRoleArchiveResp vipLevel
         * @property {number|Long|null} [vipPoint] ReadRoleArchiveResp vipPoint
         */

        /**
         * Constructs a new ReadRoleArchiveResp.
         * @memberof jenic
         * @classdesc Represents a ReadRoleArchiveResp.
         * @implements IReadRoleArchiveResp
         * @constructor
         * @param {jenic.IReadRoleArchiveResp=} [properties] Properties to set
         */
        function ReadRoleArchiveResp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ReadRoleArchiveResp accountId.
         * @member {string} accountId
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.accountId = "";

        /**
         * ReadRoleArchiveResp roleId.
         * @member {string} roleId
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.roleId = "";

        /**
         * ReadRoleArchiveResp roleName.
         * @member {string} roleName
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.roleName = "";

        /**
         * ReadRoleArchiveResp gender.
         * @member {number} gender
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.gender = 0;

        /**
         * ReadRoleArchiveResp professionId.
         * @member {number} professionId
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.professionId = 0;

        /**
         * ReadRoleArchiveResp vipLevel.
         * @member {number} vipLevel
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.vipLevel = 0;

        /**
         * ReadRoleArchiveResp vipPoint.
         * @member {number|Long} vipPoint
         * @memberof jenic.ReadRoleArchiveResp
         * @instance
         */
        ReadRoleArchiveResp.prototype.vipPoint = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Encodes the specified ReadRoleArchiveResp message. Does not implicitly {@link jenic.ReadRoleArchiveResp.verify|verify} messages.
         * @function encode
         * @memberof jenic.ReadRoleArchiveResp
         * @static
         * @param {jenic.IReadRoleArchiveResp} message ReadRoleArchiveResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ReadRoleArchiveResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.accountId != null && Object.hasOwnProperty.call(message, "accountId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.accountId);
            if (message.roleId != null && Object.hasOwnProperty.call(message, "roleId"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.roleId);
            if (message.roleName != null && Object.hasOwnProperty.call(message, "roleName"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.roleName);
            if (message.gender != null && Object.hasOwnProperty.call(message, "gender"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.gender);
            if (message.professionId != null && Object.hasOwnProperty.call(message, "professionId"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.professionId);
            if (message.vipLevel != null && Object.hasOwnProperty.call(message, "vipLevel"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.vipLevel);
            if (message.vipPoint != null && Object.hasOwnProperty.call(message, "vipPoint"))
                writer.uint32(/* id 7, wireType 0 =*/56).int64(message.vipPoint);
            return writer;
        };

        /**
         * Decodes a ReadRoleArchiveResp message from the specified reader or buffer.
         * @function decode
         * @memberof jenic.ReadRoleArchiveResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {jenic.ReadRoleArchiveResp} ReadRoleArchiveResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ReadRoleArchiveResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.jenic.ReadRoleArchiveResp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.accountId = reader.string();
                    break;
                case 2:
                    message.roleId = reader.string();
                    break;
                case 3:
                    message.roleName = reader.string();
                    break;
                case 4:
                    message.gender = reader.int32();
                    break;
                case 5:
                    message.professionId = reader.int32();
                    break;
                case 6:
                    message.vipLevel = reader.int32();
                    break;
                case 7:
                    message.vipPoint = reader.int64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return ReadRoleArchiveResp;
    })();

    jenic.User = (function() {

        /**
         * Properties of a User.
         * @memberof jenic
         * @interface IUser
         * @property {number|null} [age] User age
         * @property {string|null} [name] User name
         * @property {number|null} [gender] User gender
         */

        /**
         * Constructs a new User.
         * @memberof jenic
         * @classdesc Represents a User.
         * @implements IUser
         * @constructor
         * @param {jenic.IUser=} [properties] Properties to set
         */
        function User(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * User age.
         * @member {number} age
         * @memberof jenic.User
         * @instance
         */
        User.prototype.age = 0;

        /**
         * User name.
         * @member {string} name
         * @memberof jenic.User
         * @instance
         */
        User.prototype.name = "";

        /**
         * User gender.
         * @member {number} gender
         * @memberof jenic.User
         * @instance
         */
        User.prototype.gender = 0;

        /**
         * Encodes the specified User message. Does not implicitly {@link jenic.User.verify|verify} messages.
         * @function encode
         * @memberof jenic.User
         * @static
         * @param {jenic.IUser} message User message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        User.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.age != null && Object.hasOwnProperty.call(message, "age"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.age);
            if (message.name != null && Object.hasOwnProperty.call(message, "name"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.name);
            if (message.gender != null && Object.hasOwnProperty.call(message, "gender"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.gender);
            return writer;
        };

        /**
         * Decodes a User message from the specified reader or buffer.
         * @function decode
         * @memberof jenic.User
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {jenic.User} User
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        User.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.jenic.User();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.age = reader.int32();
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                case 3:
                    message.gender = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return User;
    })();

    return jenic;
})();

module.exports = $root;
