/// <reference path="../../../../creator.d.ts" />
declare namespace f {
    export namespace Action {
        interface IRunActionOption {
            target: cc.Node;
            duration?: number;
            x?: number;
            y?: number;
            delay?: number;
            cb?: () => void;
            reset?: boolean;
        }
    }
    export class Action {
        /**
         * 节点渐入
         * @param option 动作参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        static FadeIn(option: Action.IRunActionOption): void;
        /**
         * 节点渐出
         * @param option 动作参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        static FadeOut(option: Action.IRunActionOption): void;
        /**
         * 节点渐出
         * @param option 动作参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         * @param option.x 目标缩放(x和y)
         */
        static ScaleTo(option: Action.IRunActionOption): void;
        static MoveTo(option: Action.IRunActionOption): void;
        /**
         * 节点 移动
         * @param option 动作配置参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        static MoveBy(option: Action.IRunActionOption): void;
        /**
         * 节点 渐显+形变
         * @param option 动作配置参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.x x 位移
         * @param option.y 位移
         * @param option.cb 动作回调,动作执行结束后调用
         * @param shrink 是否是缩小, 默认false:扩大进入
         */
        static ScaleFadeIn(option: Action.IRunActionOption, shrink?: boolean): void;
        /**
         * 节点 渐隐+形变
         * @param option 动作配置参数
         * @param option.target 目标节点
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         * @param enlarge 是否是扩大, 默认false,缩小退出
         */
        static ScaleFadeOut(option: Action.IRunActionOption, enlarge?: boolean): void;
        static RunAction(option: Action.IRunActionOption, func: cc.FiniteTimeAction): void;
    }
    type watiCb = () => void;
    export class Wait {
        _targetHandler: watiCb;
        private _completed;
        private get isAllReady();
        constructor(cb: watiCb);
        static Alloc(cb?: watiCb): Wait;
        WaitFor(str: string | Array<string>): void;
        Ready(str: string): void;
    }
    type endCallback = () => void;
    export class Seq {
        static Alloc(): Seq;
        _sequence: Array<{
            a: (end: endCallback) => void;
            b: any;
        }>;
        _immediately: () => void;
        constructor();
        then(callback: any, context: any, endState?: any): this;
        run(): this;
        resolve(): boolean;
        curFinish(): boolean;
        getnext(): {
            a: (end: endCallback) => void;
            b: any;
        };
        clear(): void;
    }
    export {};
}
declare namespace f {
    class Arr {
        static remove<T>(array: Array<T>, value: T): boolean;
        static fastRemove<T>(array: Array<T>, value: T): void;
        static removeAt<T>(array: Array<T>, index: number): void;
        static fastRemoveAt<T>(array: Array<T>, index: number): void;
        static contains<T>(array: Array<T>, value: T): boolean;
        static verifyType<T>(array: Array<T>, type: any): boolean;
        static copy(array: any): any[];
    }
}
declare namespace f {
    namespace Event {
        interface CallbackInfo {
            target: any;
            handler: EventHandler;
            once: boolean;
        }
        type EventHandler = (...params: any) => void;
    }
    class Event {
        _map: Map<string, Array<Event.CallbackInfo>>;
        On(key: string, cb: Event.EventHandler, caller?: any, once?: boolean): void;
        Once(key: string, cb: Event.EventHandler, caller?: any): void;
        Off(key: string, cb: Event.EventHandler, caller?: any): void;
        Emit(key: string, data?: any): void;
        Clear(): void;
    }
    class GEvent {
        static _e: Event;
        static On(type: string, handler: Event.EventHandler, target?: any, once?: boolean): void;
        static Once(type: string, handler: Event.EventHandler, target?: any): void;
        static Off(type: string, handler: Event.EventHandler, target?: any): void;
        static Emit(type: string, data?: any): void;
        static Clear(): void;
    }
    enum EventType {
        PageClose = "page_close",
        ClickBtnClose = "click_btn_close",
        ListItemUpdate = "list_item_update",
        ListItemContentVisible = "list_item_content_visible",
        ViewBaseAddListener = "view_base_add_listener",
        ViewBaseRemoveListener = "view_base_remove_listener",
        ViewBaseSetEventTarget = "view_base_set_event_target"
    }
}
declare namespace f {
    export namespace Sets {
        class Queue<T> {
            _arr: Array<T>;
            constructor(arr?: Array<T>);
            get(): T;
            set(obj: T): void;
            clear(): void;
            get size(): number;
        }
        class Stack<T> {
            _arr: Array<T>;
            constructor(arr: Array<T>);
            get(): T;
            set(obj: T): void;
            clear(): void;
            get size(): number;
        }
    }
    export class Tween {
        static FPS: number;
        startObj: Object;
        _curObj: Object;
        endObj: Object;
        states: Sets.Queue<TweenState | TweenCallback>;
        _currentState: TweenState | TweenCallback;
        _curTime: number;
        _maxTime: number;
        Timer: any;
        _timeStamp: number;
        constructor(obj: any);
        to(duration: number, obj: any, progress?: (obj: any, ratio: number) => void): this;
        call(callback: () => void): void;
        start(): this;
        stop(): void;
        goOn(): void;
        nextState(): void;
    }
    class TweenState {
        _duration: number;
        _target: any;
        _progress: (obj: any, ratio: number) => void;
        constructor(duration: number, target: any, progress?: (obj: any, ratio: number) => void);
    }
    class TweenCallback {
        _callback: () => void;
        constructor(callback: () => void);
    }
    export {};
}
declare namespace f {
    class Http {
        static Get(url: any, callback: any): void;
        static POST(url: any, data: any, callback: any): void;
    }
}
declare namespace f {
    interface ActionOption {
        duration?: number;
        cb?: () => void;
    }
    export class NodeMgr {
        static AddSingletonChild(parent: cc.Node, name: string): cc.Node;
        static AddSingletonComponent(node: cc.Node, type: any): cc.Component;
        static CreateNodePool(str: string): cc.NodePool;
        static SetPageParam(node: cc.Node, param: any): void;
        static GetPageParam(node: cc.Node): any;
        static Visible(target: cc.Node, active: boolean, option?: ActionOption): void;
    }
    export {};
}
declare namespace f {
    namespace Res {
        interface LoadPageInfo {
            name: string;
            bundleName?: string;
            path?: string;
        }
        interface PreloadInfo {
            comonConfirm: cc.Node | cc.Prefab;
            tip: cc.Node | cc.Prefab;
        }
    }
    class Res {
        static viewCache: Array<cc.Node>;
        static _viewCacheSize: number;
        static dialogCache: Array<cc.Node>;
        static _dialogCacheSize: number;
        static tipChache: Array<cc.Node>;
        static _tipChacheSize: number;
        static defaultDialog: cc.Node | cc.Prefab;
        static DEFAULT_DIALOG_NAME: string;
        static defaultTip: cc.Node | cc.Prefab;
        static DEBUG(): {
            viewCache: cc.Node[];
            dialogCache: cc.Node[];
        };
        static GetTransEff(str: string): cc.Material;
        /**
         * 资源预加载
         * @param prelaodInfo 预加载内容
         * @param prelaodInfo.comonConfirm 默认确认框
         * @param prelaodInfo.tip 默认tips提示
         */
        static Preload(prelaodInfo: Res.PreloadInfo): void;
        static LoadCommonDialog(node: cc.Node | cc.Prefab): void;
        static LoadPage(option: Res.LoadPageInfo, cb: (node: cc.Node) => void): void;
        static LoadDialog(option: Res.LoadPageInfo, cb: (node: cc.Node) => void): void;
        static LoadTip(): cc.Node;
        static LoadBundle(name: string, cb: (bundle: cc.AssetManager.Bundle) => void): void;
        static TakePage(node: cc.Node): void;
        static TakeDialog(node: cc.Node): void;
        static TakeTip(node: cc.Node): void;
        static GetDialogCache(dialogName: string): cc.Node;
        static CacheDialog(node: cc.Node): void;
        static GetViewCache(pageName: string): cc.Node;
        static CacheView(node: cc.Node): void;
        static GetTipCache(): cc.Node;
        static CacheTip(node: any): void;
        static GetStorage(key: string): string;
        static SetStorage(key: string, value: string): void;
        static GetDefaultSprite(): cc.SpriteFrame;
    }
}
declare namespace f {
    class ShaderUtil {
        protected static nodePool: cc.NodePool;
        protected static get cameraNode(): cc.Node;
        protected static cutoff: number;
        protected static radius: number;
        protected static gridOuter: Float64Array;
        protected static gridInner: Float64Array;
        protected static f: Float64Array;
        protected static d: Float64Array;
        protected static z: Float64Array;
        protected static v: Int16Array;
        static nodeName: string;
        static snap(node: cc.Node, width?: number, height?: number): cc.RenderTexture;
        static getPixels(data: cc.RenderTexture): Uint8Array;
        static savePicFile(data: any, width: any, height: any, cb: (err: Error, res: any) => void, filename?: string): void;
        /**
         * 为包装节点 和其所有子节点 附加shader特效
         * @param node 包装节点
         * @param name 特效名称
         * @param option 设置
         */
        /**
         * 取消包装的特效
         * @param node 包装节点
         */
        static unWrap(node: cc.Node): void;
        static RenderSDF(texture: cc.RenderTexture, radius?: number, cutoff?: number): {
            texture: cc.RenderTexture;
            alpha: Uint8ClampedArray;
        };
        protected static EDT(data: any, width: any, height: any, f: any, d: any, v: any, z: any): void;
        protected static EDT1D(f: any, d: any, v: any, z: any, n: any): void;
    }
}
declare namespace f {
    class Str {
        static format(tpl: string, data: any): string;
        static Compress(strNormalString: string): string;
        static Decompress(strCompressedString: string): string;
    }
}
declare namespace f {
    namespace UI {
        interface ConfirmDialogInfo {
            txt?: string;
            okcb?: () => void;
            okTxt?: string;
            cancelcb?: () => void;
            cancelTxt?: string;
        }
        interface OnClickOption {
            zoom?: number;
        }
    }
    class UI {
        static GetLoadPageInfo(name: string | Res.LoadPageInfo): Res.LoadPageInfo;
        static OpenMainPage(name: string | Res.LoadPageInfo): void;
        static OpenPage(name: string | Res.LoadPageInfo): void;
        static OpenDialog(name: string | Res.LoadPageInfo, param?: any, eventTarget?: Event): void;
        private static _inLineWaittingPageInfo;
        private static _inlineLoading;
        static OpenDialogInline(name: string | Res.LoadPageInfo, param?: any, eventTarget?: Event): void;
        static CloseDialog(name: string): void;
        /**
         *
         * @param option
         * @param option.txt 确认框内容
         * @param option.tittle 确认框标题
         * @param option.okcb 确认按钮回调
         * @param option.okTxt 确认按钮文本
         * @param option.cancelcb 取消按钮回调
         * @param option.cancelTxt 取消按钮回调
         */
        static Confirm(option: UI.ConfirmDialogInfo): void;
        static Tip(str: string): void;
        /**
         * 为目标节点添加点击事件
         * @param target 目标节点
         * @param cb 点击回调
         * @param caller 作用域
         * @returns
         */
        static OnClick(target: cc.Node, cb: () => void, caller?: any, option?: UI.OnClickOption): void;
        static SetTxt(target: cc.Node | cc.Label | cc.RichText, str: string | number): void;
        static SetImg(target: cc.Node | cc.Sprite, sf: cc.SpriteFrame): void;
        static FixedBorders(node: cc.Node, borders: cc.Node, borderSize: number): void;
        /**
         * 关闭该节点所附着的页面（ViewBase）
         * @param node 节点 或者 节点所挂载的组件
         */
        static CloseLocatedView(node: cc.Node | cc.Component): void;
    }
    namespace UI {
        const DIALOG_INFO = "_dialog_info";
    }
    enum UIHierarchy {
        main = 0,
        view = 1,
        dialog = 2,
        tips = 3
    }
    class UIMgr {
        static _hierarchy: Array<cc.Node>;
        static _inited: boolean;
        static Init(): void;
        static BindHierarchy(str: string): cc.Node;
    }
}
declare namespace f {
    class Util {
        /**
         * 将number的值限制在范围 [lower, upper]内
         * @param number 需要限制的数
         * @param lower 最小值
         * @param upper 最大值
         * @returns 被限制在范围内的数
         */
        static Clamp(number: any, lower: any, upper: any): any;
        static Random(n1: any, n2: any): any;
        static GetUrl(path: any, name: any): string;
        static _getClass(o: any): string;
        static CheckIntersection(node: cc.Node, node1: cc.Node): boolean;
        static GetSymmetricPoint(A: cc.Vec2, B: cc.Vec2, P: cc.Vec2): cc.Vec2;
        static MD5(string: string): string;
    }
}
declare namespace f {
    class ViewMgr {
        static root: cc.Node;
        static _curView: cc.Node;
        static _lastViews: Array<cc.Node>;
        static Init(node: cc.Node): void;
        static Open(node: cc.Node): void;
        static Close(node: cc.Node): void;
        static OnViewClose(e: cc.Event): void;
    }
}
