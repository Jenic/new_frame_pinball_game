# 分辨率
    画布分辨率为 630*1365
    宽高比为 9:19.5 适配IPhon10 机型

    背景图分辨率为 910*1365
    宽高比为 2:3 适配旧机型 小胖子屏幕

# 安装npm插件
* 在该文件同目录控制台运行  npm 命令
npm install crypto-js -save
npm install @types/crypto-js -save-dev
npm install protobufjs -save

# protobuf 消息文件生成及使用
1: 将proto定义文件放在 proto文件夹内
2: win运行 .\pb_maker.bat 
3: 消息文件生成在 assets/Script 内, pb.d.ts / pb.js
注意： pb.js 不要勾选导入为插件。



# 问题记录
# 1. 刚体runAction时，子节点如果也是刚体，则不会跟着父节点一起action
# 2. 刚体runAction是，如果判断子节点是刚体，跟着做action，则子节点会
#    累计两次action（cc.moveBy 产生双倍位移），原因未知

////////////////////////////////
TODO List
    游戏暂停
    关卡配置
    音效

    发布...
    资源管理
    热更管理

    网络
    登录
    关卡
    积分
    提现
    广告



# 框架内容

视图管理/弹窗管理
项目资源管理/游戏内资源管理
流程控制管理
Shader特效
消息传递
Protocol Buffer 3