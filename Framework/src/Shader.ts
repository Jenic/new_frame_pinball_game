namespace f {
    // interface option {
    //     duration?: number;
    //     size?: number;
    //     color?: cc.Color;
    //     timeScale?: number;
    //     repeat?: number;
    //     SDF?: boolean;
    //     block?: boolean;
    // }

    let INF = 1e10;
    export class ShaderUtil {
        protected static nodePool: cc.NodePool = new cc.NodePool();
        protected static get cameraNode(): cc.Node {
            let node;
            if (this.nodePool.size() > 0) {
                node = this.nodePool.get();
            } else {
                node = new cc.Node;
                let camera = node.addComponent(cc.Camera);
                camera.backgroundColor = new cc.Color(255, 255, 255, 0);        // 透明区域仍然保持透明，半透明区域和白色混合
                camera.clearFlags = cc.Camera.ClearFlags.DEPTH | cc.Camera.ClearFlags.STENCIL | cc.Camera.ClearFlags.COLOR;

                // 设置你想要的截图内容的 cullingMask
                camera.cullingMask = 0xffffffff;
            }
            return node;
        }

        protected static cutoff: number;
        protected static radius: number;
        protected static gridOuter: Float64Array;
        protected static gridInner: Float64Array;
        protected static f: Float64Array;
        protected static d: Float64Array;
        protected static z: Float64Array;
        protected static v: Int16Array;

        public static nodeName = 'shader_util_node';
        public static snap(node: cc.Node, width?: number, height?: number): cc.RenderTexture {
            // !当 node 带cc.Button 组件时，截图会出问题。。。
            if (!cc.isValid(node)) return;
            node.active = true;
            let extend = 0; // 额外的截图范围
            width = width || node.width;
            height = height || node.height;
            // 使截屏处于被截屏对象中心（两者有同样的父节点 ）
            let cameraNode = this.cameraNode;
            let camera = cameraNode.getComponent(cc.Camera);

            cameraNode.x = (0.5 - node.anchorX) * width;
            cameraNode.y = (0.5 - node.anchorY) * height;
            cameraNode.parent = node;

            let success: boolean = false;
            let result = null;
            try {
                let scaleX = node.scaleX;   //this.fitArea.scaleX;
                let scaleY = node.scaleY;   //this.fitArea.scaleY;
                let gl = cc.game['_renderContext'];

                let targetWidth = Math.floor(width * scaleX + extend * 2);      // texture's width/height must be integer
                let targetHeight = Math.floor(height * scaleY + extend * 2);

                // 内存纹理创建后缓存在目标节点上
                // 如果尺寸和上次不一样也重新创建
                let texture: cc.RenderTexture = new cc.RenderTexture();
                texture.initWithSize(targetWidth, targetHeight, gl.STENCIL_INDEX8);
                texture.packable = false;


                camera.alignWithScreen = false;
                // camera.orthoSize = root.height / 2;
                camera.orthoSize = targetHeight / 2;
                camera.targetTexture = texture;

                // 渲染一次摄像机，即更新一次内容到 RenderTexture 中
                camera.render(node);
                // let screenShot = target;
                success = true;
                result = texture;
            } finally {
                camera.targetTexture = null;
                this.nodePool.put(cameraNode);
                if (!success) {
                    // target.active = false;
                    console.log("截图失败");
                }
            }
            result.setFlipY(true);
            return result;
        }

        public static getPixels(data: cc.RenderTexture) {
            let pixels = data.readPixels();

            let width = data.width;
            let height = data.height;
            let picData = new Uint8Array(width * height * 4);
            let rowBytes = width * 4;
            for (let row = 0; row < height; row++) {
                let srow = height - 1 - row;
                let start = srow * width * 4;
                let reStart = row * width * 4;
                // save the piexls data
                for (let i = 0; i < rowBytes; i++) {
                    picData[reStart + i] = pixels[start + i];
                }
            }
            return picData;
        }


        public static savePicFile(data, width, height, cb: (err: Error, res: any) => void, filename?: string) {
            if (cc.sys.isNative) {

                let path = jsb.fileUtils.getWritablePath();
                let name = filename || 'render_to_sprite_image.png';
                let fullPath = path + name;
                let success = jsb['saveImageData'](data, width, height, fullPath)
                if (success) {
                    cc.log("save image data success, file: " + fullPath);
                    jsb.reflection.callStaticMethod('org/cocos2dx/javascript/NativeUtil', 'saveFile', '(Ljava/lang/String;)V', fullPath);
                    cb(null, true);
                }
                else {
                    cc.error("save image data failed!");
                    cb(new Error('save image data failed!'), null);
                }
            } else {

            }

        }
        /**
         * 为包装节点 和其所有子节点 附加shader特效
         * @param node 包装节点
         * @param name 特效名称
         * @param option 设置
         */
        // public static wrap(node: cc.Node, name: string, option?: option, testNode?: cc.Node): void {
        //     if (!node) {
        //         cc.error('#ShaderUtil->warp(): node不存在：', node);
        //         return;
        //     }
        //     let material = Res.GetTransEff(name);
        //     if (!material) {
        //         cc.error('#ShaderUtil->warp(): 材质不存在：', name);
        //         return;
        //     }

        //     let handler = node.getChildByName(this.nodeName);
        //     if (!cc.isValid(handler)) {
        //         handler = new cc.Node();
        //         handler.parent = node;
        //         // 大小
        //         handler.height = node.height;
        //         handler.width = node.width;
        //         // 适配缩放
        //         handler.scaleY = -1 / node.scaleY;
        //         handler.scaleX = 1 / node.scaleX;
        //         // 适配锚点
        //         handler.anchorX = node.anchorX;
        //         handler.anchorY = 1 - node.anchorY;
        //         handler.name = this.nodeName;
        //     }

        //     let sprite = handler.getComponent(cc.Sprite);
        //     if (!cc.isValid(sprite)) {
        //         sprite = handler.addComponent(cc.Sprite);
        //     }
        //     let mat = sprite.setMaterial(0, material);

        //     let texture = ShaderUtil.snap(node);
        //     if (option) {
        //         if (option.SDF) {
        //             let sdfRadius = Math.max(60, node.height / 3);
        //             texture = ShaderUtil.RenderSDF(texture, sdfRadius, 0.5).texture;
        //         }
        //         if (option.color) {
        //             mat.setProperty('color',
        //                 [
        //                     option.color.r / 255,
        //                     option.color.g / 255,
        //                     option.color.b / 255,
        //                     option.color.a / 255
        //                 ]);
        //         }

        //         if (option.block) {
        //             let block = handler.getComponent(cc.BlockInputEvents);
        //             if (!cc.isValid(block)) {
        //                 block = handler.addComponent(cc.BlockInputEvents);
        //             }
        //             block.enabled = true;
        //         } else {
        //             let block = handler.getComponent(cc.BlockInputEvents);
        //             if (cc.isValid(block)) {
        //                 block.enabled = false;
        //             }
        //         }
        //     }
        //     let sf = new cc.SpriteFrame(texture);
        //     sprite.spriteFrame = sf;

        //     if (cc.isValid(testNode)) {
        //         let comp = testNode.getComponent(cc.Sprite);
        //         comp.spriteFrame = sf;
        //     }

        // }
        /**
         * 取消包装的特效
         * @param node 包装节点
         */
        public static unWrap(node: cc.Node) {
            let handler = node.getChildByName(this.nodeName);
            handler.destroy();
        }

        public static RenderSDF(texture: cc.RenderTexture, radius?: number, cutoff?: number): { texture: cc.RenderTexture, alpha: Uint8ClampedArray } {
            let imgData = texture.readPixels();
            let width = texture.width;
            let height = texture.height;

            // initialize members
            // let cutoff = this.cutoff || 0.25;
            if (cutoff === undefined)
                cutoff = 0

            radius = radius || this.radius || 18;

            let area = width * height;
            let longSide = Math.max(width, height);
            this.gridOuter = new Float64Array(area);
            this.gridInner = new Float64Array(area);
            this.f = new Float64Array(longSide);
            this.d = new Float64Array(longSide);
            this.z = new Float64Array(longSide + 1);
            this.v = new Int16Array(longSide);

            var alphaChannel = new Uint8ClampedArray(area);

            for (var i = 0; i < area; i++) {
                var a = imgData[i * 4 + 3] / 255; // alpha value
                this.gridOuter[i] = a === 1 ? 0 : a === 0 ? INF : Math.pow(Math.max(0, 0.5 - a), 2);
                this.gridInner[i] = a === 1 ? INF : a === 0 ? 0 : Math.pow(Math.max(0, a - 0.5), 2);
            }

            this.EDT(this.gridOuter, width, height, this.f, this.d, this.v, this.z);
            this.EDT(this.gridInner, width, height, this.f, this.d, this.v, this.z);

            for (i = 0; i < area; i++) {
                var d = this.gridOuter[i] - this.gridInner[i];
                alphaChannel[i] = Math.max(0, Math.min(255, Math.round(255 - 255 * (d / radius + cutoff))));
                // imgData[i * 4 + 0] = 255;
                // imgData[i * 4 + 1] = 255;
                // imgData[i * 4 + 2] = 255;
                imgData[i * 4 + 3] = alphaChannel[i];
            }

            let resultTexture = new cc.RenderTexture;
            resultTexture.initWithData(imgData, cc.Texture2D.PixelFormat.RGBA8888, width, height);
            return {
                texture: resultTexture,
                alpha: alphaChannel
            };
        };

        // 2D Euclidean distance transform by Felzenszwalb & Huttenlocher https://cs.brown.edu/~pff/dt/
        protected static EDT(data, width, height, f, d, v, z) {
            for (var x = 0; x < width; x++) {
                for (var y = 0; y < height; y++) {
                    f[y] = data[y * width + x];
                }
                this.EDT1D(f, d, v, z, height);
                for (y = 0; y < height; y++) {
                    data[y * width + x] = d[y];
                }
            }
            for (y = 0; y < height; y++) {
                for (x = 0; x < width; x++) {
                    f[x] = data[y * width + x];
                }
                this.EDT1D(f, d, v, z, width);
                for (x = 0; x < width; x++) {
                    data[y * width + x] = Math.sqrt(d[x]);
                }
            }
        }

        // 1D squared distance transform
        protected static EDT1D(f, d, v, z, n) {
            v[0] = 0;
            z[0] = -INF;
            z[1] = +INF;

            for (var q = 1, k = 0; q < n; q++) {
                var s = ((f[q] + q * q) - (f[v[k]] + v[k] * v[k])) / (2 * q - 2 * v[k]);
                while (s <= z[k]) {
                    k--;
                    s = ((f[q] + q * q) - (f[v[k]] + v[k] * v[k])) / (2 * q - 2 * v[k]);
                }
                k++;
                v[k] = q;
                z[k] = s;
                z[k + 1] = +INF;
            }

            for (q = 0, k = 0; q < n; q++) {
                while (z[k + 1] < q) k++;
                d[q] = (q - v[k]) * (q - v[k]) + f[v[k]];
            }
        }
    }

}