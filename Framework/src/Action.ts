namespace f {
    export namespace Action {
        export interface IRunActionOption {
            target: cc.Node,
            duration?: number,
            x?: number,
            y?: number,
            delay?: number,
            cb?: () => void
            reset?: boolean,
        }
    }
    export class Action {
        /**
         * 节点渐入
         * @param option 动作参数 
         * @param option.target 目标节点 
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        public static FadeIn(option: Action.IRunActionOption) {
            let duration = option.duration || 1;
            let func: cc.ActionInterval = cc.fadeIn(duration);
            this.RunAction(option, func);
        }

        /**
         * 节点渐出
         * @param option 动作参数 
         * @param option.target 目标节点 
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        public static FadeOut(option: Action.IRunActionOption) {
            let duration = option.duration || 1;
            let func: cc.ActionInterval = cc.fadeOut(duration);
            this.RunAction(option, func);


        }
        /**
         * 节点渐出
         * @param option 动作参数 
         * @param option.target 目标节点 
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         * @param option.x 目标缩放(x和y)
         */
        public static ScaleTo(option: Action.IRunActionOption) {
            let duration = option.duration || 1;
            let scale = option.x || 1;
            let func: cc.ActionInterval = cc.scaleTo(duration, scale);
            this.RunAction(option, func);

        }

        public static MoveTo(option: Action.IRunActionOption) {
            let duration = option.duration || 1;
            let x = option.x || option.target.x;
            let y = option.y || option.target.y;
            let func: cc.ActionInterval = cc.moveTo(duration, x, y);
            this.RunAction(option, func);

        }

        /**
         * 节点 移动
         * @param option 动作配置参数
         * @param option.target 目标节点 
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         */
        public static MoveBy(option: Action.IRunActionOption) {
            let duration = option.duration || 1;
            let x = option.x || 0;
            let y = option.y || 0;
            let func: cc.ActionInterval = cc.moveBy(duration, x, y);
            this.RunAction(option, func);

        }

        /**
         * 节点 渐显+形变
         * @param option 动作配置参数
         * @param option.target 目标节点 
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.x x 位移
         * @param option.y 位移
         * @param option.cb 动作回调,动作执行结束后调用
         * @param shrink 是否是缩小, 默认false:扩大进入
         */
        public static ScaleFadeIn(option: Action.IRunActionOption, shrink?: boolean) {
            // node: cc.Node, duration: number, cb?: () => void
            let duration = option.duration || 1;

            let startScale = 0.2;
            if (shrink) {
                startScale = 2;
            }
            option.target.scale = startScale;
            option.target.opacity = 0;
            let func: cc.FiniteTimeAction = cc.spawn(
                cc.scaleTo(duration, 1).easing(cc.easeBackOut()),
                cc.fadeIn(duration)
            );
            this.RunAction(option, func);

        }

        /**
         * 节点 渐隐+形变
         * @param option 动作配置参数
         * @param option.target 目标节点 
         * @param option.duration 持续时长,单位:秒,默认:1
         * @param option.delay 延迟执行,单位:秒,默认:0
         * @param option.cb 动作回调,动作执行结束后调用
         * @param enlarge 是否是扩大, 默认false,缩小退出
         */
        public static ScaleFadeOut(option: Action.IRunActionOption, enlarge?: boolean) {
            // node: cc.Node, duration: number, cb?: () => void
            let duration = option.duration || 1;

            let endScale = 0.2;
            if (enlarge) {
                endScale = 2;
            }
            // option.target.scale = 1;
            // option.target.opacity = 0;
            let func: cc.FiniteTimeAction = cc.spawn(
                cc.scaleTo(duration, endScale).easing(cc.easeBackIn()),
                cc.fadeOut(duration)
            );
            this.RunAction(option, func);
        }


        static RunAction(option: Action.IRunActionOption, func: cc.FiniteTimeAction) {
            let node = option.target;
            let delay = option.delay;
            let cb = option.cb || (() => { });
            delay = delay || 0;
            if (option.reset) {
                node.stopAllActions();
            }
            node.runAction(
                cc.sequence(
                    cc.delayTime(delay),
                    func,
                    cc.callFunc(cb)
                )
            );
        }
    }

    //! 等待器
    type watiCb = () => void;
    export class Wait {
        _targetHandler: watiCb = () => { };
        private _completed: { [key: string]: boolean } = {};


        private get isAllReady(): boolean {
            for (const i in this._completed) {
                if (Object.prototype.hasOwnProperty.call(this._completed, i)) {
                    const el = this._completed[i];
                    if (!el) return false;
                }
            }
            return true;
        }
        constructor(cb: watiCb) {
            this._targetHandler = cb;
        }

        public static Alloc(cb?: watiCb) {
            return new Wait(cb ? cb : () => { });
        }


        public WaitFor(str: string | Array<string>) {
            if (str instanceof Array) {
                for (let i = 0; i < str.length; i++) {
                    const el = str[i];
                    this.WaitFor(el);
                }
            } else {
                console.log('f wait for ', str);
                this._completed[str] = false;
            }
        }

        public Ready(str: string) {
            this._completed[str] = true;
            console.log('f ready for ', str);
            if (this.isAllReady) {
                console.log('OK!!!!');
                this._targetHandler();
                this._targetHandler = () => { };
            }
        }
    }

    //! 动作链
    type endCallback = () => void
    export class Seq {

        public static Alloc(): Seq {
            return new Seq();
        }

        _sequence: Array<{ a: (end: endCallback) => void, b: any }> = [];
        _immediately: () => void = null;
        constructor() {
            let array = arguments[0];
            this._sequence = array || [];
        }

        then(callback, context, endState?) {
            let a = callback;
            let b = endState;
            if (context) {
                a = callback && callback.bind(context);
                b = endState && endState.bind(context);
            }
            this._sequence.push({ a, b });
            return this;
        }

        run() {
            this.resolve();
            return this;
        }

        resolve() {
            let next = this.getnext();
            if (!next) { return true; }
            if (next.a && typeof next.a === 'function') {
                next.a(this.resolve.bind(this));
            }
            this._immediately = next.b;
        }

        curFinish() {
            if (typeof this._immediately === 'function') {
                this._immediately();
                this._immediately = null;
            }
            return this.resolve();
        }

        getnext() {
            let a = this._sequence.shift();
            return a;
        }

        clear() {
            this._sequence = [];
        }
    }
}