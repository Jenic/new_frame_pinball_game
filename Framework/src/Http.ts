namespace f {
    export class Http {
        public static Get(url, callback) {
            var xhr = cc.loader.getXMLHttpRequest();//new XMLHttpRequest();//
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var respone = xhr.responseText;
                    callback(respone);
                } else {

                }
            };
            xhr.open("GET", url, true);
            if (cc.sys.isNative) {
                xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
            }
            // note: In Internet Explorer, the timeout property may be set only after calling the open()
            // method and before calling the send() method.
            xhr.timeout = 5000;// 5 seconds for timeout
            xhr.send();
        }

        public static POST(url, data, callback) {
            var xhr = cc.loader.getXMLHttpRequest();//new XMLHttpRequest();//
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var respone = xhr.responseText;
                    callback(respone);
                } else {

                }
            };
            xhr.open("POST", url, true);
            if (cc.sys.isNative) {
                xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
            }
            xhr.setRequestHeader("Content-Type", "application/json");
            // note: In Internet Explorer, the timeout property may be set only after calling the open()
            // method and before calling the send() method.
            xhr.timeout = 5000;// 5 seconds for timeout
            let reqStr = JSON.stringify(data);
            xhr.send(reqStr);
        }
    }
    
}