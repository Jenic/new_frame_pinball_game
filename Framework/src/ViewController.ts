namespace f {
    export class ViewMgr {
        static root: cc.Node = null; //
        static _curView: cc.Node = null;
        static _lastViews: Array<cc.Node> = [];


        public static Init(node:cc.Node) {
            this.root = node;
        }

        public static Open(node: cc.Node) {
            if (cc.isValid(this._curView)) {
                this._curView.active = false;
                this._lastViews.push(this._curView);
            }

            node.active = true;;
            node.parent = this.root;
            this._curView = node;
        }

        public static Close(node: cc.Node) {
            f.Arr.remove(this._lastViews, node);
            if (this._curView === node) {
                node.destroy();
                this._curView = this._lastViews.pop();
                this._curView.active = true;
            }
        }


        public static OnViewClose(e: cc.Event) {
            e.stopPropagationImmediate();
            let node = e.target;
            this.Close(node);
            // todo 让res controller 缓存节点
        }

    }


}