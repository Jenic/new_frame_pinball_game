namespace f {
    export namespace Event {
        export interface CallbackInfo {
            target: any,
            handler: EventHandler,
            once: boolean
        }
        export type EventHandler = (...params: any) => void
    }

    export class Event {
        _map: Map<string, Array<Event.CallbackInfo>> = new Map<string, Array<Event.CallbackInfo>>();

        On(key: string, cb: Event.EventHandler, caller?: any, once?: boolean): void {
            let arr = this._map.get(key) || [];
            for (let i = 0; i < arr.length; i++) {
                const el = arr[i];
                if (el.target === caller && el.handler === cb) {
                    console.log('事件已注册，跳过');
                    return;
                }
            }
            let info: Event.CallbackInfo = {
                target: caller,
                handler: cb,
                once
            };
            arr.push(info);
            this._map.set(key, arr);
        }

        Once(key: string, cb: Event.EventHandler, caller?: any) {
            this.On(key, cb, caller, true);
        }

        Off(key: string, cb: Event.EventHandler, caller?) {
            let arr = this._map.get(key);
            for (let i = 0; i < arr.length; i++) {
                const el = arr[i];
                if (el.target === caller && el.handler === cb) {
                    f.Arr.remove(arr, el);
                    return;
                }
            }
        }

        Emit(key: string, data?) {
            let handlers = this._map.get(key) || [];
            for (let i = 0; i < handlers.length; i++) {
                const el = handlers[i];
                if (typeof el.handler === 'function') {
                    el.handler.call(el.target, data);
                    if (el.once) {
                        this.Off(key, el.handler, el.target);
                    }
                }
            }
        }

        Clear() {
            this._map = new Map<string, Array<Event.CallbackInfo>>();
        }

    }

    export class GEvent {
        static _e: Event = new Event();

        public static On(type: string, handler: Event.EventHandler, target?: any, once?: boolean) {
            this._e.On(type, handler, target, once);
        }

        public static Once(type: string, handler: Event.EventHandler, target?: any) {
            this._e.Once(type, handler, target);
        }

        public static Off(type: string, handler: Event.EventHandler, target?: any) {
            this._e.Off(type, handler, target);
        }

        public static Emit(type: string, data?) {
            this._e.Emit(type, data);
        }

        public static Clear() {
            this._e.Clear();
        }
    }

    export enum EventType {
        PageClose = 'page_close',
        ClickBtnClose = 'click_btn_close',
        ListItemUpdate = 'list_item_update',
        ListItemContentVisible = 'list_item_content_visible',
        ViewBaseAddListener = 'view_base_add_listener',
        ViewBaseRemoveListener = 'view_base_remove_listener',
        ViewBaseSetEventTarget = 'view_base_set_event_target',
    }
}