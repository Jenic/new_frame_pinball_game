/// <reference path="../../creator.d.ts" />
namespace f {
    export namespace Sets {
        export class Queue<T>{
            _arr: Array<T>;
            constructor(arr?: Array<T>) {
                this._arr = arr || [];
            }

            get(): T {
                return this._arr.shift();
            }

            set(obj: T) {
                this._arr.push(obj);
            }

            clear(): void {
                this._arr = [];
            }

            get size(): number {
                return this._arr.length;
            }

        }

        export class Stack<T> {
            _arr: Array<T>;
            constructor(arr: Array<T>) {
                this._arr = arr;
            }

            get(): T {
                return this._arr.pop();
            }

            set(obj: T) {
                this._arr.push(obj);
            }

            clear() {
                this._arr = [];
            }

            get size(): number {
                return this._arr.length;
            }
        }
    }


    // 简易 Tween
    export class Tween {
        static FPS = 60;

        startObj: Object;
        _curObj: Object;
        endObj: Object;
        states: Sets.Queue<TweenState | TweenCallback> = new Sets.Queue<TweenState | TweenCallback>();
        _currentState: TweenState | TweenCallback;

        _curTime: number;
        _maxTime: number;
        Timer: any = null;

        _timeStamp: number;

        constructor(obj) {
            this.startObj = obj;
        }

        to(duration: number, obj: any, progress?: (obj: any, ratio: number) => void) {
            this.states.set(new TweenState(duration, obj, progress));
            return this;
        }

        call(callback: () => void) {
            this.states.set(new TweenCallback(callback));
        }

        start() {
            this._timeStamp = new Date().getTime();
            this.goOn();
            return this;
        }

        stop() {
            clearTimeout(this.Timer);
        }

        goOn() {
            if (this._currentState instanceof TweenState) {
                if (this._curTime > this._maxTime) {
                    let progress = this._currentState._progress;
                    if (typeof progress === 'function') {
                        this._curObj = this._currentState._target;
                        this.startObj = this._currentState._target;
                        progress(this._currentState._target, 1);
                    }

                    this.nextState();
                    return;
                } else {
                    let that = this;
                    this.Timer = setTimeout(() => {
                        let now = new Date().getTime();
                        let dt = now - this._timeStamp;
                        this._timeStamp = now;
                        that._curTime += dt / 1000;
                        let state = that._currentState as TweenState;
                        let progress = state._progress;
                        if (typeof progress === 'function') {
                            let Obj = {};
                            let ratio = that._curTime / that._maxTime;
                            that._curObj = Obj;

                            let keys = Object.keys(this.startObj);
                            for (let i = 0; i < keys.length; i++) {
                                const el = keys[i];
                                let value1 = this.startObj[el];
                                let value2 = this.endObj[el];
                                if (typeof value1 === 'number' && typeof value2 === 'number') {
                                    let value = value1 + (value2 - value1) * ratio;
                                    Obj[el] = value;
                                }

                                if (typeof value1 === 'string' && typeof value2 === 'string') {
                                    let oLength = value1.length;
                                    let offset = value2.length - value1.length;
                                    Obj[el] = value2.slice(0, oLength + offset * ratio);
                                }
                            }
                            progress(Obj, ratio);
                        }


                        that.goOn();
                    }, 1000 / Tween.FPS);
                }

                return;
            }

            if (this._currentState instanceof TweenCallback) {
                this._currentState._callback();
                this.nextState();
                return;
            }
            this.nextState();
        }
        nextState() {
            if (this.states.size > 0) {
                let state = this.states.get();
                if (state instanceof TweenState) {
                    this._maxTime = state._duration
                    this._curTime = 0;
                    this.endObj = state._target;
                }
                this._currentState = state;
                this.goOn();
            }
        }
    }

    class TweenState {
        _duration: number;
        _target: any;
        _progress: (obj: any, ratio: number) => void;
        constructor(duration: number, target: any, progress?: (obj: any, ratio: number) => void) {
            this._duration = duration;
            this._target = target;
            this._progress = progress;
        }
    }

    class TweenCallback {
        _callback: () => void;
        constructor(callback: () => void) {
            this._callback = callback;
        }
    }

}