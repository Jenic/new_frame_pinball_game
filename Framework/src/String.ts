namespace f {
    export class Str {
        public static format(tpl: string, data: any) {
            return tpl.replace(/\{\{(.+?)\}\}/g, function (m, m1) {
                m;
                return data[m1]
            })
        }

        // render('我是{{name}}，年龄{{age}}，性别{{sex}}',{
        //     name:'姓名',
        //     age:18,sex:'女',
        // }) 
        // // "我是姓名，年龄18，性别女"

        public static Compress(strNormalString: string) {
            var strCompressedString = "";
            var ht = new Array();
            for (i = 0; i < 128; i++) {
                ht[i] = i;
            }

            var used = 128;
            var intLeftOver = 0;
            var intOutputCode = 0;
            var pcode = 0;
            var ccode = 0;
            var k = 0;

            for (var i = 0; i < strNormalString.length; i++) {
                ccode = strNormalString.charCodeAt(i);
                k = (pcode << 8) | ccode;
                if (ht[k] != null) {
                    pcode = ht[k];
                } else {
                    intLeftOver += 12;
                    intOutputCode <<= 12;
                    intOutputCode |= pcode;
                    pcode = ccode;
                    if (intLeftOver >= 16) {
                        strCompressedString += String.fromCharCode(intOutputCode >> (intLeftOver - 16));
                        intOutputCode &= (Math.pow(2, (intLeftOver - 16)) - 1);
                        intLeftOver -= 16;
                    }
                    if (used < 4096) {
                        used++;
                        ht[k] = used - 1;
                    }
                }
            }

            if (pcode != 0) {
                intLeftOver += 12;
                intOutputCode <<= 12;
                intOutputCode |= pcode;
            }

            if (intLeftOver >= 16) {
                strCompressedString += String.fromCharCode(intOutputCode >> (intLeftOver - 16));
                intOutputCode &= (Math.pow(2, (intLeftOver - 16)) - 1);
                intLeftOver -= 16;
            }

            if (intLeftOver > 0) {
                intOutputCode <<= (16 - intLeftOver);
                strCompressedString += String.fromCharCode(intOutputCode);
            }

            return strCompressedString;
        }
        public static Decompress(strCompressedString: string):string {
            var strNormalString = "";
            var ht = new Array();

            for (i = 0; i < 128; i++) {
                ht[i] = String.fromCharCode(i);
            }

            var used = 128;
            var intLeftOver = 0;
            var intOutputCode = 0;
            var ccode = 0;
            var pcode = 0;
            var key = 0;

            for (var i = 0; i < strCompressedString.length; i++) {
                intLeftOver += 16;
                intOutputCode <<= 16;
                intOutputCode |= strCompressedString.charCodeAt(i);

                while (1) {
                    if (intLeftOver >= 12) {
                        ccode = intOutputCode >> (intLeftOver - 12);
                        if (typeof (key = ht[ccode]) != "undefined") {
                            strNormalString += key;
                            if (used > 128) {
                                ht[ht.length] = ht[pcode] + String(key).substr(0, 1);
                            }
                            pcode = ccode;
                        } else {
                            key = ht[pcode] + ht[pcode].substr(0, 1);
                            strNormalString += key;
                            ht[ht.length] = ht[pcode] + String(key).substr(0, 1);
                            pcode = ht.length - 1;
                        }

                        used++;
                        intLeftOver -= 12;
                        intOutputCode &= (Math.pow(2, intLeftOver) - 1);
                    } else {
                        break;
                    }
                }
            }
            return strNormalString.trimEnd()
        }
    }
}