namespace f {


    export namespace UI {
        export interface ConfirmDialogInfo {
            txt?: string,
            okcb?: () => void,
            okTxt?: string,
            cancelcb?: () => void,
            cancelTxt?: string,
        }

        export interface OnClickOption {
            zoom?: number
        }
    }

    export class UI {
        public static GetLoadPageInfo(name: string | Res.LoadPageInfo) {
            let pageInfo: Res.LoadPageInfo;
            if (typeof name === 'string') {
                pageInfo = { name }
            } else {
                pageInfo = name
            }
            return pageInfo;
        }
        public static OpenMainPage(name: string | Res.LoadPageInfo) {
            let pageInfo: Res.LoadPageInfo = this.GetLoadPageInfo(name);
            Res.LoadPage(pageInfo, (node) => {
                MainPageController.Open(node, null);
            })
        }
        public static OpenPage(name: string | Res.LoadPageInfo) {
            let pageInfo: Res.LoadPageInfo = this.GetLoadPageInfo(name);
            Res.LoadPage(pageInfo, (node) => {
                PageController.Open(node, null);
            })
        }

        public static OpenDialog(name: string | Res.LoadPageInfo, param?: any, eventTarget?: Event) {
            let pageInfo: Res.LoadPageInfo = this.GetLoadPageInfo(name);
            Res.LoadDialog(pageInfo, (node) => {
                NodeMgr.SetPageParam(node, param);
                DialogController.Open(node, eventTarget);
            })
        }


        private static _inLineWaittingPageInfo: Array<any> = new Array<Res.LoadPageInfo>();
        private static _inlineLoading: boolean = false;
        public static OpenDialogInline(name: string | Res.LoadPageInfo, param?: any, eventTarget?: Event) {
            let pageInfo: Res.LoadPageInfo = this.GetLoadPageInfo(name);
            if (this._inlineLoading) {
                this._inLineWaittingPageInfo.push({ pageInfo, param, eventTarget });
                return;
            } else {
                this._inlineLoading = true;
                Res.LoadDialog(pageInfo, (node) => {
                    NodeMgr.SetPageParam(node, param);
                    DialogController.InlineOpen(node, eventTarget);
                    this._inlineLoading = false;

                    let info = this._inLineWaittingPageInfo.shift();
                    if(info){
                        this.OpenDialogInline(info.pageInfo, info.param, info.eventTarget);
                    }
                })
            }

        }

        public static CloseDialog(name: string) {
            DialogController.CloseDialogByName(name);
        }

        /**
         * 
         * @param option 
         * @param option.txt 确认框内容
         * @param option.tittle 确认框标题
         * @param option.okcb 确认按钮回调 
         * @param option.okTxt 确认按钮文本 
         * @param option.cancelcb 取消按钮回调 
         * @param option.cancelTxt 取消按钮回调 
         */
        public static Confirm(option: UI.ConfirmDialogInfo) {
            let name = Res.DEFAULT_DIALOG_NAME;
            Res.LoadDialog({ name }, (node) => {
                NodeMgr.SetPageParam(node, option);
                DialogController.Open(node, null);
            })
        }

        public static Tip(str: string) {
            TipsController.showTipsInLine(str);
        }

        /**
         * 为目标节点添加点击事件
         * @param target 目标节点
         * @param cb 点击回调
         * @param caller 作用域
         * @returns 
         */
        public static OnClick(target: cc.Node, cb: () => void, caller?: any, option?: UI.OnClickOption) {
            if (!cc.isValid(target)) {
                console.warn(`#节点无效，添加点击事件失败！`)
                return;
            }
            if (caller) cb = cb.bind(caller);
            let x = target.scale;
            let zoom = option && option.zoom || 0.8;
            let duration = 0.05;
            // 防止多次点击
            const SINGLE = "__single_click";
            let block = target.getComponent(cc.BlockInputEvents);
            if (!block) target.addComponent(cc.BlockInputEvents);
            target.on(cc.Node.EventType.TOUCH_START, () => {
                if (target[SINGLE]) {
                    return;
                }
                target[SINGLE] = true;
                Action.ScaleTo({ target, duration, x: x * zoom });
            })
            target.on(cc.Node.EventType.TOUCH_CANCEL, () => {
                target[SINGLE] = false;
                Action.ScaleTo({ target, duration, x });
            })
            target.on(cc.Node.EventType.TOUCH_END, () => {
                target[SINGLE] = false;
                Action.ScaleTo({ target, duration, x, cb });
            })
        }

        public static SetTxt(target: cc.Node | cc.Label | cc.RichText, str: string | number) {
            if (!cc.isValid(target)) {
                console.warn('#SetTxt ：target 无效 ')
                return;
            }
            let label = target;
            if (label instanceof cc.Node) {
                let comp = label.getComponent(cc.Label);
                if (comp) {
                    label = comp;
                } else {
                    label = label.getComponent(cc.RichText);
                }
            }
            if (label) {
                label.string = String(str);
            } else {
                console.warn('#SetTxt ：target 未挂载 cc.Label | cc.RichText 组件 ')

            }
        }

        public static SetImg(target: cc.Node | cc.Sprite, sf: cc.SpriteFrame) {
            if (!cc.isValid(target)) {
                console.warn('#SetImg ：target 无效 ');
                return;
            }
            let sprite = target;
            if (sprite instanceof cc.Node) {
                sprite = sprite.getComponent(cc.Sprite);
            }
            if (sprite) {
                sprite.spriteFrame = sf;
            } else {
                console.warn('#SetImg ：target 未挂载 cc.Sprite 组件 ')

            }
        }



        public static FixedBorders(node: cc.Node, borders: cc.Node, borderSize: number) {
            borders.x = node.x;
            borders.y = node.y;

            let top = borders.children[0];
            let bottom = borders.children[1];
            let left = borders.children[2];
            let right = borders.children[3];

            top.anchorY = 0;
            top.width = node.width + 2 * borderSize;
            top.height = borderSize;
            top.x = node.x;
            top.y = node.y + node.height / 2;

            bottom.anchorY = 1;
            bottom.width = node.width + 2 * borderSize;
            bottom.height = borderSize;
            bottom.x = node.x;
            bottom.y = node.y - node.height / 2;

            left.anchorX = 1;
            left.width = borderSize;
            left.height = node.height + borderSize * 2;
            left.x = node.x - node.width / 2;
            left.y = node.y;

            right.anchorX = 0;
            right.width = borderSize;
            right.height = node.height + borderSize * 2;
            right.x = node.x + node.width / 2;
            right.y = node.y;

        }

        /**
         * 关闭该节点所附着的页面（ViewBase）
         * @param node 节点 或者 节点所挂载的组件
         */
        public static CloseLocatedView(node: cc.Node | cc.Component) {
            if (node instanceof cc.Component) {
                node = node.node;
            }
            let e = new cc.Event.EventCustom(f.EventType.ClickBtnClose, true);
            node.dispatchEvent(e);
        }
    }

    export namespace UI {
        export const DIALOG_INFO = '_dialog_info';
    }
    export enum UIHierarchy {
        main,
        view,
        dialog,
        tips
    }
    export class UIMgr {
        static _hierarchy: Array<cc.Node> = [];
        static _inited: boolean = false;
        public static Init() {
            // if (this._inited) {
            //     return;
            // }
            // this._inited = true;
            MainPageController.Init(this.BindHierarchy('main'));
            PageController.Init(this.BindHierarchy('view'));
            DialogController.Init(this.BindHierarchy('dialog'));
            TipsController.Init(this.BindHierarchy('tips'));
        }

        static BindHierarchy(str: string) {
            let scene: cc.Node = cc.director.getScene();
            let canvas = cc.find('Canvas', scene);
            let node = NodeMgr.AddSingletonChild(canvas, str);

            this._hierarchy[UIHierarchy[str]] = node;
            node.zIndex = UIHierarchy[str];
            return node;
        }

    }

    class ViewController {
        static root: cc.Node;
        public static Init(node: cc.Node) {
            console.log("view controller init!")
            this.root = node;
            node.on(EventType.PageClose, this.OnPageClose, this);

        }

        static OnPageClose(event: cc.Event) {
            event.stopPropagationImmediate();
            this.Close(event.target);
        }

        static Open(node: cc.Node, eventTarget: Event) {
            console.log('view controller open ', node);
            node.parent = this.root;
            node.active = true;
            //! onLoad方法在 首次激活 或者附加在 激活节点时 调用；
            //! 若在这之前 emit 则由于 未执行 onLoad里的on方法，emit石沉大海
            node.emit(EventType.ViewBaseAddListener);
            node.emit(EventType.ViewBaseSetEventTarget, eventTarget);
        }

        static Close(node: cc.Node) {
            node.emit(EventType.ViewBaseRemoveListener);
            Res.TakePage(node);
        }
    }


    class MainPageController extends ViewController {

    }

    class PageController extends ViewController {

    }

    interface DialogSeqInfo {
        node: cc.Node;
        eventTarget: Event;
    }
    class DialogController extends ViewController {
        static _mask: cc.Node;

        static _opening: cc.Node;
        static _hidding: Array<cc.Node>;
        static _waittingDialogs: Array<DialogSeqInfo>;

        public static override Init(node: cc.Node) {
            super.Init(node);
            this._hidding = new Array<cc.Node>();
            this._waittingDialogs = new Array<DialogSeqInfo>();

            let mask = NodeMgr.AddSingletonChild(this.root, 'mask');
            let sprite = NodeMgr.AddSingletonComponent(mask, cc.Sprite) as cc.Sprite;
            NodeMgr.AddSingletonComponent(mask, cc.BlockInputEvents);
            sprite.spriteFrame = Res.GetDefaultSprite();
            mask.color = cc.Color.GRAY;
            mask.opacity = 153;
            mask.width = cc.visibleRect.width;
            mask.height = cc.visibleRect.height;
            mask.active = false;
            mask.on(cc.Node.EventType.TOUCH_END, this.CloseCurrent, this);
            this._mask = mask;
        }

        static override Open(node: cc.Node, eventTarget: Event) {
            console.log('dialog controller open a dialog');

            super.Open(node, eventTarget);
            if (this._opening) {
                this.Hide(this._opening);
            }
            this._opening = node;
            this._mask.active = true;
            Action.ScaleFadeIn({ target: node, duration: 0.3 });
        }

        static Hide(node: cc.Node) {
            node.active = false;
            this._hidding.push(node);
            node.emit(EventType.ViewBaseRemoveListener);
        }

        static Show(node: cc.Node) {
            node.emit(EventType.ViewBaseAddListener);
            node.active = true;
        }

        static InlineOpen(node: cc.Node, eventTarget: Event) {
            if (!this._opening) {
                this.Open(node, eventTarget);
            } else {
                let dialog: DialogSeqInfo = {
                    node,
                    eventTarget
                }
                this._waittingDialogs.push(dialog);
            }
        }

        static CloseCurrent() {
            let node = this._opening;
            if (node) {
                this.Close(node);
            }
        }
        static CloseDialogByName(name: string) {
            for (let i = 0; i < this._hidding.length; i++) {
                const el = this._hidding[i];
                if (el.name === name) {
                    f.Arr.remove(this._hidding, el);
                    Res.TakeDialog(el);
                }
            }
        }
        static override  Close(node: cc.Node) {
            console.log('dialog controller close a dialog')
            this._opening = null;
            //! 先显示隐藏节点
            if (this._hidding.length > 0) {
                let hideNode = this._hidding.pop();
                this._opening = hideNode;
                this.Show(hideNode);
                Res.TakeDialog(node);
                return;
            }
            //! 再依次打开窗口
            let dialog = this._waittingDialogs.shift();
            this._mask.active = !!dialog;
            node.emit(EventType.ViewBaseRemoveListener);
            Action.ScaleFadeOut({
                target: node, duration: 0.3, cb: () => {
                    Res.TakeDialog(node);
                    if (dialog) {
                        this.Open(dialog.node, dialog.eventTarget);
                    }
                }
            });
        }
    }

    class TipsController extends ViewController {
        static _opening: Array<cc.Node> = new Array<cc.Node>();
        static _waittingTips: Array<string> = new Array<string>();
        static _isInAction: boolean = false;
        public static showTip(str) {
            let node = Res.LoadTip();
            node.parent = this.root;
            node.active = true;
            let label = node.children[0];
            let comp = NodeMgr.AddSingletonComponent(label, cc.Label) as cc.Label;
            comp.string = str;


            let duration = 0.8;
            let distance = node.height + 2;
            for (let i = 0; i < this._opening.length; i++) {
                const el = this._opening[i];
                Action.MoveBy({
                    target: el,
                    y: distance,
                    duration
                })
            }
            this._opening.push(node);
            this._isInAction = true;
            node.runAction(
                cc.sequence(
                    cc.moveBy(duration, 0, distance),
                    cc.callFunc(() => {
                        this._isInAction = false;
                        this.hasNext();
                    }),
                    cc.delayTime(duration * 3),
                    cc.callFunc(() => {
                        this.Close(node);
                    })
                )
            )
        }

        public static showTipsInLine(str) {
            if (this._isInAction) {
                this._waittingTips.push(str);
            } else {
                this.showTip(str);
            }
        }

        static override  Close(node: cc.Node) {
            console.log('dialog controller close a dialog')
            f.Arr.remove(this._opening, node);
            Res.TakeDialog(node);
        }

        static hasNext() {
            let string = this._waittingTips.shift();
            if (string) {
                this.showTip(string);
            }
        }

    }

}
