namespace f {
    export namespace Res {
        export interface LoadPageInfo {
            name: string,
            bundleName?: string,
            path?: string,
        }

        export interface PreloadInfo {
            comonConfirm: cc.Node | cc.Prefab,
            tip: cc.Node | cc.Prefab,
        }
    }

    export class Res {
        static viewCache: Array<cc.Node> = new Array<cc.Node>();
        static _viewCacheSize: number = 0;

        static dialogCache: Array<cc.Node> = new Array<cc.Node>();
        static _dialogCacheSize: number = 0;

        static tipChache: Array<cc.Node> = new Array<cc.Node>();
        static _tipChacheSize: number = 0;

        static defaultDialog: cc.Node | cc.Prefab;
        static DEFAULT_DIALOG_NAME: string = '_defaultDialogName_';

        static defaultTip: cc.Node | cc.Prefab;

        public static DEBUG() {
            return { viewCache: this.viewCache, dialogCache: this.dialogCache };
        }
        public static GetTransEff(str: string): cc.Material {
            // TODO 获取cocos 材质
            str;
            return
        }

        /**
         * 资源预加载
         * @param prelaodInfo 预加载内容
         * @param prelaodInfo.comonConfirm 默认确认框
         * @param prelaodInfo.tip 默认tips提示
         */
        public static Preload(prelaodInfo: Res.PreloadInfo) {
            this.defaultDialog = prelaodInfo.comonConfirm;
            this.defaultTip = prelaodInfo.tip;
        }
        public static LoadCommonDialog(node: cc.Node | cc.Prefab) {
            this.defaultDialog = node;
        }

        // -------------------------- 界面加载相关
        public static LoadPage(option: Res.LoadPageInfo, cb: (node: cc.Node) => void) {
            let node = this.GetViewCache(option.name);
            if (cc.isValid(node)) {
                cb(node);
                return;
            }
            if (option.bundleName) {
                this.LoadBundle(option.bundleName, (bundle) => {
                    bundle.load<cc.Prefab>(option.name, cc.Prefab, (err, res) => {
                        if (err) {
                            console.log(`#Bundle LoadPage: Error `, err);
                            return;
                        }
                        let node = cc.instantiate(res);
                        node.name = option.name;
                        cb(node);
                    })
                })
            } else {
                let path = Util.GetUrl(option.path, option.name);
                cc.loader.loadRes(path, cc.Prefab, (err, res) => {
                    if (err) {
                        console.log(`#LoadPage: Error `, err);
                        return;
                    }
                    let node = cc.instantiate(res);
                    node.name = option.name;
                    cb(node);
                })
            }
        }

        public static LoadDialog(option: Res.LoadPageInfo, cb: (node: cc.Node) => void) {
            let name = option.name || this.DEFAULT_DIALOG_NAME;
            let node = this.GetDialogCache(name);
            if (cc.isValid(node)) {
                cb(node);
                return;
            }

            if (name === this.DEFAULT_DIALOG_NAME) {
                let dialog = cc.instantiate(this.defaultDialog) as cc.Node;
                cb(dialog);
                return;
            }

            if (option.bundleName) {
                this.LoadBundle(option.bundleName, (bundle) => {
                    bundle.load<cc.Prefab>(option.name, cc.Prefab, (err, res) => {
                        if (err) {
                            console.log(`#Bundle LoadDialog: Error `, err);
                            return;
                        }

                        cb(cc.instantiate(res));
                    })
                })
            } else {
                let path = Util.GetUrl(option.path, option.name);
                cc.loader.loadRes(path, cc.Prefab, (err, res) => {
                    if (err) {
                        console.log(`#LoadDialog: Error `, err);
                        return;
                    }
                    cb(cc.instantiate(res));
                })
            }
        }

        public static LoadTip() {
            let node = this.GetTipCache();
            if (cc.isValid(node)) {
                return node;
            }
            let tip = cc.instantiate(this.defaultTip) as cc.Node;
            return tip;
        }

        public static LoadBundle(name: string, cb: (bundle: cc.AssetManager.Bundle) => void) {
            cc.assetManager.loadBundle(name, (err, bundle) => {
                if (err) {
                    console.log(`#LoadBundle: Error`, err);
                    return;
                }
                cb(bundle);
            })
        }

        public static TakePage(node: cc.Node) {
            this.CacheView(node);
        }

        public static TakeDialog(node: cc.Node) {
            this.CacheDialog(node);
        }

        public static TakeTip(node: cc.Node) {
            this.CacheTip(node);
        }

        public static GetDialogCache(dialogName: string): cc.Node {
            for (let i = 0; i < this.dialogCache.length; i++) {
                const el = this.dialogCache[i];
                if (el.name === dialogName) {
                    Arr.remove(this.dialogCache, el);
                    return el;
                }
            }
            return null;
        }

        static CacheDialog(node: cc.Node) {
            this.dialogCache.push(node);
            node.removeFromParent(false);
            node.active = false;
            if (this.dialogCache.length > this._dialogCacheSize) {
                let delNode = this.dialogCache.shift();
                if (cc.isValid(delNode)) {
                    delNode.destroy();
                }
            }
        }

        public static GetViewCache(pageName: string): cc.Node {
            for (let i = 0; i < this.viewCache.length; i++) {
                const el = this.viewCache[i];
                if (el.name === pageName) {
                    Arr.remove(this.viewCache, el);
                    return el;
                }
            }
            return null;
        }

        static CacheView(node: cc.Node) {
            this.viewCache.push(node);
            node.removeFromParent(false);
            node.active = false;
            if (this.viewCache.length > this._viewCacheSize) {
                let delNode = this.viewCache.shift();
                if (cc.isValid(delNode)) {
                    delNode.destroy();
                }
            }
        }

        public static GetTipCache() {
            return this.tipChache.shift();
        }

        static CacheTip(node) {
            this.tipChache.push(node);
            node.removeFromParent(false);
            node.active = false;
            if (this.tipChache.length > this._tipChacheSize) {
                let delNode = this.tipChache.shift();
                if (cc.isValid(delNode)) {
                    delNode.destroy();
                }
            }
        }
        // ---------------------------------------------------

        public static GetStorage(key: string): string {
            return cc.sys.localStorage.getItem(key);
        }

        public static SetStorage (key: string, value: string): void {
            cc.sys.localStorage.setItem(key, value);
        }

        public static GetDefaultSprite() {
            let texture = new cc.RenderTexture();
            texture.initWithData(new Uint8Array([0, 0, 0]), cc.Texture2D.PixelFormat.RGB888, 1, 1);
            let sp = new cc.SpriteFrame(texture);
            return sp;
        }
    }
}