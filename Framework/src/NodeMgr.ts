namespace f {
    interface ActionOption {
        duration?: number;
        cb?: () => void,
    }
    export class NodeMgr {
        public static AddSingletonChild(parent: cc.Node, name: string): cc.Node {
            let node = parent.getChildByName(name);
            if (!cc.isValid(node)) {
                node = new cc.Node();
                node.name = name;
                node.parent = parent;
            }
            return node;
        }

        public static AddSingletonComponent(node: cc.Node, type: any): cc.Component {
            let comp = node.getComponent(type);
            if (!cc.isValid(comp)) {
                comp = node.addComponent(type);
            }
            return comp;
        }

        public static CreateNodePool(str: string) {
            return new cc.NodePool(str);
        }

        public static SetPageParam(node: cc.Node, param: any) {
            node[f.UI.DIALOG_INFO] = param;
        }

        public static GetPageParam(node: cc.Node) {
            return node[f.UI.DIALOG_INFO];
        }

        public static Visible(target: cc.Node, active: boolean, option?: ActionOption) {
            if (!cc.isValid(target)) {
                console.warn('#SetImg ：target 无效 ');
                return;
            }
            let duration = option && option.duration || 0;
            let cb = option && option.cb || (() => { });
            if (duration > 0) {
                if (active) {
                    f.Action.ScaleFadeIn({ target, duration, cb });
                } else {
                    f.Action.ScaleFadeOut({ target, duration, cb });
                }
            } else {
                target.active = active;
                target.scale = active ? 1 : 0;
                target.opacity = active ? 255 : 0;
            }

        }

    }
}