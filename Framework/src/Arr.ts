namespace f {
    export class Arr {
        public static remove<T>(array: Array<T>, value: T): boolean {
            var index = array.indexOf(value);
            if (index >= 0) {
                this.removeAt(array, index);
                return true;
            }
            else {
                return false;
            }
        }
        public static fastRemove<T>(array: Array<T>, value: T) {
            var index = array.indexOf(value);
            if (index >= 0) {
                array[index] = array[array.length - 1];
                --array.length;
            }
        }
        public static removeAt<T>(array: Array<T>, index: number) {
            array.splice(index, 1);
        }

        public static fastRemoveAt<T>(array: Array<T>, index: number) {
            var length = array.length;
            if (index < 0 || index >= length) {
                return;
            }
            array[index] = array[length - 1];
            array.length = length - 1;
        }
        public static contains<T>(array: Array<T>, value: T) {
            return array.indexOf(value) >= 0;
        }


        public static verifyType<T>(array: Array<T>, type) {
            if (array && array.length > 0) {
                for (var i = 0; i < array.length; i++) {
                    if (!(array[i] instanceof type)) {
                        return false;
                    }
                }
            }
            return true;
        }
        public static copy(array) {
            var i, len = array.length, arr_clone = new Array(len);
            for (i = 0; i < len; i += 1)
                arr_clone[i] = array[i];
            return arr_clone;
        }
    }
}